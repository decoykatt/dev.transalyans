-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Ноя 30 2017 г., 14:26
-- Версия сервера: 5.6.36-cll-lve
-- Версия PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `cefar2_gktas`
--

-- --------------------------------------------------------

--
-- Структура таблицы `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `alias` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `html` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `banner`
--

INSERT INTO `banner` (`id`, `alias`, `name`, `html`) VALUES
(1, 'phone', 'Телефон', '+7 (992) 001 91 38'),
(2, 'email', 'Мэил', 'gk-tas@yandex.ru');

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_section`
--

CREATE TABLE `catalog_section` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) DEFAULT NULL,
  `alias` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position_table` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `section_table` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `children_tpl` text COLLATE utf8_unicode_ci,
  `leaf` smallint(6) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `catalog_section`
--

INSERT INTO `catalog_section` (`id`, `parent_id`, `title`, `position`, `alias`, `position_table`, `section_table`, `children_tpl`, `leaf`) VALUES
(1, NULL, 'Формы', NULL, 'forms_section', NULL, 'section_forms_settings', '<tpl>\r\n  <section>section_forms</section>\r\n  <position>position_forms</position>\r\n  <leaf>1</leaf>\r\n</tpl>', NULL),
(2, NULL, 'ГРУППА КОМПАНИЙ «ТРАНСАЛЬЯНС»', 2, 'gruppa-kompanij-transalyans', NULL, NULL, NULL, NULL),
(4, NULL, 'Что вы получаете?', 3, 'chto-vy-poluchaete', 'position_bonus', '', '', NULL),
(5, NULL, 'Как происходит сотрудничество', 4, 'kak-proishodit-sotrudnichestvo', 'position_coop', '', '', NULL),
(6, 1, 'Форма обращения', 1, 'forma_obracsheniya', 'position_forms', 'section_forms', '', 1),
(8, NULL, 'ГРУППА КОМПАНИЙ', 5, 'gruppa-kompanij', '', 'section_page', '', NULL),
(9, NULL, 'Слайдер', 6, 'slajder', 'position_slides', '', '', NULL),
(10, 1, 'Оставить заявку хэдер', 2, 'ostavit_zayavku_heder', 'position_forms', 'section_forms', '', 1),
(11, 1, 'Оставить заявку футер', 3, 'ostavit_zayavku_futer', 'position_forms', 'section_forms', '', 1),
(12, NULL, 'Таймер', 7, 'tajmer', '', 'section_timer', '', NULL),
(13, 1, 'Заявка акция', 4, 'zayavka_akciya', 'position_forms', 'section_forms', '', 1),
(14, 1, 'Форма слайдер', 5, 'forma_slajder', 'position_forms', 'section_forms', '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `forms_field_action`
--

CREATE TABLE `forms_field_action` (
  `id` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `position` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `forms_field_action`
--

INSERT INTO `forms_field_action` (`id`, `name`, `position`) VALUES
('auth', 'Авторизация', 2),
('is_active', 'Активация учетной запись', 4),
('register', 'Регистрация', 1),
('restore_pass', 'Восстановить пароль', 3),
('send_email', 'Отправка на email', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `forms_field_type`
--

CREATE TABLE `forms_field_type` (
  `id` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `position` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `forms_field_type`
--

INSERT INTO `forms_field_type` (`id`, `name`, `position`) VALUES
('check', 'Флаг', 7),
('date', 'Дата', 5),
('hidden', 'Скрытое поле', 11),
('html', 'Произвольный HTML', 15),
('info', 'Информация', 14),
('label', 'Подпись', 13),
('link', 'Ссылка', 10),
('memo', 'Многострочный текст', 2),
('pwd', 'Пароль', 6),
('radiobox', 'Радиобокс', 12),
('select', 'Выпадающий список', 3),
('submit', 'Кнопка подачи запроса', 8),
('text', 'Текстовая строка', 1),
('upload', 'Обзор', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `menus`
--

INSERT INTO `menus` (`id`, `name`, `title`) VALUES
(1, 'header-menu', 'Меню шапка'),
(2, 'footer', 'Меню футер');

-- --------------------------------------------------------

--
-- Структура таблицы `menus_item`
--

CREATE TABLE `menus_item` (
  `id` int(11) NOT NULL,
  `menus_id` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `visible` smallint(6) NOT NULL DEFAULT '0',
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `type_link` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attr` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_src` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `menus_item`
--

INSERT INTO `menus_item` (`id`, `menus_id`, `title`, `parent_id`, `position`, `visible`, `type`, `type_id`, `type_link`, `attr`, `img_src`) VALUES
(1, 1, 'О компании', NULL, 1, 1, '_link', NULL, '#main1', 'class=\"jakor\"', NULL),
(2, 1, 'Преимущества', NULL, 2, 1, '_link', NULL, '#adv21', 'class=\"jakor\"', NULL),
(3, 1, 'Сотрудничество', NULL, 3, 1, '_link', NULL, '#sotr', 'class=\"jakor\"', NULL),
(4, 1, 'Контакты', NULL, 4, 1, '_link', NULL, '#down', 'class=\"jakor\"', NULL),
(5, 1, 'Акции', NULL, 5, 1, '_link', NULL, '#maims', 'class=\"jakor\"', NULL),
(19, 1, '+7 (992) 001 91 38', NULL, 6, 1, '_link', NULL, 'tel:+7 (992) 001 91 38', 'class=\"header-tel\"', NULL),
(11, 2, 'О компании', NULL, 1, 1, '_link', NULL, '#main1', 'class=\"jakor\"', NULL),
(12, 2, 'Преимущества', NULL, 2, 1, '_link', NULL, '#adv21', 'class=\"jakor\"', NULL),
(13, 2, 'Сотрудничество', NULL, 3, 1, '_link', NULL, '#sotr', 'class=\"jakor\"', NULL),
(14, 2, 'Контакты', NULL, 4, 1, '_link', NULL, '#down', 'class=\"jakor\"', NULL),
(15, 2, 'Акции', NULL, 5, 1, '_link', NULL, '#maims', 'class=\"jakor\"', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title2` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `position` int(11) NOT NULL,
  `visible` smallint(6) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `alias` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plugins` text COLLATE utf8_unicode_ci,
  `mainpage` smallint(6) NOT NULL DEFAULT '0',
  `announcement` text COLLATE utf8_unicode_ci,
  `fileupload` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_src` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `redirect` smallint(6) DEFAULT NULL,
  `head_title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `tag` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `title`, `title2`, `content`, `position`, `visible`, `parent_id`, `alias`, `template`, `plugins`, `mainpage`, `announcement`, `fileupload`, `img_src`, `redirect`, `head_title`, `meta_keywords`, `meta_description`, `tag`) VALUES
(1, 'Главная', '', '<h1>ГРУППА КОМПАНИЙ &laquo;ТРАНСАЛЬЯНС&raquo;</h1>\n\n<div class=\"liline\"></div>\n\n<p>Оказываем транспортные услуги по доставке грузов в Екатеринбург и из Екатеринбурга по России. Для перевозки грузов используется автомобильный транспорт. Нашим основным направлением является &mdash; осуществление транспортировки негабаритных и тяжеловесных грузов на территории России.</p>\n\n<p>Перевозка негабаритных грузов: крупногабаритных и тяжеловесных имеет особую специфику организации. Этот вид услуги требует детального знания особенностей эксплутации грузового автомобильного транспорта, дорожного законодательства и особенностей автотрасс предпологаемого маршрута. Специалисты компании хорошо знакомы с особенностями перевозок негабарита различных видов: техники, емкостей, бытовок, ж.б. конструкций, профильных материалов, станков, оборудования.</p>\n\n<ul>\n	<li>Железнодорожные перевозки</li>\n	<li>Мультимодальные перевозки</li>\n	<li>Пассажирские автоперевозки</li>\n	<li>Складские услуги</li>\n	<li>Таможенные услуги</li>\n</ul>\n', 1, 0, NULL, 'index', 'main.tpl.php', '', 1, '', NULL, NULL, NULL, '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `position_bonus`
--

CREATE TABLE `position_bonus` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `title` varchar(500) DEFAULT NULL,
  `content` text,
  `img` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `position_bonus`
--

INSERT INTO `position_bonus` (`id`, `section_id`, `title`, `content`, `img`) VALUES
(5, 4, 'ЭКОНОМИЯ ВАШЕГО ВРЕМЕНИ', 'Весь процесс подготовки документов, планирование маршрута и оформление разрешений, мы берем на себя.', 'files/catalog/upload/4_clock.svg'),
(6, 4, 'ПОЛНАЯ ЮРИДИЧЕСКАЯ ОТВЕТСТВЕННОСТЬ ЗА ВАШ ГРУЗ', 'Договор составлен в Вашу пользу, мы берем на себя полную ответственность о сохранности Вашего груза и гарантию доставки в оговоренные сроки. ', 'files/catalog/upload/5_lawyer.svg'),
(7, 4, 'БЕЗОПАСНОСТЬ В ПУТИ СЛЕДОВАНИЯ', 'Предоставление машины прикрытия или машины сопровождения. ', 'files/catalog/upload/6_shield.svg'),
(8, 4, 'ОНЛАЙН МОНИТОРИНГ ВАШЕГО ГРУЗА', 'Хотите уточнить где находится Ваш груз в данный момент? Звоните! Наш оператор в режиме 24 часа всегда готов проинформировать по какой трассе в данный момент времени движется груз и ориентировочное время в пути. ', 'files/catalog/upload/7_speak.svg'),
(9, 4, 'ПРЕДВАРИТЕЛЬНО ОПОВЕЩАЕМ О ЛЮБЫХ ОТКЛОНЕНИЯХ ОТ ГРАФИКА ИЛИ ПЕРЕСЕЧЕНИИ КОНТРОЛЬНЫХ ТОЧЕК МАРШРУТА', 'Вы получаете СМС-сообщение с уведомлением о движении автопоезда. ', 'files/catalog/upload/8_loud.svg'),
(10, 4, 'ЖЕЛАНИЕ СОТРУДНИЧАТЬ С НАМИ СНОВА И СНОВА', 'Статистика работы показывает, что 91% клиентов, заинтересованы в продолжении сотрудничества и работают теперь только с нами.', 'files/catalog/upload/9_handshake.svg');

-- --------------------------------------------------------

--
-- Структура таблицы `position_coop`
--

CREATE TABLE `position_coop` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `title` varchar(500) DEFAULT NULL,
  `content` text,
  `img` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `position_coop`
--

INSERT INTO `position_coop` (`id`, `section_id`, `title`, `content`, `img`) VALUES
(1, 5, '1', 'Ваше обращение', 'files/catalog/upload/11_phone.svg'),
(2, 5, '2', 'Обсуждение специфики Вашего груза и условия его перевозки', 'files/catalog/upload/12_bubble.svg'),
(3, 5, '3', 'Согласование и подписание договора между нашими компаниями', 'files/catalog/upload/13_sign.svg'),
(4, 5, '4', 'Доставка Вашего груза до пункта назначения', 'files/catalog/upload/14_truck.svg'),
(5, 5, '5', 'Ваш груз доставлен, обращайтесь к нам снова!', 'files/catalog/upload/15_thumb.svg');

-- --------------------------------------------------------

--
-- Структура таблицы `position_forms`
--

CREATE TABLE `position_forms` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `type_id` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `valid_empty` smallint(6) DEFAULT '0',
  `valid_email` smallint(6) DEFAULT '0',
  `select_options` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `nameid` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `html_before` text,
  `html_after` text,
  `retailcrm_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `position_forms`
--

INSERT INTO `position_forms` (`id`, `section_id`, `name`, `position`, `type_id`, `valid_empty`, `valid_email`, `select_options`, `nameid`, `html_before`, `html_after`, `retailcrm_name`) VALUES
(1, 6, 'Пожалуйста представьтесь', 10, 'text', 1, NULL, NULL, 'name', '<div class=\"col-sm-6\">\n                            <div class=\"form-group\">', '                            </div>', 'name'),
(2, 6, 'Ваш номер телефона', 20, 'text', 1, NULL, NULL, 'phone', '                            <div class=\"form-group\">', '                            </div>\n                    </div>', 'phone'),
(3, 6, 'Жду звонка', 50, 'submit', NULL, NULL, NULL, NULL, '<div class=\"col-xs-12 col-sm-12\">        \n', ' </div>', NULL),
(4, 6, 'Комментарий к обращению', 30, 'memo', 1, NULL, NULL, 'text', '                    <div class=\"col-sm-6\">\n                        <div class=\"form-group\">', '                        </div>\n                    </div>', 'text'),
(6, 10, 'Пожалуйста представьтесь', 10, 'text', 1, NULL, NULL, 'name', '<div class=\"form-group form-group-header\">', '</div>', 'name'),
(7, 10, 'Номер телефона', 20, 'text', 1, NULL, NULL, 'phone', '<div class=\"form-group form-group-header\">', '</div>', 'phone'),
(8, 10, 'E-mail', 30, 'text', 1, 1, NULL, 'email', '<div class=\"form-group form-group-header\">', '</div>', 'email'),
(9, 10, 'ОТПРАВИТЬ', 50, 'submit', NULL, NULL, NULL, NULL, '<div class=\"modal-footer form-group header_fly\">', '</div>', NULL),
(10, 11, 'Пожалуйста представьтесь', 10, 'text', 1, NULL, NULL, 'name', '<div class=\"form-group form-group-footer\">', '</div>', 'name'),
(11, 11, 'Номер телефона', 20, 'text', 1, NULL, NULL, 'phone', '<div class=\"form-group form-group-footer\">', '</div>', 'phone'),
(12, 11, 'E-mail', 30, 'text', 1, 1, NULL, 'email', '<div class=\"form-group form-group-footer\">', '</div>', 'email'),
(13, 11, 'ОТПРАВИТЬ', 50, 'submit', NULL, NULL, NULL, NULL, '<div class=\"modal-footer form-group header_fly\">', '</div>', NULL),
(14, 6, NULL, 40, 'check', 1, NULL, NULL, 'politic_security', '<div class=\"col-xs-12 col-sm-12\">\n<div class=\"secondcheckbox\">\n<label>', '<span>                                   \n</span>\n</label>\n<p>Я даю свое согласие на обработку персональных данных и соглашаюсь с условиями и\n<a href=\"#\"> политикой конфиденциальности\n</a>\n</p>\n</div>\n</div>  ', NULL),
(15, 10, NULL, 40, 'check', 1, NULL, NULL, 'chechbox_policity_header', '<div class=\"thirdcheckbox\">\n                             <label>', '<span>                                   \n                                </span>\n                             </label>\n                             <p>Я даю свое согласие на обработку персональных данных и соглашаюсь с условиями и\n                             <a href=\"#\"> политикой конфиденциальности\n                             </a>\n                             </p>\n                          </div>', NULL),
(16, 11, NULL, 40, 'check', 1, NULL, NULL, 'checkbox_policity_footer', '<div class=\"thirdcheckbox\">\n                             <label>', '<span>                                   \n                                </span>\n                             </label>\n                             <p>Я даю свое согласие на обработку персональных данных и соглашаюсь с условиями и\n                             <a href=\"#\"> политикой конфиденциальности\n                             </a>\n                             </p>\n                          </div>', NULL),
(17, 13, 'Ваш номер телефона', 10, 'text', 1, NULL, NULL, 'phone', '<div class=\"form-group\">\n<div class=\"tel2\">', '</div>', 'phone'),
(18, 13, 'ОТПРАВИТЬ', 20, 'submit', NULL, NULL, NULL, NULL, '<div class=\"yb\">', '</div>', NULL),
(19, 13, NULL, 30, 'check', 1, NULL, NULL, 'check_privaci_promo', '<div class=\"checkb\">\n                             <label>', '<span>                                   \n                                </span>\n                             </label>\n                             <p>Я даю свое согласие на обработку персональных<br> данных и соглашаюсь с условиями\n                             <br>\n                             и\n                             <a href=\"#\"> политикой конфиденциальности\n                             </a>\n                             </p>\n                          </div>\n</div>', NULL),
(20, 14, 'Откуда', 10, 'text', 1, NULL, NULL, 'from', '<div class=\"form-group\">', '</div>', NULL),
(21, 14, 'Куда', 20, 'text', 1, NULL, NULL, 'where', '<div class=\"form-group\">', '</div>', NULL),
(22, 14, 'РАСЧИТАТЬ СТОИМОСТЬ', 30, 'submit', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `position_news`
--

CREATE TABLE `position_news` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datestamp` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `content` longtext COLLATE utf8_unicode_ci,
  `img` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public` tinyint(2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `position_setting`
--

CREATE TABLE `position_setting` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `img` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `public` tinyint(2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `position_slides`
--

CREATE TABLE `position_slides` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `title2` varchar(250) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `visible` tinyint(2) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `position_slides`
--

INSERT INTO `position_slides` (`id`, `section_id`, `img`, `title`, `title2`, `position`, `visible`, `description`) VALUES
(1, 9, 'files/catalog/upload/1fb6207264cae7077866ff57ef05048a.jpg', 'СЕЗОННОЕ СНИЖЕНИЕ ЦЕН НА ГРУЗОПЕРЕВОЗКИ', 'Снижаем цены до 10% на все виды <br> грузоперевозок по России и СНГ', 10, 1, 'mod( \'catalog.action.forms\', array( \'alias\' => \'forma_slajder\' ) )'),
(2, 9, 'files/catalog/upload/4ab0b2a12195f31056d31c482017d6c3.jpg', 'СЕЗОННОЕ СНИЖЕНИЕ ЦЕН НА ГРУЗОПЕРЕВОЗКИ', 'Снижаем цены до 10% на все виды  грузоперевозок по России и СНГ', 20, 1, 'mod( \'catalog.action.forms\', array( \'alias\' => \'forma_slajder\' ) )');

-- --------------------------------------------------------

--
-- Структура таблицы `section_forms`
--

CREATE TABLE `section_forms` (
  `id` int(11) NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `efrom` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `efromname` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `esubject` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `captcha` smallint(6) DEFAULT '0',
  `header_mail` text COLLATE utf8_unicode_ci,
  `title_form` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `success_message` text COLLATE utf8_unicode_ci,
  `html` text COLLATE utf8_unicode_ci,
  `html_id` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class_form` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `method` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'post',
  `action` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `save_table` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `retailcrm_order_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `retailcrm_on` tinyint(2) DEFAULT NULL,
  `ya_metrika_target_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ya_metrika_target_id_button` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ya_metrika_on` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `section_forms`
--

INSERT INTO `section_forms` (`id`, `email`, `efrom`, `efromname`, `esubject`, `captcha`, `header_mail`, `title_form`, `success_message`, `html`, `html_id`, `class_form`, `method`, `action`, `save_table`, `retailcrm_order_method`, `retailcrm_on`, `ya_metrika_target_id`, `ya_metrika_target_id_button`, `ya_metrika_on`) VALUES
(6, 'cefar@mail.ru', NULL, NULL, 'ФОРМУ ОБРАЩЕНИЯ', 0, NULL, NULL, 'Заявка успешно отправлена!', '<p>Имя: {NAME}</p>\n\n<p>Телефон: {PHONE}</p>\n\n<p>Текст: {TEXT}</p>\n', 'f_forma_obracsheniya', NULL, 'ajax-post', 'send_email', NULL, 'f-forma-obracsheniya', 1, NULL, NULL, NULL),
(10, 'cefar@mail.ru', NULL, NULL, 'Заявка -> Хэдер', 0, NULL, NULL, 'Заявка успешно отправлена!', '<p>Имя: {NAME}</p>\n\n<p>Телефон: {PHONE}</p>\n\n<p>email: {EMAIL}</p>\n', 'f_ostavit_zayavku_heder', NULL, 'ajax-post', 'send_email', NULL, 'f-ostavit-zayavku-heder', 1, NULL, NULL, NULL),
(11, 'cefar@mail.ru', NULL, NULL, 'Заявка -> Футер', 0, NULL, NULL, 'Заявка успешно отправлена!', '<p>Имя: {NAME}</p>\n\n<p>Телефон: {PHONE}</p>\n\n<p>email: {MAIL}</p>\n', 'f_ostavit_zayavku_futer', NULL, 'ajax-post', 'send_email', NULL, 'f-ostavit-zayavku-futer', 1, NULL, NULL, NULL),
(13, 'cefar@mail.ru', NULL, NULL, 'Заявка -> Главная страница -> Акция', 0, NULL, NULL, 'Ваша заявка успешно отправлена!', '<p>Телефон: {PHONE}</p>\n', 'f_zayavka_akciya', NULL, 'ajax-post', 'send_email', NULL, 'f-zayavka-akciya', 1, NULL, NULL, NULL),
(14, 'cefar@mail.ru', NULL, NULL, 'Слайдер -> Заказ', 0, NULL, NULL, 'Ваша заявка успешно отправлена!', '<p>Откуда: {FROM}</p>\n\n<p>Куда: {WHERE}</p>\n', 'f_forma_slajder', NULL, 'ajax-post', 'send_email', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `section_forms_settings`
--

CREATE TABLE `section_forms_settings` (
  `id` int(11) NOT NULL,
  `ya_metrika_key` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ya_metrika_id` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ya_metrika_on` tinyint(2) DEFAULT NULL,
  `retailcrm_key` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `retailcrm_on` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `section_forms_settings`
--

INSERT INTO `section_forms_settings` (`id`, `ya_metrika_key`, `ya_metrika_id`, `ya_metrika_on`, `retailcrm_key`, `retailcrm_on`) VALUES
(1, NULL, NULL, NULL, 'RC-14245400185-2', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `section_form_feedback`
--

CREATE TABLE `section_form_feedback` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `efrom` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `efromname` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `esubject` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `section_news`
--

CREATE TABLE `section_news` (
  `id` int(11) NOT NULL,
  `page_size` int(11) DEFAULT NULL,
  `head_title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `tag` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `section_page`
--

CREATE TABLE `section_page` (
  `id` int(11) NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `visible` smallint(6) DEFAULT '1',
  `head_title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `tag` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `section_page`
--

INSERT INTO `section_page` (`id`, `content`, `visible`, `head_title`, `meta_keywords`, `meta_description`, `tag`) VALUES
(8, NULL, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `section_timer`
--

CREATE TABLE `section_timer` (
  `id` int(11) NOT NULL,
  `datestamp_start` int(11) DEFAULT NULL,
  `datestamp_over` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `section_timer`
--

INSERT INTO `section_timer` (`id`, `datestamp_start`, `datestamp_over`) VALUES
(12, 1509476400, 1510686000);

-- --------------------------------------------------------

--
-- Структура таблицы `security_role`
--

CREATE TABLE `security_role` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `security_role`
--

INSERT INTO `security_role` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Структура таблицы `security_user`
--

CREATE TABLE `security_user` (
  `id` int(11) NOT NULL,
  `disabled` smallint(6) DEFAULT '0',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `security_user`
--

INSERT INTO `security_user` (`id`, `disabled`, `name`, `password`) VALUES
(1, 0, 'Bliznes', '65646a951ea29392a378ce4043af3e74');

-- --------------------------------------------------------

--
-- Структура таблицы `security_user_role`
--

CREATE TABLE `security_user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `security_user_role`
--

INSERT INTO `security_user_role` (`user_id`, `role_id`) VALUES
(1, 1),
(1, 2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `catalog_section`
--
ALTER TABLE `catalog_section`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `forms_field_action`
--
ALTER TABLE `forms_field_action`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `forms_field_type`
--
ALTER TABLE `forms_field_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menus_item`
--
ALTER TABLE `menus_item`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_bonus`
--
ALTER TABLE `position_bonus`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_coop`
--
ALTER TABLE `position_coop`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_forms`
--
ALTER TABLE `position_forms`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_news`
--
ALTER TABLE `position_news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_setting`
--
ALTER TABLE `position_setting`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_slides`
--
ALTER TABLE `position_slides`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_forms`
--
ALTER TABLE `section_forms`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_forms_settings`
--
ALTER TABLE `section_forms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_form_feedback`
--
ALTER TABLE `section_form_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_news`
--
ALTER TABLE `section_news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_page`
--
ALTER TABLE `section_page`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_timer`
--
ALTER TABLE `section_timer`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_role`
--
ALTER TABLE `security_role`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_user`
--
ALTER TABLE `security_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`name`);

--
-- Индексы таблицы `security_user_role`
--
ALTER TABLE `security_user_role`
  ADD PRIMARY KEY (`user_id`,`role_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `catalog_section`
--
ALTER TABLE `catalog_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `menus_item`
--
ALTER TABLE `menus_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `position_bonus`
--
ALTER TABLE `position_bonus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `position_coop`
--
ALTER TABLE `position_coop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `position_forms`
--
ALTER TABLE `position_forms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблицы `position_news`
--
ALTER TABLE `position_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `position_setting`
--
ALTER TABLE `position_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `position_slides`
--
ALTER TABLE `position_slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `section_timer`
--
ALTER TABLE `section_timer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `security_user`
--
ALTER TABLE `security_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
