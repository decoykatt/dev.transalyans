-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Дек 04 2017 г., 02:14
-- Версия сервера: 5.7.15
-- Версия PHP: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `dev.transalyans`
--

-- --------------------------------------------------------

--
-- Структура таблицы `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `alias` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `html` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `banner`
--

INSERT INTO `banner` (`id`, `alias`, `name`, `html`) VALUES
(1, 'phone', 'Телефон', '+7 (992) 001 91 38'),
(2, 'email', 'Мэил', 'sale@gktas.ru');

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_section`
--

CREATE TABLE `catalog_section` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) DEFAULT NULL,
  `alias` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position_table` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `section_table` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `children_tpl` text COLLATE utf8_unicode_ci,
  `leaf` smallint(6) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `catalog_section`
--

INSERT INTO `catalog_section` (`id`, `parent_id`, `title`, `position`, `alias`, `position_table`, `section_table`, `children_tpl`, `leaf`) VALUES
(1, NULL, 'Формы', NULL, 'forms_section', NULL, 'section_forms_settings', '<tpl>\r\n  <section>section_forms</section>\r\n  <position>position_forms</position>\r\n  <leaf>1</leaf>\r\n</tpl>', NULL),
(16, NULL, 'Города', 7, 'goroda', 'position_setting', '', '', NULL),
(4, NULL, 'Что вы получаете?', 3, 'chto-vy-poluchaete', 'position_bonus', '', '', NULL),
(5, NULL, 'Как происходит сотрудничество', 4, 'kak-proishodit-sotrudnichestvo', 'position_coop', '', '', NULL),
(6, 1, 'Форма обращения', 1, 'forma_obracsheniya', 'position_forms', 'section_forms', '', 1),
(15, NULL, 'Изменение цветов', 6, 'izmenenie-cvetov', '', 'section_change_color', '', NULL),
(9, NULL, 'Слайдер', 6, 'slajder', 'position_slides', '', '', NULL),
(10, 1, 'Оставить заявку хэдер', 2, 'ostavit_zayavku_heder', 'position_forms', 'section_forms', '', 1),
(11, 1, 'Оставить заявку футер', 3, 'ostavit_zayavku_futer', 'position_forms', 'section_forms', '', 1),
(12, NULL, 'Таймер', 7, 'tajmer', '', 'section_timer', '', NULL),
(13, 1, 'Заявка акция', 4, 'zayavka_akciya', 'position_forms', 'section_forms', '', 1),
(14, 1, 'Форма слайдер', 5, 'forma_slajder', 'position_forms', 'section_forms', '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `forms_field_action`
--

CREATE TABLE `forms_field_action` (
  `id` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `position` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `forms_field_action`
--

INSERT INTO `forms_field_action` (`id`, `name`, `position`) VALUES
('auth', 'Авторизация', 2),
('is_active', 'Активация учетной запись', 4),
('register', 'Регистрация', 1),
('restore_pass', 'Восстановить пароль', 3),
('send_email', 'Отправка на email', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `forms_field_type`
--

CREATE TABLE `forms_field_type` (
  `id` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `position` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `forms_field_type`
--

INSERT INTO `forms_field_type` (`id`, `name`, `position`) VALUES
('check', 'Флаг', 7),
('date', 'Дата', 5),
('hidden', 'Скрытое поле', 11),
('html', 'Произвольный HTML', 15),
('info', 'Информация', 14),
('label', 'Подпись', 13),
('link', 'Ссылка', 10),
('memo', 'Многострочный текст', 2),
('pwd', 'Пароль', 6),
('radiobox', 'Радиобокс', 12),
('select', 'Выпадающий список', 3),
('submit', 'Кнопка подачи запроса', 8),
('text', 'Текстовая строка', 1),
('upload', 'Обзор', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `menus`
--

INSERT INTO `menus` (`id`, `name`, `title`) VALUES
(1, 'header-menu', 'Меню шапка'),
(2, 'footer', 'Меню футер');

-- --------------------------------------------------------

--
-- Структура таблицы `menus_item`
--

CREATE TABLE `menus_item` (
  `id` int(11) NOT NULL,
  `menus_id` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `visible` smallint(6) NOT NULL DEFAULT '0',
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `type_link` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attr` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_src` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `menus_item`
--

INSERT INTO `menus_item` (`id`, `menus_id`, `title`, `parent_id`, `position`, `visible`, `type`, `type_id`, `type_link`, `attr`, `img_src`) VALUES
(1, 1, 'О компании', NULL, 2, 1, '_link', NULL, '#main1', 'class="jakor"', NULL),
(2, 1, 'Преимущества', NULL, 3, 1, '_link', NULL, '#adv21', 'class="jakor"', NULL),
(3, 1, 'Сотрудничество', NULL, 4, 1, '_link', NULL, '#sotr', 'class="jakor"', NULL),
(4, 1, 'Контакты', NULL, 5, 1, '_link', NULL, '#down', 'class="jakor"', NULL),
(5, 1, 'Акции', NULL, 6, 1, '_link', NULL, '#maims', 'class="jakor"', NULL),
(19, 1, '+7 (992) 001 91 38', NULL, 7, 1, '_link', NULL, 'tel:+7 (992) 001 91 38', 'class="header-tel"', NULL),
(11, 2, 'О компании', NULL, 2, 1, '_link', NULL, '#main1', 'class="jakor"', NULL),
(12, 2, 'Преимущества', NULL, 3, 1, '_link', NULL, '#adv21', 'class="jakor"', NULL),
(13, 2, 'Сотрудничество', NULL, 4, 1, '_link', NULL, '#sotr', 'class="jakor"', NULL),
(14, 2, 'Контакты', NULL, 5, 1, '_link', NULL, '#down', 'class="jakor"', NULL),
(15, 2, 'Акции', NULL, 6, 1, '_link', NULL, '#maims', 'class="jakor"', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title2` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `position` int(11) NOT NULL,
  `visible` smallint(6) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `alias` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plugins` text COLLATE utf8_unicode_ci,
  `mainpage` smallint(6) NOT NULL DEFAULT '0',
  `announcement` text COLLATE utf8_unicode_ci,
  `fileupload` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_src` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `redirect` smallint(6) DEFAULT NULL,
  `head_title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `tag` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `title`, `title2`, `content`, `position`, `visible`, `parent_id`, `alias`, `template`, `plugins`, `mainpage`, `announcement`, `fileupload`, `img_src`, `redirect`, `head_title`, `meta_keywords`, `meta_description`, `tag`) VALUES
(1, 'Главная', '', '<h1>ГРУППА КОМПАНИЙ &laquo;ТРАНСАЛЬЯНС&raquo;</h1>\n\n<div class="liline"></div>\n\n<p>Оказываем транспортные услуги по доставке грузов в Екатеринбург и из Екатеринбурга по России. Для перевозки грузов используется автомобильный транспорт. Нашим основным направлением является &mdash; осуществление транспортировки негабаритных и тяжеловесных грузов на территории России.</p>\n\n<p>Перевозка негабаритных грузов: крупногабаритных и тяжеловесных имеет особую специфику организации. Этот вид услуги требует детального знания особенностей эксплутации грузового автомобильного транспорта, дорожного законодательства и особенностей автотрасс предпологаемого маршрута. Специалисты компании хорошо знакомы с особенностями перевозок негабарита различных видов: техники, емкостей, бытовок, ж.б. конструкций, профильных материалов, станков, оборудования.</p>\n\n<ul>\n	<li>Железнодорожные перевозки</li>\n	<li>Мультимодальные перевозки</li>\n	<li>Пассажирские автоперевозки</li>\n	<li>Складские услуги</li>\n	<li>Таможенные услуги</li>\n</ul>\n', 1, 0, NULL, 'index', 'main.tpl.php', '', 1, '', NULL, NULL, NULL, '', '', '', ''),
(2, 'Товарное предложение', '', '<h1>ОПИСАНИЕ МАРШРУТА</h1>\n\n<div class="liline"></div>\n\n<p>Разделяют 2 города 205 км. Связывает две уральских столицы часть трассы М-5 «Урал» и трасса М 36. Расстояние (в зависимости \nот конкретных адресов в городах) преодолевается легко. В рекомендованном скоростном режиме дорога займет 3 часа.</p>', 2, 1, NULL, 'tovarnoe-predlozhenie', 'offer.tpl.php', '', 0, '', NULL, NULL, NULL, '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `position_bonus`
--

CREATE TABLE `position_bonus` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `title` varchar(500) DEFAULT NULL,
  `content` text,
  `img` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `position_bonus`
--

INSERT INTO `position_bonus` (`id`, `section_id`, `title`, `content`, `img`) VALUES
(5, 4, 'ЭКОНОМИЯ ВАШЕГО ВРЕМЕНИ', 'Весь процесс подготовки документов, планирование маршрута и оформление разрешений, мы берем на себя.', 'files/catalog/upload/4_clock.svg'),
(6, 4, 'ПОЛНАЯ ЮРИДИЧЕСКАЯ ОТВЕТСТВЕННОСТЬ ЗА ВАШ ГРУЗ', 'Договор составлен в Вашу пользу, мы берем на себя полную ответственность о сохранности Вашего груза и гарантию доставки в оговоренные сроки. ', 'files/catalog/upload/5_lawyer.svg'),
(7, 4, 'БЕЗОПАСНОСТЬ В ПУТИ СЛЕДОВАНИЯ', 'Предоставление машины прикрытия или машины сопровождения. ', 'files/catalog/upload/6_shield.svg'),
(8, 4, 'ОНЛАЙН МОНИТОРИНГ ВАШЕГО ГРУЗА', 'Хотите уточнить где находится Ваш груз в данный момент? Звоните! Наш оператор в режиме 24 часа всегда готов проинформировать по какой трассе в данный момент времени движется груз и ориентировочное время в пути. ', 'files/catalog/upload/7_speak.svg'),
(9, 4, 'ПРЕДВАРИТЕЛЬНО ОПОВЕЩАЕМ О ЛЮБЫХ ОТКЛОНЕНИЯХ ОТ ГРАФИКА ИЛИ ПЕРЕСЕЧЕНИИ КОНТРОЛЬНЫХ ТОЧЕК МАРШРУТА', 'Вы получаете СМС-сообщение с уведомлением о движении автопоезда. ', 'files/catalog/upload/8_loud.svg'),
(10, 4, 'ЖЕЛАНИЕ СОТРУДНИЧАТЬ С НАМИ СНОВА И СНОВА', 'Статистика работы показывает, что 91% клиентов, заинтересованы в продолжении сотрудничества и работают теперь только с нами.', 'files/catalog/upload/9_handshake.svg');

-- --------------------------------------------------------

--
-- Структура таблицы `position_coop`
--

CREATE TABLE `position_coop` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `title` varchar(500) DEFAULT NULL,
  `content` text,
  `img` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `position_coop`
--

INSERT INTO `position_coop` (`id`, `section_id`, `title`, `content`, `img`) VALUES
(1, 5, '1', 'Ваше обращение', 'files/catalog/upload/11_phone.svg'),
(2, 5, '2', 'Обсуждение специфики Вашего груза и условия его перевозки', 'files/catalog/upload/12_bubble.svg'),
(3, 5, '3', 'Согласование и подписание договора между нашими компаниями', 'files/catalog/upload/13_sign.svg'),
(4, 5, '4', 'Доставка Вашего груза до пункта назначения', 'files/catalog/upload/14_truck.svg'),
(5, 5, '5', 'Ваш груз доставлен, обращайтесь к нам снова!', 'files/catalog/upload/15_thumb.svg');

-- --------------------------------------------------------

--
-- Структура таблицы `position_forms`
--

CREATE TABLE `position_forms` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `type_id` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `valid_empty` smallint(6) DEFAULT '0',
  `valid_email` smallint(6) DEFAULT '0',
  `select_options` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `nameid` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `html_before` text,
  `html_after` text,
  `retailcrm_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `position_forms`
--

INSERT INTO `position_forms` (`id`, `section_id`, `name`, `position`, `type_id`, `valid_empty`, `valid_email`, `select_options`, `nameid`, `html_before`, `html_after`, `retailcrm_name`) VALUES
(1, 6, 'Пожалуйста представьтесь', 10, 'text', 1, NULL, NULL, 'name', '<div class="col-sm-6">\n                            <div class="form-group">', '                            </div>', 'name'),
(2, 6, 'Ваш номер телефона', 20, 'text', 1, NULL, NULL, 'phone', '                            <div class="form-group">', '                            </div>\n                    </div>', 'phone'),
(3, 6, 'Жду звонка', 50, 'submit', NULL, NULL, NULL, NULL, '<div class="col-xs-12 col-sm-12">        \n', ' </div>', NULL),
(4, 6, 'Комментарий к обращению', 30, 'memo', 1, NULL, NULL, 'text', '                    <div class="col-sm-6">\n                        <div class="form-group">', '                        </div>\n                    </div>', 'text'),
(6, 10, 'Пожалуйста представьтесь', 10, 'text', 1, NULL, NULL, 'name', '<div class="form-group form-group-header">', '</div>', 'name'),
(7, 10, 'Номер телефона', 20, 'text', 1, NULL, NULL, 'phone', '<div class="form-group form-group-header">', '</div>', 'phone'),
(8, 10, 'E-mail', 30, 'text', 1, 1, NULL, 'email', '<div class="form-group form-group-header">', '</div>', 'email'),
(9, 10, 'ОТПРАВИТЬ', 50, 'submit', NULL, NULL, NULL, NULL, '<div class="modal-footer form-group header_fly">', '</div>', NULL),
(10, 11, 'Пожалуйста представьтесь', 10, 'text', 1, NULL, NULL, 'name', '<div class="form-group form-group-footer">', '</div>', 'name'),
(11, 11, 'Номер телефона', 20, 'text', 1, NULL, NULL, 'phone', '<div class="form-group form-group-footer">', '</div>', 'phone'),
(12, 11, 'E-mail', 30, 'text', 1, 1, NULL, 'email', '<div class="form-group form-group-footer">', '</div>', 'email'),
(13, 11, 'ОТПРАВИТЬ', 50, 'submit', NULL, NULL, NULL, NULL, '<div class="modal-footer form-group header_fly">', '</div>', NULL),
(14, 6, NULL, 40, 'check', 1, NULL, NULL, 'politic_security', '<div class="col-xs-12 col-sm-12">\n<div class="secondcheckbox">\n<label>', '<span>                                   \n</span>\n</label>\n<p>Я даю свое согласие на обработку персональных данных и соглашаюсь с условиями и\n<a href="#"> политикой конфиденциальности\n</a>\n</p>\n</div>\n</div>  ', NULL),
(15, 10, NULL, 40, 'check', 1, NULL, NULL, 'chechbox_policity_header', '<div class="thirdcheckbox">\n                             <label>', '<span>                                   \n                                </span>\n                             </label>\n                             <p>Я даю свое согласие на обработку персональных данных и соглашаюсь с условиями и\n                             <a href="#"> политикой конфиденциальности\n                             </a>\n                             </p>\n                          </div>', NULL),
(16, 11, NULL, 40, 'check', 1, NULL, NULL, 'checkbox_policity_footer', '<div class="thirdcheckbox">\n                             <label>', '<span>                                   \n                                </span>\n                             </label>\n                             <p>Я даю свое согласие на обработку персональных данных и соглашаюсь с условиями и\n                             <a href="#"> политикой конфиденциальности\n                             </a>\n                             </p>\n                          </div>', NULL),
(17, 13, 'Ваш номер телефона', 10, 'text', 1, NULL, NULL, 'phone', '<div class="form-group">\n<div class="tel2">', '</div>', 'phone'),
(18, 13, 'ОТПРАВИТЬ', 20, 'submit', NULL, NULL, NULL, NULL, '<div class="yb">', '</div>', NULL),
(19, 13, NULL, 30, 'check', 1, NULL, NULL, 'check_privaci_promo', '<div class="checkb">\n                             <label>', '<span>                                   \n                                </span>\n                             </label>\n                             <p>Я даю свое согласие на обработку персональных<br> данных и соглашаюсь с условиями\n                             <br>\n                             и\n                             <a href="#"> политикой конфиденциальности\n                             </a>\n                             </p>\n                          </div>\n</div>', NULL),
(20, 14, 'Откуда', 10, 'text', 1, NULL, NULL, 'from', '<div class="form-group">', '</div>', NULL),
(21, 14, 'Куда', 20, 'text', 1, NULL, NULL, 'where', '<div class="form-group">', '</div>', NULL),
(22, 14, 'РАСЧИТАТЬ СТОИМОСТЬ', 30, 'submit', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `position_news`
--

CREATE TABLE `position_news` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datestamp` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `content` longtext COLLATE utf8_unicode_ci,
  `img` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public` tinyint(2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `position_setting`
--

CREATE TABLE `position_setting` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `img` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `public` tinyint(2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `position_setting`
--

INSERT INTO `position_setting` (`id`, `section_id`, `title`, `value`, `type`, `description`, `img`, `position`, `public`) VALUES
(1, 16, 'Варгаши', NULL, NULL, NULL, NULL, 10, 1),
(2, 16, 'Глядянское', NULL, NULL, NULL, NULL, 20, 1),
(3, 16, 'Далматово', NULL, NULL, NULL, NULL, 30, 1),
(4, 16, 'Катайск', NULL, NULL, NULL, NULL, 40, 1),
(5, 16, 'Кетово', NULL, NULL, NULL, NULL, 50, 1),
(6, 16, 'Курган', NULL, NULL, NULL, NULL, 60, 1),
(7, 16, 'Куртамыш', NULL, NULL, NULL, NULL, 70, 1),
(8, 16, 'Лебяжье', NULL, NULL, NULL, NULL, 80, 1),
(9, 16, 'Макушино', NULL, NULL, NULL, NULL, 90, 1),
(10, 16, 'Мишкино', NULL, NULL, NULL, NULL, 100, 1),
(11, 16, 'Макроусово', NULL, NULL, NULL, NULL, 110, 1),
(12, 16, 'Петухово', NULL, NULL, NULL, NULL, 120, 1),
(13, 16, 'Половинное', NULL, NULL, NULL, NULL, 130, 1),
(14, 16, 'Сафакулево', NULL, NULL, NULL, NULL, 140, 1),
(15, 16, 'Частоозерье', NULL, NULL, NULL, NULL, 150, 1),
(16, 16, 'Шадринск', NULL, NULL, NULL, NULL, 160, 1),
(17, 16, 'Шатрово', NULL, NULL, NULL, NULL, 170, 1),
(18, 16, 'Шумиха', NULL, NULL, NULL, NULL, 180, 1),
(19, 16, 'Щучье', NULL, NULL, NULL, NULL, 190, 1),
(20, 16, 'Уксянское', NULL, NULL, NULL, NULL, 200, 1),
(21, 16, 'Целинное', NULL, NULL, NULL, NULL, 210, 1),
(22, 16, 'Юргамыш', NULL, NULL, NULL, NULL, 220, 1),
(23, 16, 'Алапаевск', NULL, NULL, NULL, NULL, 230, 1),
(24, 16, 'Артемовский', NULL, NULL, NULL, NULL, 240, 1),
(25, 16, 'Асбест', NULL, NULL, NULL, NULL, 250, 1),
(26, 16, 'Бисерть', NULL, NULL, NULL, NULL, 260, 1),
(27, 16, 'Богданович', NULL, NULL, NULL, NULL, 270, 1),
(28, 16, 'Верхотурье', NULL, NULL, NULL, NULL, 280, 1),
(29, 16, 'Верх-Нейвинск', NULL, NULL, NULL, NULL, 290, 1),
(30, 16, 'Верхняя Пышма', NULL, NULL, NULL, NULL, 300, 1),
(31, 16, 'Верхняя Салда', NULL, NULL, NULL, NULL, 310, 1),
(32, 16, 'Верхний Тагил', NULL, NULL, NULL, NULL, 320, 1),
(33, 16, 'Волчанск', NULL, NULL, NULL, NULL, 330, 1),
(34, 16, 'Дегтярск', NULL, NULL, NULL, NULL, 340, 1),
(35, 16, 'Качканар', NULL, NULL, NULL, NULL, 350, 1),
(36, 16, 'Екатеринбург', NULL, NULL, NULL, NULL, 360, 1),
(37, 16, 'Заречный', NULL, NULL, NULL, NULL, 370, 1),
(38, 16, 'Ирбит', NULL, NULL, NULL, NULL, 380, 1),
(39, 16, 'Каменск-Уральский', NULL, NULL, NULL, NULL, 390, 1),
(40, 16, 'Камышлов', NULL, NULL, NULL, NULL, 400, 1),
(41, 16, 'Карпинск', NULL, NULL, NULL, NULL, 410, 1),
(42, 16, 'Кировград', NULL, NULL, NULL, NULL, 420, 1),
(43, 16, 'Краснотурьинск', NULL, NULL, NULL, NULL, 430, 1),
(44, 16, 'Красноуральск', NULL, NULL, NULL, NULL, 440, 1),
(45, 16, 'Красноуфимск', NULL, NULL, NULL, NULL, 450, 1),
(46, 16, 'Кушва', NULL, NULL, NULL, NULL, 460, 1),
(47, 16, 'Михайловск', NULL, NULL, NULL, NULL, 470, 1),
(48, 16, 'Невьянск', NULL, NULL, NULL, NULL, 480, 1),
(49, 16, 'Нижние Серги', NULL, NULL, NULL, NULL, 490, 1),
(50, 16, 'Нижний Тагил', NULL, NULL, NULL, NULL, 500, 1),
(51, 16, 'Новая Ляля', NULL, NULL, NULL, NULL, 510, 1),
(52, 16, 'Новоуральск', NULL, NULL, NULL, NULL, 520, 1),
(53, 16, 'Первоуральск', NULL, NULL, NULL, NULL, 530, 1),
(54, 16, 'Полевской', NULL, NULL, NULL, NULL, 540, 1),
(55, 16, 'Ревда', NULL, NULL, NULL, NULL, 550, 1),
(56, 16, 'Реж', NULL, NULL, NULL, NULL, 560, 1),
(57, 16, 'Серов', NULL, NULL, NULL, NULL, 570, 1),
(58, 16, 'Среднеуральск', NULL, NULL, NULL, NULL, 580, 1),
(59, 16, 'Староуткинск', NULL, NULL, NULL, NULL, 590, 1),
(60, 16, 'Сысерть', NULL, NULL, NULL, NULL, 600, 1),
(61, 16, 'Тавда', NULL, NULL, NULL, NULL, 610, 1),
(62, 16, 'Талица', NULL, NULL, NULL, NULL, 620, 1),
(63, 16, 'Туринск', NULL, NULL, NULL, NULL, 630, 1),
(64, 16, 'Берёзовский', NULL, NULL, NULL, NULL, 640, 1),
(65, 16, 'Лесной', NULL, NULL, NULL, NULL, 650, 1),
(66, 16, 'Заводоуковск', NULL, NULL, NULL, NULL, 660, 1),
(67, 16, 'Горячие источники', NULL, NULL, NULL, NULL, 670, 1),
(68, 16, 'Исетское', NULL, NULL, NULL, NULL, 680, 1),
(69, 16, 'Ишим', NULL, NULL, NULL, NULL, 690, 1),
(70, 16, 'Тобольск', NULL, NULL, NULL, NULL, 700, 1),
(71, 16, 'Тюмень', NULL, NULL, NULL, NULL, 710, 1),
(72, 16, 'Уват', NULL, NULL, NULL, NULL, 720, 1),
(73, 16, 'Ялуторовск', NULL, NULL, NULL, NULL, 730, 1),
(74, 16, 'Нижневартовск', NULL, NULL, NULL, NULL, 740, 1),
(75, 16, 'Сургут', NULL, NULL, NULL, NULL, 750, 1),
(76, 16, 'Нефтеюганск', NULL, NULL, NULL, NULL, 760, 1),
(77, 16, 'Когалым', NULL, NULL, NULL, NULL, 770, 1),
(78, 16, 'Нягань', NULL, NULL, NULL, NULL, 780, 1),
(79, 16, 'Ханты-Мансийск', NULL, NULL, NULL, NULL, 790, 1),
(80, 16, 'Белоярский', NULL, NULL, NULL, NULL, 800, 1),
(81, 16, 'Лангепас', NULL, NULL, NULL, NULL, 810, 1),
(82, 16, 'Лянтор', NULL, NULL, NULL, NULL, 820, 1),
(83, 16, 'Мегион', NULL, NULL, NULL, NULL, 830, 1),
(84, 16, 'Покачи', NULL, NULL, NULL, NULL, 840, 1),
(85, 16, 'Пыть-Ях', NULL, NULL, NULL, NULL, 850, 1),
(86, 16, 'Радужный', NULL, NULL, NULL, NULL, 860, 1),
(87, 16, 'Советский', NULL, NULL, NULL, NULL, 870, 1),
(88, 16, 'Урай', NULL, NULL, NULL, NULL, 880, 1),
(89, 16, 'Югорск', NULL, NULL, NULL, NULL, 890, 1),
(90, 16, 'Копейск', NULL, NULL, NULL, NULL, 900, 1),
(91, 16, 'Аргаяш', NULL, NULL, NULL, NULL, 910, 1),
(92, 16, 'Аша', NULL, NULL, NULL, NULL, 920, 1),
(93, 16, 'Байрамгулово', NULL, NULL, NULL, NULL, 930, 1),
(94, 16, 'Бакал', NULL, NULL, NULL, NULL, 940, 1),
(95, 16, 'Белоусово', NULL, NULL, NULL, NULL, 950, 1),
(96, 16, 'Бреды', NULL, NULL, NULL, NULL, 960, 1),
(97, 16, 'Варламово', NULL, NULL, NULL, NULL, 970, 1),
(98, 16, 'Варна', NULL, NULL, NULL, NULL, 980, 1),
(99, 16, 'Верхнеуральск', NULL, NULL, NULL, NULL, 990, 1),
(100, 16, 'Вишневогорск', NULL, NULL, NULL, NULL, 1000, 1),
(101, 16, 'Верхний Уфалей', NULL, NULL, NULL, NULL, 1010, 1),
(102, 16, 'Еманжелинск', NULL, NULL, NULL, NULL, 1020, 1),
(103, 16, 'Еткуль', NULL, NULL, NULL, NULL, 1030, 1),
(104, 16, 'Златоуст', NULL, NULL, NULL, NULL, 1040, 1),
(105, 16, 'Зюраткуль', NULL, NULL, NULL, NULL, 1050, 1),
(106, 16, 'Карталы', NULL, NULL, NULL, NULL, 1060, 1),
(107, 16, 'Касли', NULL, NULL, NULL, NULL, 1070, 1),
(108, 16, 'Катав-Ивановск', NULL, NULL, NULL, NULL, 1080, 1),
(109, 16, 'Карабаш', NULL, NULL, NULL, NULL, 1090, 1),
(110, 16, 'Кизильское', NULL, NULL, NULL, NULL, 1100, 1),
(111, 16, 'Коелга', NULL, NULL, NULL, NULL, 1110, 1),
(112, 16, 'Коркино', NULL, NULL, NULL, NULL, 1120, 1),
(113, 16, 'Кузнецкое', NULL, NULL, NULL, NULL, 1130, 1),
(114, 16, 'Кунашак', NULL, NULL, NULL, NULL, 1140, 1),
(115, 16, 'Куса', NULL, NULL, NULL, NULL, 1150, 1),
(116, 16, 'Кыштым', NULL, NULL, NULL, NULL, 1160, 1),
(117, 16, 'Магнитогорск', NULL, NULL, NULL, NULL, 1170, 1),
(118, 16, 'Магнитка', NULL, NULL, NULL, NULL, 1180, 1),
(119, 16, 'Миасс', NULL, NULL, NULL, NULL, 1190, 1),
(120, 16, 'Миасское', NULL, NULL, NULL, NULL, 1200, 1),
(121, 16, 'Миньяр', NULL, NULL, NULL, NULL, 1210, 1),
(122, 16, 'Мирный, Чебаркульский район', NULL, NULL, NULL, NULL, 1220, 1),
(123, 16, 'Муслюмово', NULL, NULL, NULL, NULL, 1230, 1),
(124, 16, 'Новогорный', NULL, NULL, NULL, NULL, 1240, 1),
(125, 16, 'Нязепетровск', NULL, NULL, NULL, NULL, 1250, 1),
(126, 16, 'Озерск', NULL, NULL, NULL, NULL, 1260, 1),
(127, 16, 'Октябрьское', NULL, NULL, NULL, NULL, 1270, 1),
(128, 16, 'Пласт', NULL, NULL, NULL, NULL, 1280, 1),
(129, 16, 'Полетаево', NULL, NULL, NULL, NULL, 1290, 1),
(130, 16, 'Роза', NULL, NULL, NULL, NULL, 1300, 1),
(131, 16, 'Сатка', NULL, NULL, NULL, NULL, 1310, 1),
(132, 16, 'Сим', NULL, NULL, NULL, NULL, 1320, 1),
(133, 16, 'Снежинск', NULL, NULL, NULL, NULL, 1330, 1),
(134, 16, 'Троицк', NULL, NULL, NULL, NULL, 1340, 1),
(135, 16, 'Трехгорный', NULL, NULL, NULL, NULL, 1350, 1),
(136, 16, 'Тургояк', NULL, NULL, NULL, NULL, 1360, 1),
(137, 16, 'Чебаркуль', NULL, NULL, NULL, NULL, 1370, 1),
(138, 16, 'Чесма', NULL, NULL, NULL, NULL, 1380, 1),
(139, 16, 'Уйское', NULL, NULL, NULL, NULL, 1390, 1),
(140, 16, 'Усть-Катав', NULL, NULL, NULL, NULL, 1400, 1),
(141, 16, 'Фершампенуаз', NULL, NULL, NULL, NULL, 1410, 1),
(142, 16, 'Юрюзань', NULL, NULL, NULL, NULL, 1420, 1),
(143, 16, 'Южноуральск', NULL, NULL, NULL, NULL, 1430, 1),
(144, 16, 'Москва', NULL, NULL, NULL, NULL, 1440, 1),
(145, 16, 'Санкт-Петербург', NULL, NULL, NULL, NULL, 1450, 1),
(146, 16, 'Новосибирск', NULL, NULL, NULL, NULL, 1460, 1),
(147, 16, 'Екатеринбург', NULL, NULL, NULL, NULL, 1470, 1),
(148, 16, 'Нижний Новгород', NULL, NULL, NULL, NULL, 1480, 1),
(149, 16, 'Казань', NULL, NULL, NULL, NULL, 1490, 1),
(150, 16, 'Челябинск', NULL, NULL, NULL, NULL, 1500, 1),
(151, 16, 'Омск', NULL, NULL, NULL, NULL, 1510, 1),
(152, 16, 'Самара', NULL, NULL, NULL, NULL, 1520, 1),
(153, 16, 'Ростов-на-Дону', NULL, NULL, NULL, NULL, 1530, 1),
(154, 16, 'Стерлитамак', NULL, NULL, NULL, NULL, 1540, 1),
(155, 16, 'Уфа', NULL, NULL, NULL, NULL, 1550, 1),
(156, 16, 'Учалы', NULL, NULL, NULL, NULL, 1560, 1),
(157, 16, 'Красноярск', NULL, NULL, NULL, NULL, 1570, 1),
(158, 16, 'Пермь', NULL, NULL, NULL, NULL, 1580, 1),
(159, 16, 'Воронеж', NULL, NULL, NULL, NULL, 1590, 1),
(160, 16, 'Волгоград', NULL, NULL, NULL, NULL, 1600, 1),
(161, 16, 'Краснодар', NULL, NULL, NULL, NULL, 1610, 1),
(162, 16, 'Саратов', NULL, NULL, NULL, NULL, 1620, 1),
(163, 16, 'Тюмень', NULL, NULL, NULL, NULL, 1630, 1),
(164, 16, 'Тольятти', NULL, NULL, NULL, NULL, 1640, 1),
(165, 16, 'Астана', NULL, NULL, NULL, NULL, 1650, 1),
(166, 16, 'Карабалык', NULL, NULL, NULL, NULL, 1660, 1),
(167, 16, 'Кокшетау', NULL, NULL, NULL, NULL, 1670, 1),
(168, 16, 'Костанай', NULL, NULL, NULL, NULL, 1680, 1),
(169, 16, 'Петропавловск', NULL, NULL, NULL, NULL, 1690, 1),
(170, 16, 'Рудный', NULL, NULL, NULL, NULL, 1700, 1),
(171, 16, 'Ижевск', NULL, NULL, NULL, NULL, 1710, 1),
(172, 16, 'Казань', NULL, NULL, NULL, NULL, 1720, 1),
(173, 16, 'Кунгур', NULL, NULL, NULL, NULL, 1730, 1),
(174, 16, 'Лангепас', NULL, NULL, NULL, NULL, 1740, 1),
(175, 16, 'Москва', NULL, NULL, NULL, NULL, 1750, 1),
(176, 16, 'Мегион', NULL, NULL, NULL, NULL, 1760, 1),
(177, 16, 'Набережные Челны', NULL, NULL, NULL, NULL, 1770, 1),
(178, 16, 'Нефтеюганск', NULL, NULL, NULL, NULL, 1780, 1),
(179, 16, 'Нижневартовск', NULL, NULL, NULL, NULL, 1790, 1),
(180, 16, 'Омск', NULL, NULL, NULL, NULL, 1800, 1),
(181, 16, 'Оренбург', NULL, NULL, NULL, NULL, 1810, 1),
(182, 16, 'Орск', NULL, NULL, NULL, NULL, 1820, 1),
(183, 16, 'Пермь', NULL, NULL, NULL, NULL, 1830, 1),
(184, 16, 'Самара', NULL, NULL, NULL, NULL, 1840, 1),
(185, 16, 'Саликамск', NULL, NULL, NULL, NULL, 1850, 1),
(186, 16, 'Сургут', NULL, NULL, NULL, NULL, 1860, 1),
(187, 16, 'Тольятти', NULL, NULL, NULL, NULL, 1870, 1),
(188, 16, 'Ульяновск', NULL, NULL, NULL, NULL, 1880, 1),
(189, 16, 'Ханты-Мансийск', NULL, NULL, NULL, NULL, 1890, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `position_slides`
--

CREATE TABLE `position_slides` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `title2` varchar(250) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `visible` tinyint(2) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `position_slides`
--

INSERT INTO `position_slides` (`id`, `section_id`, `img`, `title`, `title2`, `position`, `visible`, `description`) VALUES
(1, 9, 'files/catalog/upload/1fb6207264cae7077866ff57ef05048a.jpg', 'СЕЗОННОЕ СНИЖЕНИЕ ЦЕН НА ГРУЗОПЕРЕВОЗКИ', 'Снижаем цены до 10% на все виды <br> грузоперевозок по России и СНГ', 10, 1, 'mod( \'catalog.action.forms\', array( \'alias\' => \'forma_slajder\' ) )'),
(2, 9, 'files/catalog/upload/4ab0b2a12195f31056d31c482017d6c3.jpg', 'СЕЗОННОЕ СНИЖЕНИЕ ЦЕН НА ГРУЗОПЕРЕВОЗКИ', 'Снижаем цены до 10% на все виды  грузоперевозок по России и СНГ', 20, 1, 'mod( \'catalog.action.forms\', array( \'alias\' => \'forma_slajder\' ) )');

-- --------------------------------------------------------

--
-- Структура таблицы `position_slides_offer`
--

CREATE TABLE `position_slides_offer` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `title2` varchar(200) DEFAULT NULL,
  `title3` varchar(200) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `visible` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `position_slides_offer`
--

INSERT INTO `position_slides_offer` (`id`, `section_id`, `img`, `title`, `title2`, `title3`, `position`, `visible`) VALUES
(1, 17, 'files/catalog/upload/2fbb1b0f09b64f85bac21b5615d51cee.jpg', 'Доставка крупногабаритного груза', 'из ЧЕЛЯБИНСКА', 'в ЕКАТЕРИНБУРГ', 10, 1),
(2, 17, 'files/catalog/upload/84c46ca7db6ac6c62143620ca58020c3.jpg', 'Доставка крупногабаритного груза 2', 'из Уфы', 'в Москву', 20, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `section_change_color`
--

CREATE TABLE `section_change_color` (
  `id` int(11) NOT NULL,
  `Header` text,
  `Header_navbar` text,
  `Slider_h1` text,
  `Slider_h2` text,
  `Slider_form` text,
  `Slider_back` text,
  `banner` text,
  `contacts` text,
  `footer` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `section_change_color`
--

INSERT INTO `section_change_color` (`id`, `Header`, `Header_navbar`, `Slider_h1`, `Slider_h2`, `Slider_form`, `Slider_back`, `banner`, `contacts`, `footer`) VALUES
(15, '#003463', '#002343', 'rgba(0, 52, 99, 0.78)', 'rgba(0, 29, 56, 0.77)', 'rgba(0, 52, 99, 0.78)', '#002343', '#002343', '#002343', '#002343');

-- --------------------------------------------------------

--
-- Структура таблицы `section_forms`
--

CREATE TABLE `section_forms` (
  `id` int(11) NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `efrom` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `efromname` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `esubject` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `captcha` smallint(6) DEFAULT '0',
  `header_mail` text COLLATE utf8_unicode_ci,
  `title_form` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `success_message` text COLLATE utf8_unicode_ci,
  `html` text COLLATE utf8_unicode_ci,
  `html_id` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class_form` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `method` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'post',
  `action` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `save_table` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `retailcrm_order_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `retailcrm_on` tinyint(2) DEFAULT NULL,
  `ya_metrika_target_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ya_metrika_target_id_button` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ya_metrika_on` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `section_forms`
--

INSERT INTO `section_forms` (`id`, `email`, `efrom`, `efromname`, `esubject`, `captcha`, `header_mail`, `title_form`, `success_message`, `html`, `html_id`, `class_form`, `method`, `action`, `save_table`, `retailcrm_order_method`, `retailcrm_on`, `ya_metrika_target_id`, `ya_metrika_target_id_button`, `ya_metrika_on`) VALUES
(6, 'cefar@mail.ru', NULL, NULL, 'ФОРМУ ОБРАЩЕНИЯ', 0, NULL, NULL, 'Заявка успешно отправлена!', '<p>Имя: {NAME}</p>\n\n<p>Телефон: {PHONE}</p>\n\n<p>Текст: {TEXT}</p>\n', 'f_forma_obracsheniya', NULL, 'ajax-post', 'send_email', NULL, 'f-forma-obracsheniya', 1, NULL, NULL, NULL),
(10, 'cefar@mail.ru', NULL, NULL, 'Заявка -> Хэдер', 0, NULL, NULL, 'Заявка успешно отправлена!', '<p>Имя: {NAME}</p>\n\n<p>Телефон: {PHONE}</p>\n\n<p>email: {EMAIL}</p>\n', 'f_ostavit_zayavku_heder', NULL, 'ajax-post', 'send_email', NULL, 'f-ostavit-zayavku-heder', 1, NULL, NULL, NULL),
(11, 'cefar@mail.ru', NULL, NULL, 'Заявка -> Футер', 0, NULL, NULL, 'Заявка успешно отправлена!', '<p>Имя: {NAME}</p>\n\n<p>Телефон: {PHONE}</p>\n\n<p>email: {MAIL}</p>\n', 'f_ostavit_zayavku_futer', NULL, 'ajax-post', 'send_email', NULL, 'f-ostavit-zayavku-futer', 1, NULL, NULL, NULL),
(13, 'cefar@mail.ru', NULL, NULL, 'Заявка -> Главная страница -> Акция', 0, NULL, NULL, 'Ваша заявка успешно отправлена!', '<p>Телефон: {PHONE}</p>\n', 'f_zayavka_akciya', NULL, 'ajax-post', 'send_email', NULL, 'f-zayavka-akciya', 1, NULL, NULL, NULL),
(14, 'cefar@mail.ru', NULL, NULL, 'Слайдер -> Заказ', 0, NULL, NULL, 'Ваша заявка успешно отправлена!', '<p>Откуда: {FROM}</p>\n\n<p>Куда: {WHERE}</p>\n', 'f_forma_slajder', NULL, 'ajax-post', 'send_email', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `section_forms_settings`
--

CREATE TABLE `section_forms_settings` (
  `id` int(11) NOT NULL,
  `ya_metrika_key` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ya_metrika_id` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ya_metrika_on` tinyint(2) DEFAULT NULL,
  `retailcrm_key` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `retailcrm_on` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `section_forms_settings`
--

INSERT INTO `section_forms_settings` (`id`, `ya_metrika_key`, `ya_metrika_id`, `ya_metrika_on`, `retailcrm_key`, `retailcrm_on`) VALUES
(1, NULL, NULL, NULL, 'RC-14245400185-2', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `section_form_feedback`
--

CREATE TABLE `section_form_feedback` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `efrom` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `efromname` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `esubject` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `section_news`
--

CREATE TABLE `section_news` (
  `id` int(11) NOT NULL,
  `page_size` int(11) DEFAULT NULL,
  `head_title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `tag` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `section_offer`
--

CREATE TABLE `section_offer` (
  `id` int(11) NOT NULL,
  `kilmeters` text,
  `ind_route` text,
  `treatment` text,
  `aprove` text,
  `coop_car` text,
  `cover_car` text,
  `exped` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `section_offer`
--

INSERT INTO `section_offer` (`id`, `kilmeters`, `ind_route`, `treatment`, `aprove`, `coop_car`, `cover_car`, `exped`) VALUES
(16, '20000', '20000', '20000', '20000', '20000', '20000', '20000');

-- --------------------------------------------------------

--
-- Структура таблицы `section_page`
--

CREATE TABLE `section_page` (
  `id` int(11) NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `visible` smallint(6) DEFAULT '1',
  `head_title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `tag` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `section_timer`
--

CREATE TABLE `section_timer` (
  `id` int(11) NOT NULL,
  `datestamp_start` int(11) DEFAULT NULL,
  `datestamp_over` int(11) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `1_string` text,
  `2_string` text,
  `3_string` text,
  `4_string` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `section_timer`
--

INSERT INTO `section_timer` (`id`, `datestamp_start`, `datestamp_over`, `img`, `1_string`, `2_string`, `3_string`, `4_string`) VALUES
(12, 1509476400, 1512673200, 'files/catalog/upload/857ce6af73ecc6970045706453d4e4c0.jpg', 'ВСЁ ПРОСТО.', 'МЫ ЗНАЕМ, КАК ДОСТАВИТЬ!', 'ЗВОНИТЕ НАМ ДО 30 СЕНТЯБРЯ', 'И МЫ СДЕЛАЕМ ВАМ СКИДКУ В 20%!');

-- --------------------------------------------------------

--
-- Структура таблицы `security_role`
--

CREATE TABLE `security_role` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `security_role`
--

INSERT INTO `security_role` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Структура таблицы `security_user`
--

CREATE TABLE `security_user` (
  `id` int(11) NOT NULL,
  `disabled` smallint(6) DEFAULT '0',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `security_user`
--

INSERT INTO `security_user` (`id`, `disabled`, `name`, `password`) VALUES
(1, 0, 'Bliznes', '65646a951ea29392a378ce4043af3e74');

-- --------------------------------------------------------

--
-- Структура таблицы `security_user_role`
--

CREATE TABLE `security_user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `security_user_role`
--

INSERT INTO `security_user_role` (`user_id`, `role_id`) VALUES
(1, 1),
(1, 2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `catalog_section`
--
ALTER TABLE `catalog_section`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `forms_field_action`
--
ALTER TABLE `forms_field_action`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `forms_field_type`
--
ALTER TABLE `forms_field_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menus_item`
--
ALTER TABLE `menus_item`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_bonus`
--
ALTER TABLE `position_bonus`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_coop`
--
ALTER TABLE `position_coop`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_forms`
--
ALTER TABLE `position_forms`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_news`
--
ALTER TABLE `position_news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_setting`
--
ALTER TABLE `position_setting`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_slides`
--
ALTER TABLE `position_slides`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `position_slides_offer`
--
ALTER TABLE `position_slides_offer`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_change_color`
--
ALTER TABLE `section_change_color`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_forms`
--
ALTER TABLE `section_forms`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_forms_settings`
--
ALTER TABLE `section_forms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_form_feedback`
--
ALTER TABLE `section_form_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_news`
--
ALTER TABLE `section_news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_offer`
--
ALTER TABLE `section_offer`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_page`
--
ALTER TABLE `section_page`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `section_timer`
--
ALTER TABLE `section_timer`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_role`
--
ALTER TABLE `security_role`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `security_user`
--
ALTER TABLE `security_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`name`);

--
-- Индексы таблицы `security_user_role`
--
ALTER TABLE `security_user_role`
  ADD PRIMARY KEY (`user_id`,`role_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `catalog_section`
--
ALTER TABLE `catalog_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `menus_item`
--
ALTER TABLE `menus_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `position_bonus`
--
ALTER TABLE `position_bonus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `position_coop`
--
ALTER TABLE `position_coop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `position_forms`
--
ALTER TABLE `position_forms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблицы `position_news`
--
ALTER TABLE `position_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `position_setting`
--
ALTER TABLE `position_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;
--
-- AUTO_INCREMENT для таблицы `position_slides`
--
ALTER TABLE `position_slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `position_slides_offer`
--
ALTER TABLE `position_slides_offer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `section_change_color`
--
ALTER TABLE `section_change_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `section_offer`
--
ALTER TABLE `section_offer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `section_timer`
--
ALTER TABLE `section_timer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `security_user`
--
ALTER TABLE `security_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
