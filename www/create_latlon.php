<?php

$url = 'https://geocode-maps.yandex.ru/1.x/?format=json&geocode=';


	$fp = file( "./dt_towns.data" );
	$fw = fopen( "dt_towns_latlon.data", "a+" );

	foreach ( $fp as $key => $f ) {

		$first_symbol = substr( $f, 0, 1 );
		$f = trim( $f );
		if ( $first_symbol == '-' ) continue;
		if ( !$f ) continue;

		$ex_town = explode( '|', $f );
		$f = trim( $ex_town[ 0 ] );

		$latlon = file_get_contents( $url . str_replace( ' ', '%20', $f ) );
		$latlon = json_decode( $latlon, true );
		$latlon = $latlon[ 'response' ][ 'GeoObjectCollection' ][ 'featureMember' ][ 0 ][ 'GeoObject' ][ 'Point' ][ 'pos' ];
		fwrite( $fw, $f . '|' . $latlon . "\n" );
	}
