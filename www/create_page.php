<?php

	ini_set( 'memory_limit', '500M' );

	$path_cms = realpath( dirname( __FILE__ ) . '/..' ) . DIRECTORY_SEPARATOR . 'www' . DIRECTORY_SEPARATOR;
	require realpath( dirname( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'morphy' . DIRECTORY_SEPARATOR . 'changewords.php';
	require $path_cms . 'config.php';
	require $path_cms . 'cms.php';

	$file_base = './dt_towns.data';
	$file = file( $file_base );

	$ID_file = 0;
	$CITY1_file = '';
	$CITY2_file = '';

	$fp = fopen("dt_created_offers.data", "w+");

	$sc_fp = file("./dt_request_offers_unserialaize.data");

	foreach ( $file as $key => $f ) {

		$first_symbol = substr( $f, 0, 1 );
		$f = trim( $f );
		if ( $first_symbol == '-' ) continue;
		if ( !$f ) continue;
			
		$ex_town = explode( '|', $f );
		$f = trim( $ex_town[ 0 ] );

		//ChangeWords
		$parent_page = 'Не габаритные грузоперевозки из ' .ChangeWords($f);
		$parent_page_alias = 'ne-gabaritnie-gruzoperevozki-iz-' . Utils :: translit(  $f );
		$position = 10;

		$table = new Table( 'pages' );
		$parent_page_exist = $table -> select( 'SELECT * FROM `pages` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $parent_page_alias ) );
		if ( !count( $parent_page_exist ) ) {
			// создаем родительскую страницу
			$parent = new stdClass( );
			$parent -> title = $parent_page;
			$parent -> title2 = '';
			$parent -> alias = $parent_page_alias;
			$parent -> visible = 1;
			$parent -> meta_keywords = '';
			$parent -> meta_description = '';
			$parent -> head_title = 'Товарное предложение города';
			$parent -> position = $position;
			$parent -> template = 'page.tpl.php';
			$parent -> mainpage = 0;
			$parent_id = $table -> save( $parent );
			if ( $table -> errorInfo ) {
				throw new Exception( 'pdo_error : ' . $table -> errorInfo );
			}
			echo "parent page is created! id - " . $parent_id . "\n";
		}
		else {
			$parent_page_exist = end( $parent_page_exist );
			$parent_page_exist = ( object ) $parent_page_exist;
			$parent_id = $parent_page_exist -> id;
			echo "parent page is exist! id - " . $parent_id . "\n";
		}

		// Такси Челябинск - Город
		$town_id = array( );
		$position++;

			foreach ($file as $key => $f_sub) {

				$first_symbol = substr( $f_sub, 0, 1 );
				$f_sub = trim( $f_sub );
				if ( $first_symbol == '-' ) continue;
				if ( !$f_sub ) continue;

				$ex_town = explode( '|', $f_sub );
				/*$price = ( count( $ex_town ) > 1 ) ? trim( $ex_town[ 1 ] ) : '';*/
				$price = '';
				$f_sub = trim( $ex_town[ 0 ] );

				if ( $f == $f_sub ) continue;

				$ID_file++;
				$CITY1_file = $f;
				$CITY2_file = $f_sub;




				$TEXT_FOR_FILE = $ID_file.'|'.$CITY1_file.'|'.$CITY2_file.PHP_EOL;


				fwrite($fp, $TEXT_FOR_FILE);

					// создаем страницу межгорода
					$town_page_alias = Utils :: translit( $f  .' - '. $f_sub );
					$town_alias = Utils :: translit( $f );
					$town_page_exist = $table -> select( 'SELECT * FROM `pages` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $town_page_alias ) );

					if ( !count( $town_page_exist ) ) {
						$town = new stdClass( );
						$town -> parent_id = $parent_id;
						$town -> title = $f .' - '. $f_sub;
						$town -> title2 = $price;
						$town -> alias = $town_page_alias;
						$town -> visible = 1;
						$town -> meta_keywords = '';
						$town -> meta_description = 'Не габаритный груз - перевозка из ' . ChangeWords( $f )  .'в '. $f_sub;
						$town -> announcement = '';
						$town -> head_title = $f .' - '. $f_sub;
						$town -> position = $position;
						$town -> template = 'page.tpl.php';
						$town -> mainpage = 0;
						$town -> tag = 'Перевозка не габаритных грузов ' . $f .' - '. $f_sub . ', всего за ' . $price . ' руб.';
						$t_id = $table -> save( $town );
						$town_id[ $f ] = $t_id;
						if ( $table -> errorInfo ) {
							throw new Exception( 'pdo_error : ' . $table -> errorInfo );
						}
						echo "town page " . Utils :: translit( $town -> title ) . " is created! id - " . $t_id . "\n";
					}
					else {
						$town_page_exist = end( $town_page_exist );
						$town_page_exist = ( object ) $town_page_exist;
						$town_id[ $f ] = $town_page_exist -> id;
						$update =  'UPDATE `pages`
									SET `title2`=:title2,
									    `title`=:title,
										`meta_description`=:meta_description,
										`meta_keywords`=:meta_keywords,
										`head_title`=:head_title,
										`tag`=:tag,
										`announcement`=:announcement
									WHERE `id`=:id';
						$table -> execute( $update, array(
							'title2' => $price,
							'title' => $f  .' - '. $f_sub,
							'meta_description' => 'Не габаритный груз - перевозка из ' . ChangeWords( $f )  .'в '. $f_sub,
							'announcement' => '',
							'meta_keywords' => '',
							'head_title' => $f  .' - '. $f_sub,
							'tag' => 'Перевозка не габаритных грузов ' . $f .' - '. $f_sub . ', всего за ' . $price . ' руб.',
							'id' => $town_page_exist -> id
						) );
						echo "town page " . $town_page_exist -> alias . " is exist! id - " . $town_page_exist -> id . "\n";
				}
			}
	}

	fclose($fp);
	

	
	
/*	// Такси Город - Челябинск
	$town_id = array( );
	foreach ( $file as $key => $f ) {
		++$position;
		$first_symbol = substr( $f, 0, 1 );
		$f = trim( $f );
		if ( $first_symbol == '-' ) continue;
		if ( !$f ) continue;

		$ex_town = explode( '|', $f );
		$price = ( count( $ex_town ) > 1 ) ? trim( $ex_town[ 1 ] ) : '';
		$f = trim( $ex_town[ 0 ] );
		
		// создаем страницу межгорода
		$town_page_alias = Utils :: translit( 'Такси ' . $f . ' - Челябинск' );
		$town_alias = Utils :: translit( $f );
		$town_page_exist = $table -> select( 'SELECT * FROM `pages` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $town_page_alias ) );

		if ( !count( $town_page_exist ) ) {
			$town = new stdClass( );
			$town -> parent_id = $parent_id;
			$town -> title = 'Такси ' . $f . ' - Челябинск';
			$town -> title2 = $price;
			$town -> alias = $town_page_alias;
			$town -> visible = 1;
			$town -> meta_keywords = '';
			$town -> meta_description = 'Стоимость такси из ' . ChangeWords( $f ) . ' в Челябинск | официальный сайт такси STAR';
			$town -> head_title = 'Заказать такси ' . $f . ' - Челябинск';
			$town -> position = $position;
			$town -> template = 'page.tpl.php';
			$town -> mainpage = 0;
			$town -> tag = 'Заказать такси ' . $f . ' - Челябинск за ';
			$t_id = $table -> save( $town );
			$town_id[ $f ] = $t_id;
			if ( $table -> errorInfo ) {
				throw new Exception( 'pdo_error : ' . $table -> errorInfo );
			}
			echo "town page " . Utils :: translit( $town -> title ) . " is created! id - " . $t_id . "\n";
		}
		else {
			$town_page_exist = end( $town_page_exist );
			$town_page_exist = ( object ) $town_page_exist;
			$town_id[ $f ] = $town_page_exist -> id;
			$update =  'UPDATE `pages`
						SET `title2`=:title2,
						    `title`=:title,
							`meta_description`=:meta_description,
							`meta_keywords`=:meta_keywords,
							`head_title`=:head_title,
							`tag`=:tag
						WHERE `id`=:id';
			$table -> execute( $update, array(
				'title2' => $price,
				'title' => 'Такси ' . $f . ' - Челябинск',
				'meta_description' => 'Стоимость такси из ' . ChangeWords( $f ) . ' в Челябинск | официальный сайт такси STAR',
				'meta_keywords' => '',
				'head_title' => 'Заказать такси ' . $f . ' - Челябинск',
				'tag' => 'Заказать такси ' . $f . ' - Челябинск за ' . $price . ' руб.',
				'id' => $town_page_exist -> id
			) );
			echo "town page " . $town_page_exist -> alias . " is exist! id - " . $town_page_exist -> id . "\n";
		}
	}*/
	
	
/*
//////////////////////////////////////////////////////////
	//ChangeWords
	$parent_page = 'Такси Аэропорт';
	$parent_page_alias = 'taxi-aeroport';
	$position += 10;
	
	$table = new Table( 'pages' );
	$parent_page_exist = $table -> select( 'SELECT * FROM `pages` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $parent_page_alias ) );
	if ( !count( $parent_page_exist ) ) {
		// создаем родительскую страницу
		$parent = new stdClass( );
		$parent -> title = $parent_page;
		$parent -> title2 = 'Заказать такси в Аэропорт';
		$parent -> alias = $parent_page_alias;
		$parent -> visible = 1;
		$parent -> meta_keywords = '';
		$parent -> meta_description = 'Комфортно втретить в Аэропорту или быстро добраться в Аэропорт - очень легко с такси STAR';
		$parent -> head_title = 'Такси Аэропорт - Челябинск';
		$parent -> position = $position;
		$parent -> template = 'page.tpl.php';
		$parent -> mainpage = 0;
		$parent_id = $table -> save( $parent );
		if ( $table -> errorInfo ) {
			throw new Exception( 'pdo_error : ' . $table -> errorInfo );
		}
		echo "parent page is created! id - " . $parent_id . "\n";
	}
	else {
		$parent_page_exist = end( $parent_page_exist );
		$parent_page_exist = ( object ) $parent_page_exist;
		$parent_id = $parent_page_exist -> id;
		echo "parent page is exist! id - " . $parent_id . "\n";
	}

	$file_base = './dt_in_chel_aero.data';
	$file = file( $file_base );


	// Такси Челябинск - Город
	$town_id = array( );
	foreach ( $file as $key => $f ) {
		++$position;
		$first_symbol = substr( $f, 0, 1 );
		$f = trim( $f );
		if ( $first_symbol == '-' ) continue;
		if ( !$f ) continue;
		
		$ex_town = explode( '|', $f );
		$price = ( count( $ex_town ) > 2 ) ? trim( $ex_town[ 2 ] ) : '';
		$f = trim( $ex_town[ 0 ] );
		$h = ( count( $ex_town ) > 1 ) ? trim( $ex_town[ 1 ] ) : '';
		
		// создаем страницу межгорода
		$town_page_alias = Utils :: translit( 'Такси ' . $f . '-' . $h );
		$town_alias = Utils :: translit( $f . '-' . $h );
		$town_page_exist = $table -> select( 'SELECT * FROM `pages` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $town_page_alias ) );

		if ( !count( $town_page_exist ) ) {
			$town = new stdClass( );
			$town -> parent_id = $parent_id;
			$town -> title = 'Такси ' . $f . ' - ' . $h;
			$town -> title2 = $price;
			$town -> alias = $town_page_alias;
			$town -> visible = 1;
			$town -> meta_keywords = '';
			$town -> meta_description = 'Узнать стоимость такси до ' . ChangeWords( $f ) . ' из ' . ChangeWords( $h ) . ' | официальный сайт такси STAR';
			$town -> head_title = 'Заказать такси ' . $f . ' - ' . $h;
			$town -> position = $position;
			$town -> template = 'page.tpl.php';
			$town -> mainpage = 0;
			$town -> tag = 'Заказать такси ' . $f . ' - ' . $h . ' за ' . $price . ' руб.';
			$t_id = $table -> save( $town );
			$town_id[ $f . '-' . $h ] = $t_id;
			if ( $table -> errorInfo ) {
				throw new Exception( 'pdo_error : ' . $table -> errorInfo );
			}
			echo "town page " . Utils :: translit( $town -> title ) . " is created! id - " . $t_id . "\n";
		}
		else {
			$town_page_exist = end( $town_page_exist );
			$town_page_exist = ( object ) $town_page_exist;
			$town_id[ $f . '-' . $h ] = $town_page_exist -> id;
			$update =  'UPDATE `pages`
						SET `title2`=:title2,
						    `title`=:title,
							`meta_description`=:meta_description,
							`meta_keywords`=:meta_keywords,
							`head_title`=:head_title,
							`tag`=:tag
						WHERE `id`=:id';
			$table -> execute( $update, array(
				'title2' => $price,
				'title' => 'Такси ' . $f . ' - ' . $h,
				'meta_description' => 'Узнать стоимость такси до ' . ChangeWords( $f ) . ' из ' . ChangeWords( $h ) . ' | официальный сайт такси STAR',
				'meta_keywords' => '',
				'head_title' => 'Заказать такси ' . $f . ' - ' . $h,
				'tag' => 'Заказать такси ' . $f . ' - ' . $h . ' за ' . $price . ' руб.',
				'id' => $town_page_exist -> id
			) );
			echo "town page " . $town_page_exist -> alias . " is exist! id - " . $town_page_exist -> id . "\n";
		}
	}

	

	//////////////////////////////////////////////////////////
	//ChangeWords
	$parent_page = 'Такси в Санатории';
	$parent_page_alias = 'taxi-sanatorium';
	$position += 10;
	
	$table = new Table( 'pages' );
	$parent_page_exist = $table -> select( 'SELECT * FROM `pages` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $parent_page_alias ) );
	if ( !count( $parent_page_exist ) ) {
		// создаем родительскую страницу
		$parent = new stdClass( );
		$parent -> title = $parent_page;
		$parent -> title2 = 'Заказать такси в Санатории';
		$parent -> alias = $parent_page_alias;
		$parent -> visible = 1;
		$parent -> meta_keywords = '';
		$parent -> meta_description = 'Комфортно добраться до Санатория или из Санатория к Вашему дому поможет - такси STAR';
		$parent -> head_title = 'Такси Санатории - Челябинск';
		$parent -> position = $position;
		$parent -> template = 'page.tpl.php';
		$parent -> mainpage = 0;
		$parent_id = $table -> save( $parent );
		if ( $table -> errorInfo ) {
			throw new Exception( 'pdo_error : ' . $table -> errorInfo );
		}
		echo "parent page is created! id - " . $parent_id . "\n";
	}
	else {
		$parent_page_exist = end( $parent_page_exist );
		$parent_page_exist = ( object ) $parent_page_exist;
		$parent_id = $parent_page_exist -> id;
		echo "parent page is exist! id - " . $parent_id . "\n";
	}

	$file_base = './dt_chel_sanatorii.data';
	$file = file( $file_base );


	// Такси Челябинск - Город
	$town_id = array( );
	foreach ( $file as $key => $f ) {
		++$position;
		$first_symbol = substr( $f, 0, 1 );
		$f = trim( $f );
		if ( $first_symbol == '-' ) continue;
		if ( !$f ) continue;
		
		$ex_town = explode( '|', $f );
		$price = ( count( $ex_town ) > 2 ) ? trim( $ex_town[ 2 ] ) : '';
		$f = trim( $ex_town[ 0 ] );
		$h = ( count( $ex_town ) > 1 ) ? trim( $ex_town[ 1 ] ) : '';
		
		// создаем страницу межгорода
		$town_page_alias = Utils :: translit( 'Такси ' . $f . '-' . $h );
		$town_alias = Utils :: translit( $f . '-' . $h );
		$town_page_exist = $table -> select( 'SELECT * FROM `pages` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $town_page_alias ) );

		if ( !count( $town_page_exist ) ) {
			$town = new stdClass( );
			$town -> parent_id = $parent_id;
			$town -> title = 'Такси ' . $f . ' - ' . $h;
			$town -> title2 = $price;
			$town -> alias = $town_page_alias;
			$town -> visible = 1;
			$town -> meta_keywords = '';
			$town -> meta_description = 'Сколько стоит такси до ' . ChangeWords( $h ) . ' из ' . ChangeWords( $f ) . ' | официальный сайт такси STAR';
			$town -> head_title = 'Заказать такси ' . $f . ' - ' . $h;
			$town -> position = $position;
			$town -> template = 'page.tpl.php';
			$town -> mainpage = 0;
			$town -> tag = 'Заказать такси ' . $f . ' - ' . $h . ' за ' . $price . ' руб.';
			$t_id = $table -> save( $town );
			$town_id[ $f . '-' . $h ] = $t_id;
			if ( $table -> errorInfo ) {
				throw new Exception( 'pdo_error : ' . $table -> errorInfo );
			}
			echo "town page " . Utils :: translit( $town -> title ) . " is created! id - " . $t_id . "\n";
		}
		else {
			$town_page_exist = end( $town_page_exist );
			$town_page_exist = ( object ) $town_page_exist;
			$town_id[ $f . '-' . $h ] = $town_page_exist -> id;
			$update =  'UPDATE `pages`
						SET `title2`=:title2,
						    `title`=:title,
							`meta_description`=:meta_description,
							`meta_keywords`=:meta_keywords,
							`head_title`=:head_title,
							`tag`=:tag
						WHERE `id`=:id';
			$table -> execute( $update, array(
				'title2' => $price,
				'title' => 'Такси ' . $f . ' - ' . $h,
				'meta_description' => 'Сколько стоит такси до ' . ChangeWords( $h ) . ' из ' . ChangeWords( $f ) . ' | официальный сайт такси STAR',
				'meta_keywords' => '',
				'head_title' => 'Заказать такси ' . $f . ' - ' . $h,
				'tag' => 'Заказать такси ' . $f . ' - ' . $h . ' за ' . $price . ' руб.',
				'id' => $town_page_exist -> id
			) );
			echo "town page " . $town_page_exist -> alias . " is exist! id - " . $town_page_exist -> id . "\n";
		}
	}

*/
/*
	//////////////////////////////////////////////////////////
	// ChangeWords
	$parent_page = 'Такси на озера';
	$parent_page_alias = 'taksi-na-ozera';
	$position += 10;
	
	$table = new Table( 'pages' );
	$parent_page_exist = $table -> select( 'SELECT * FROM `pages` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $parent_page_alias ) );
	if ( !count( $parent_page_exist ) ) {
		// создаем родительскую страницу
		$parent = new stdClass( );
		$parent -> title = $parent_page;
		$parent -> title2 = 'Заказать такси на';
		$parent -> alias = $parent_page_alias;
		$parent -> visible = 1;
		$parent -> meta_keywords = '';
		$parent -> meta_description = 'Собрались на озеро? вас довезет такси STAR';
		$parent -> head_title = 'Такси Челябинск - озера';
		$parent -> position = $position;
		$parent -> template = 'page.tpl.php';
		$parent -> mainpage = 0;
		$parent_id = $table -> save( $parent );
		if ( $table -> errorInfo ) {
			throw new Exception( 'pdo_error : ' . $table -> errorInfo );
		}
		echo "parent page is created! id - " . $parent_id . "\n";
	}
	else {
		$parent_page_exist = end( $parent_page_exist );
		$parent_page_exist = ( object ) $parent_page_exist;
		$parent_id = $parent_page_exist -> id;
		echo "parent page is exist! id - " . $parent_id . "\n";
	}

	$file_base = './dt_chel_lakes.data';
	$file = file( $file_base );


	// Такси Челябинск - озеро
	$town_id = array( );
	foreach ( $file as $key => $f ) {
		++$position;
		$first_symbol = substr( $f, 0, 1 );
		$f = trim( $f );
		if ( $first_symbol == '-' ) continue;
		if ( !$f ) continue;
		
		$ex_town = explode( '|', $f );
		$price = ( count( $ex_town ) > 2 ) ? trim( $ex_town[ 2 ] ) : '';
		$f = trim( $ex_town[ 0 ] );
		$h = ( count( $ex_town ) > 1 ) ? trim( $ex_town[ 1 ] ) : '';
		
		// создаем страницу межгорода
		$town_page_alias = Utils :: translit( 'Такси ' . $f . '-' . $h );
		$town_alias = Utils :: translit( $f . '-' . $h );
		$town_page_exist = $table -> select( 'SELECT * FROM `pages` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $town_page_alias ) );

		if ( !count( $town_page_exist ) ) {
			$town = new stdClass( );
			$town -> parent_id = $parent_id;
			$town -> title = 'Такси ' . $f . ' - ' . $h;
			$town -> title2 = $price;
			$town -> alias = $town_page_alias;
			$town -> visible = 1;
			$town -> meta_keywords = '';
			$town -> meta_description = 'Цена такси до ' . ChangeWords( $h ) . ' из ' . ChangeWords( $f ) . ' | официальный сайт такси STAR';
			$town -> head_title = 'Заказать такси ' . $f . ' - ' . $h;
			$town -> position = $position;
			$town -> template = 'page.tpl.php';
			$town -> mainpage = 0;
			$town -> tag = 'Заказать такси ' . $f . ' - ' . $h . ' за ' . $price . ' руб.';
			$t_id = $table -> save( $town );
			$town_id[ $f . '-' . $h ] = $t_id;
			if ( $table -> errorInfo ) {
				throw new Exception( 'pdo_error : ' . $table -> errorInfo );
			}
			echo "town page " . Utils :: translit( $town -> title ) . " is created! id - " . $t_id . "\n";
		}
		else {
			$town_page_exist = end( $town_page_exist );
			$town_page_exist = ( object ) $town_page_exist;
			$town_id[ $f . '-' . $h ] = $town_page_exist -> id;
			$update =  'UPDATE `pages`
						SET `title2`=:title2,
						    `title`=:title,
							`meta_description`=:meta_description,
							`meta_keywords`=:meta_keywords,
							`head_title`=:head_title,
							`tag`=:tag
						WHERE `id`=:id';
			$table -> execute( $update, array(
				'title2' => $price,
				'title' => 'Такси ' . $f . ' - ' . $h,
				'meta_description' => 'Цена такси до ' . ChangeWords( $h ) . ' из ' . ChangeWords( $f ) . ' | официальный сайт такси STAR',
				'meta_keywords' => '',
				'head_title' => 'Заказать такси ' . $f . ' - ' . $h,
				'tag' => 'Заказать такси ' . $f . ' - ' . $h . ' за ' . $price . ' руб.',
				'id' => $town_page_exist -> id
			) );
			echo "town page " . $town_page_exist -> alias . " is exist! id - " . $town_page_exist -> id . "\n";
		}
	}
	
	
	

	//////////////////////////////////////////////////////////
	// ChangeWords
	$parent_page = 'Такси в детские лагеря';
	$parent_page_alias = 'taksi-detskie-lagerya';
	$position += 10;
	
	$table = new Table( 'pages' );
	$parent_page_exist = $table -> select( 'SELECT * FROM `pages` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $parent_page_alias ) );
	if ( !count( $parent_page_exist ) ) {
		// создаем родительскую страницу
		$parent = new stdClass( );
		$parent -> title = $parent_page;
		$parent -> title2 = 'Заказать такси на';
		$parent -> alias = $parent_page_alias;
		$parent -> visible = 1;
		$parent -> meta_keywords = '';
		$parent -> meta_description = 'Довезет Вас и Вашего ребенка в детский оздоровительный / спортивный лагерь | такси STAR';
		$parent -> head_title = 'Такси Челябинск - детские оздоровительные и спортивные лагеря';
		$parent -> position = $position;
		$parent -> template = 'page.tpl.php';
		$parent -> mainpage = 0;
		$parent_id = $table -> save( $parent );
		if ( $table -> errorInfo ) {
			throw new Exception( 'pdo_error : ' . $table -> errorInfo );
		}
		echo "parent page is created! id - " . $parent_id . "\n";
	}
	else {
		$parent_page_exist = end( $parent_page_exist );
		$parent_page_exist = ( object ) $parent_page_exist;
		$parent_id = $parent_page_exist -> id;
		echo "parent page is exist! id - " . $parent_id . "\n";
	}

	$file_base = './dt_chel_lagerya.data';
	$file = file( $file_base );


	// Такси Челябинск - озеро
	$town_id = array( );
	foreach ( $file as $key => $f ) {
		++$position;
		$first_symbol = substr( $f, 0, 1 );
		$f = trim( $f );
		if ( $first_symbol == '-' ) continue;
		if ( !$f ) continue;
		
		$ex_town = explode( '|', $f );
		$price = ( count( $ex_town ) > 2 ) ? trim( $ex_town[ 2 ] ) : '';
		$f = trim( $ex_town[ 0 ] );
		$h = ( count( $ex_town ) > 1 ) ? trim( $ex_town[ 1 ] ) : '';
		
		// создаем страницу межгорода
		$town_page_alias = Utils :: translit( 'Такси ' . $f . '-' . $h );
		$town_alias = Utils :: translit( $f . '-' . $h );
		$town_page_exist = $table -> select( 'SELECT * FROM `pages` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $town_page_alias ) );

		if ( !count( $town_page_exist ) ) {
			$town = new stdClass( );
			$town -> parent_id = $parent_id;
			$town -> title = 'Такси ' . $f . ' - ' . $h;
			$town -> title2 = $price;
			$town -> alias = $town_page_alias;
			$town -> visible = 1;
			$town -> meta_keywords = '';
			$town -> meta_description = 'Цена такси до ' . ChangeWords( $h ) . ' из ' . ChangeWords( $f ) . ' | официальный сайт такси STAR';
			$town -> head_title = 'Заказать такси ' . $f . ' - ' . $h;
			$town -> position = $position;
			$town -> template = 'page.tpl.php';
			$town -> mainpage = 0;
			$town -> tag = 'Заказать такси ' . $f . ' - ' . $h . ' за ' . $price . ' руб.';
			$t_id = $table -> save( $town );
			$town_id[ $f . '-' . $h ] = $t_id;
			if ( $table -> errorInfo ) {
				throw new Exception( 'pdo_error : ' . $table -> errorInfo );
			}
			echo "town page " . Utils :: translit( $town -> title ) . " is created! id - " . $t_id . "\n";
		}
		else {
			$town_page_exist = end( $town_page_exist );
			$town_page_exist = ( object ) $town_page_exist;
			$town_id[ $f . '-' . $h ] = $town_page_exist -> id;
			$update =  'UPDATE `pages`
						SET `title2`=:title2,
						    `title`=:title,
							`meta_description`=:meta_description,
							`meta_keywords`=:meta_keywords,
							`head_title`=:head_title,
							`tag`=:tag
						WHERE `id`=:id';
			$table -> execute( $update, array(
				'title2' => $price,
				'title' => 'Такси ' . $f . ' - ' . $h,
				'meta_description' => 'Цена такси до ' . ChangeWords( $h ) . ' из ' . ChangeWords( $f ) . ' | официальный сайт такси STAR',
				'meta_keywords' => '',
				'head_title' => 'Заказать такси ' . $f . ' - ' . $h,
				'tag' => 'Заказать такси ' . $f . ' - ' . $h . ' за ' . $price . ' руб.',
				'id' => $town_page_exist -> id
			) );
			echo "town page " . $town_page_exist -> alias . " is exist! id - " . $town_page_exist -> id . "\n";
		}
	}
	
	


	// Горнолыжные курорты
	//////////////////////////////////////////////////////////
	// ChangeWords
	$parent_page = 'Горнолыжные курорты';
	$parent_page_alias = 'gornolyzhnye-kurorty';
	$position += 10;
	
	$table = new Table( 'pages' );
	$parent_page_exist = $table -> select( 'SELECT * FROM `pages` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $parent_page_alias ) );
	if ( !count( $parent_page_exist ) ) {
		// создаем родительскую страницу
		$parent = new stdClass( );
		$parent -> title = $parent_page;
		$parent -> title2 = 'Заказать такси на';
		$parent -> alias = $parent_page_alias;
		$parent -> visible = 1;
		$parent -> meta_keywords = '';
		$parent -> meta_description = 'Зимний активный отдых начинается с поездки на горнолыжный курорт. Звони, поможем добраться! | такси STAR';
		$parent -> head_title = 'Такси Челябинск - горнолыжные курорты Урала';
		$parent -> position = $position;
		$parent -> template = 'page.tpl.php';
		$parent -> mainpage = 0;
		$parent_id = $table -> save( $parent );
		if ( $table -> errorInfo ) {
			throw new Exception( 'pdo_error : ' . $table -> errorInfo );
		}
		echo "parent page is created! id - " . $parent_id . "\n";
	}
	else {
		$parent_page_exist = end( $parent_page_exist );
		$parent_page_exist = ( object ) $parent_page_exist;
		$parent_id = $parent_page_exist -> id;
		echo "parent page is exist! id - " . $parent_id . "\n";
	}

	$file_base = './dt_chel_gornolij.data';
	$file = file( $file_base );


	// Такси Челябинск - Горнолыжные курорты
	$town_id = array( );
	foreach ( $file as $key => $f ) {
		++$position;
		$first_symbol = substr( $f, 0, 1 );
		$f = trim( $f );
		if ( $first_symbol == '-' ) continue;
		if ( !$f ) continue;
		
		$ex_town = explode( '|', $f );
		$price = ( count( $ex_town ) > 2 ) ? trim( $ex_town[ 2 ] ) : '';
		$f = trim( $ex_town[ 0 ] );
		$h = ( count( $ex_town ) > 1 ) ? trim( $ex_town[ 1 ] ) : '';
		
		// создаем страницу межгорода
		$town_page_alias = Utils :: translit( 'Такси ' . $f . '-' . $h );
		$town_alias = Utils :: translit( $f . '-' . $h );
		$town_page_exist = $table -> select( 'SELECT * FROM `pages` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $town_page_alias ) );

		if ( !count( $town_page_exist ) ) {
			$town = new stdClass( );
			$town -> parent_id = $parent_id;
			$town -> title = 'Такси ' . $f . ' - ' . $h;
			$town -> title2 = $price;
			$town -> alias = $town_page_alias;
			$town -> visible = 1;
			$town -> meta_keywords = '';
			$town -> meta_description = 'Цена такси до ' . ChangeWords( $h ) . ' из ' . ChangeWords( $f ) . ' | официальный сайт такси STAR';
			$town -> head_title = 'Заказать такси ' . $f . ' - ' . $h;
			$town -> position = $position;
			$town -> template = 'page.tpl.php';
			$town -> mainpage = 0;
			$town -> tag = 'Заказать такси ' . $f . ' - ' . $h . ' за ' . $price . ' руб.';
			$t_id = $table -> save( $town );
			$town_id[ $f . '-' . $h ] = $t_id;
			if ( $table -> errorInfo ) {
				throw new Exception( 'pdo_error : ' . $table -> errorInfo );
			}
			echo "town page " . Utils :: translit( $town -> title ) . " is created! id - " . $t_id . "\n";
		}
		else {
			$town_page_exist = end( $town_page_exist );
			$town_page_exist = ( object ) $town_page_exist;
			$town_id[ $f . '-' . $h ] = $town_page_exist -> id;
			$update =  'UPDATE `pages`
						SET `title2`=:title2,
						    `title`=:title,
							`meta_description`=:meta_description,
							`meta_keywords`=:meta_keywords,
							`head_title`=:head_title,
							`tag`=:tag
						WHERE `id`=:id';
			$table -> execute( $update, array(
				'title2' => $price,
				'title' => 'Такси ' . $f . ' - ' . $h,
				'meta_description' => 'Цена такси до ' . ChangeWords( $h ) . ' из ' . ChangeWords( $f ) . ' | официальный сайт такси STAR',
				'meta_keywords' => '',
				'head_title' => 'Заказать такси ' . $f . ' - ' . $h,
				'tag' => 'Заказать такси ' . $f . ' - ' . $h . ' за ' . $price . ' руб.',
				'id' => $town_page_exist -> id
			) );
			echo "town page " . $town_page_exist -> alias . " is exist! id - " . $town_page_exist -> id . "\n";
		}
	}
	
	



	// Базы отдыха
	//////////////////////////////////////////////////////////
	// ChangeWords
	$parent_page = 'Базы отдыха';
	$parent_page_alias = 'bazy-otdyha';
	$position += 10;
	
	$table = new Table( 'pages' );
	$parent_page_exist = $table -> select( 'SELECT * FROM `pages` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $parent_page_alias ) );
	if ( !count( $parent_page_exist ) ) {
		// создаем родительскую страницу
		$parent = new stdClass( );
		$parent -> title = $parent_page;
		$parent -> title2 = 'Заказать такси на';
		$parent -> alias = $parent_page_alias;
		$parent -> visible = 1;
		$parent -> meta_keywords = '';
		$parent -> meta_description = 'Выезъжая за город, на базу отдыха важно знать как туда добраться. Звони, мы знаем все маршруты! | такси STAR';
		$parent -> head_title = 'Такси Челябинск - базы отдыха Урала';
		$parent -> position = $position;
		$parent -> template = 'page.tpl.php';
		$parent -> mainpage = 0;
		$parent_id = $table -> save( $parent );
		if ( $table -> errorInfo ) {
			throw new Exception( 'pdo_error : ' . $table -> errorInfo );
		}
		echo "parent page is created! id - " . $parent_id . "\n";
	}
	else {
		$parent_page_exist = end( $parent_page_exist );
		$parent_page_exist = ( object ) $parent_page_exist;
		$parent_id = $parent_page_exist -> id;
		echo "parent page is exist! id - " . $parent_id . "\n";
	}

	$file_base = './dt_chel_bases.data';
	$file = file( $file_base );


	// Такси Челябинск - Горнолыжные курорты
	$town_id = array( );
	foreach ( $file as $key => $f ) {
		++$position;
		$first_symbol = substr( $f, 0, 1 );
		$f = trim( $f );
		if ( $first_symbol == '-' ) continue;
		if ( !$f ) continue;
		
		$ex_town = explode( '|', $f );
		$price = ( count( $ex_town ) > 2 ) ? trim( $ex_town[ 2 ] ) : '';
		$f = trim( $ex_town[ 0 ] );
		$h = ( count( $ex_town ) > 1 ) ? trim( $ex_town[ 1 ] ) : '';
		
		// создаем страницу межгорода
		$town_page_alias = Utils :: translit( 'Такси ' . $f . '-база отдыха ' . $h );
		$town_alias = Utils :: translit( $f . '-' . $h );
		$town_page_exist = $table -> select( 'SELECT * FROM `pages` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $town_page_alias ) );

		if ( !count( $town_page_exist ) ) {
			$town = new stdClass( );
			$town -> parent_id = $parent_id;
			$town -> title = 'Такси ' . $f . ' - база отдыха ' . $h;
			$town -> title2 = $price;
			$town -> alias = $town_page_alias;
			$town -> visible = 1;
			$town -> meta_keywords = '';
			$town -> meta_description = 'Цена такси до базы отдыха ' . $h . ' из ' . ChangeWords( $f ) . ' | официальный сайт такси STAR';
			$town -> head_title = 'Заказать такси ' . $f . ' - база отдыха ' . $h;
			$town -> position = $position;
			$town -> template = 'page.tpl.php';
			$town -> mainpage = 0;
			$town -> tag = 'Заказать такси ' . $f . ' - база отдыха ' . $h . ' за ' . $price . ' руб.';
			$t_id = $table -> save( $town );
			$town_id[ $f . '-' . $h ] = $t_id;
			if ( $table -> errorInfo ) {
				throw new Exception( 'pdo_error : ' . $table -> errorInfo );
			}
			echo "town page " . Utils :: translit( $town -> title ) . " is created! id - " . $t_id . "\n";
		}
		else {
			$town_page_exist = end( $town_page_exist );
			$town_page_exist = ( object ) $town_page_exist;
			$town_id[ $f . '-' . $h ] = $town_page_exist -> id;
			$update =  'UPDATE `pages`
						SET `title2`=:title2,
						    `title`=:title,
							`meta_description`=:meta_description,
							`meta_keywords`=:meta_keywords,
							`head_title`=:head_title,
							`tag`=:tag
						WHERE `id`=:id';
			$table -> execute( $update, array(
				'title2' => $price,
				'title' => 'Такси ' . $f . ' - база отдыха ' . $h,
				'meta_description' => 'Цена такси до базы отдыха ' . $h . ' из ' . ChangeWords( $f ) . ' | официальный сайт такси STAR',
				'meta_keywords' => '',
				'head_title' => 'Заказать такси ' . $f . ' - база отдыха ' . $h,
				'tag' => 'Заказать такси ' . $f . ' - база отдыха ' . $h . ' за ' . $price . ' руб.',
				'id' => $town_page_exist -> id
			) );
			echo "town page " . $town_page_exist -> alias . " is exist! id - " . $town_page_exist -> id . "\n";
		}
	}
	
	
*/
	
	
	
	
	
	
	
	
	
	
	