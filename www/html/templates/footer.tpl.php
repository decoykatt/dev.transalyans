    <footer>
    <div class="container-fluid">
      <div class="row fblock " >
        <div class="container hcontainer">  
                <div class="col-md-12 col-xs-12 navbar">
                	<?php mod ('menus.show.footer') ?>
                </div>
        </div>

        <div class="container" style="padding: 0 !important; position: relative;"> 

          <div class="col-md-4 gklogo col-xs-12 col-sm-4">            
               <div class="textonlogo">
                  <a href="#"><img src="/static/img/map.svg"></a>
               </div>               
          </div> 

          <div class="col-md-3 col-xs-12 col-sm-4 number-zayavka"> 
            <img src="/static/img/icons/16_phone.svg" id="pik"><?php
                          echo ' <a id="an" href="tel:'.Utils::phone_number(val ('banner.show.phone')).'">'.val ('banner.show.phone').'</a>';                          
                        ?>
            <div>
              <button class="navbutton2" data-toggle="modal" data-target="#ostavit_zayavku_futer"><b>ОСТАВИТЬ ЗАЯВКУ</b></button>
            </div>

         
             
                
          </div> 
          
          <div class="col-md-3 col-xs-12 col-sm-4 foot-adress">     
               <div>
                <img src="/static/img/icons/1_placeholder.svg">   
                <p>г. Екатеринбург<br> ул. Вишневая, д. 69, лит. С, оф. 302</p>
                <div class="mail-padding">
                  <img src="/static/img/icons/2_mail.svg">
                  <?php
                                    echo '<a href="mailto:'.(val ('banner.show.email')).'">'.val ('banner.show.email').'</a>';
                                ?>
                </div>
                
               </div>

          </div>
           

        </div>

        <div class="container" style="padding: 0 !important">          
            <div class="col-md-4 col-xs-12 col-sm-4">         
              <div class="prava">
                <p>© ГК ТРАНСАЛЬЯНС. Все права защищены. 2017 г.</p>
                <a class="pointer_a" id="trigger-overlay-6" type="button">Политика конфиденциальности</a> 
              </div>
            </div>

            <div class="col-md-3 col-xs-12 col-sm-4">
              <div class="socials">
                <ul style="padding: 0;" class="centered">
                    <li class="contlist2 centered">
                    <div class="lilcircle2"><a href="#"><i class="fa fa-vk" aria-hidden="true"></i></a></div>
                    </li>
                    <li class="contlist2 centered">
                    <div class="lilcircle2"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></div>
                    </li>
                    <li class="contlist2 centered">
                    <div class="lilcircle2"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></div>
                    </li>
                    <li class="contlist2 centered">
                    <div class="lilcircle2"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></div>
                    </li>
                    <li class="contlist2 centered">
                    <div class="lilcircle2"><a href="#"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a></div>
                    </li>
                    <li class="contlist2 centered">
                    <div class="lilcircle2"><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></div>
                    </li>
                </ul>
              </div>
            </div>  

              <div class="col-md-5 col-xs-12 lf-text col-sm-4">
                <a style="padding-right: 19px;" href="http://it-f.pro/">Создание сайта - It Factory</a>
                <div class="logo-factory">                  
                 <a href="http://it-f.pro/"><img src="/static/img/logo_factory.png"></a>
                </div>       
              </div>


        </div>


        
      
      </div>
    </div>
    </footer>

    <div class="overlay overlay-hugeinc">
            <button type="button" class="overlay-close">Close</button>
            <h2>Политика конфиденциальности</h2>
            <p>
                <strong>Основные термины:</strong><br>
Администрация сайта – уполномоченные сотрудники на управления сайтом, действующие от имени Интернет-сайта
gktas.ru, которые организуют и (или) осуществляет обработку персональных данных, а также определяет цели
обработки персональных данных, состав персональных данных, подлежащих обработке, действия (операции), совершаемые
с персональными данными.<br><br>
Персональные данные – любая информация, относящаяся к прямо или косвенно определенному или определяемому
физическому лицу (субъекту персональных данных).<br><br>
Обработка персональных данных – любое действие (операция) или совокупность действий (операций), совершаемых с
использованием средств автоматизации или без использования таких средств с персональными данными, включая сбор,
запись, систематизацию, накопление, хранение, уточнение (обновление, изменение), извлечение, использование, передачу
(распространение, предоставление, доступ), обезличивание, блокирование, удаление, уничтожение персональных данных.
Конфиденциальность персональных данных – обязательное для соблюдения Оператором или иным получившим доступ к
персональным данным лицом требование не допускать их распространения без согласия субъекта персональных данных или
наличия иного законного основания.<br><br>
Пользователь Интернет-сайта (далее – Пользователь) – лицо, имеющее доступ к Сайту, посредством сети Интернет и
использующее Сайт.<br><br>
Файл cookie – это небольшой файл, который содержит строку символов и отправляется на ваш компьютер, когда вы заходите
на определенную страницу. Веб-сайт с его помощью идентифицирует браузер при вашем повторном посещении. Такие
файлы используются в разных целях, например позволяют запоминать ваши настройки. Пользователи могут запретить
браузеру сохранять файлы cookie или включить уведомления о них. Однако это иногда приводит к некорректной работе
сайтов и сервисов.<br><br>
IP-адрес — уникальный сетевой адрес узла в компьютерной сети, построенной по протоколу IP.<br>
<span class="centered"><strong>1. Общие положения</strong></span>
1.1. Использование Пользователем Сайта означает согласие с настоящей Политикой конфиденциальности и условиями
обработки персональных данных Пользователя.<br>
1.2. В случае несогласия с условиями Политики конфиденциальности Пользователь должен прекратить использование
Сайта.<br>
1.3. Настоящая Политика конфиденциальности применяется только к Сайту gktas.ru. Данный сайт не
контролирует и не несет ответственность за сайты третьих лиц, на которые Пользователь может перейти по ссылкам,
доступным на Сайте.<br>
1.4. Администрация сайта не проверяет достоверность персональных данных, предоставляемых Пользователем сайта.<br>
1.5. Текст и условия настоящей Политики конфиденциальности в любое время могут быть изменены Администрацией сайта
без предварительного уведомления. При несогласии Пользователя с внесенными изменениями, условиями использования
Пользователем материалов и сервисов Сайта он обязан отказаться от доступа к Сайту, прекратить использование
материалов и сервисов Сайта со дня вступления в силу внесенных изменений.<br>
1.6. Новая Политика конфиденциальности вступает в силу с момента ее размещения на Сайте, если иное не предусмотрено
новой редакцией Политики конфиденциальности.<br>
<span class="centered"><strong>2. Предмет Политики конфиденциальности</strong></span>
2.1. Настоящая Политика конфиденциальности устанавливает обязательства Администрации сайта по неразглашению и
обеспечению режима защиты конфиденциальности персональных данных, которые Пользователь предоставляет по запросу
Администрации сайта при регистрации на сайте или при оформлении заказа для приобретения Товара.<br>
2.2. Персональные данные, разрешённые к обработке в рамках настоящей Политики конфиденциальности, предоставляются
Пользователем путём заполнения регистрационной формы на Сайте gktas.ru и включают в себя следующую
информацию:</p>
<ul>
    <li>2.2.1. фамилию, имя, отчество Пользователя;</li>
    <li>2.2.2. контактный телефон Пользователя;</li>
    <li>2.2.3. адрес электронной почты (e-mail);</li>
    <li>2.2.4. почтовый адрес Пользователя;</li>
</ul>
<p>2.3. Интернет-сайт защищает Данные, которые автоматически передаются в процессе просмотра рекламных блоков и при
посещении страниц, на которых установлен статистический скрипт системы ("пиксель"):</p>
<ul class="dotted">
    <li>IP адрес;</li>
    <li>информация из cookies;</li>
    <li>информация о браузере (или иной программе, которая осуществляет доступ к показу рекламы);</li>
    <li>время доступа;</li>
    <li>адрес страницы, на которой расположен рекламный блок;</li>
    <li>реферер (адрес предыдущей страницы).</li>
</ul>
<p>Отключение cookies может повлечь невозможность доступа к некоторым функциональным частям Сайта.<br>
Интернет-сайт осуществляет сбор статистики об IP-адресах своих посетителей. Данная информация используется с целью
выявления и решения технических проблем, для контроля законности проводимых финансовых платежей.<br>
<span class="centered"><strong>3. Цели сбора персональной информации Пользователя</strong></span>
3.1. Персональные данные пользователя могут использоваться в следующих целях:</p>
<ul>
    <li>3.1.1. установления с Пользователем обратной связи, включая направление уведомлений, запросов, касающихся
использования Сайта, обработки запросов и заявок от Пользователя;</li>
    <li>3.1.2. определения местонахождения Пользователя для обеспечения безопасности, предотвращения мошенничества;
подтверждения достоверности и полноты персональных данных, предоставленных Пользователем;</li>
    <li>3.1.3. предоставления Пользователю эффективной клиентской и технической поддержки при возникновении проблем,
связанных с использованием Сайта;</li>
    <li>3.1.4. предоставления доступа Пользователю на сайты или сервисы партнеров</li>
</ul>
<p><span class="centered"><strong>4. Способы и сроки обработки Персональной информации</strong></span>
4.1. Обработка персональных данных Пользователя осуществляется без ограничения срока, любым законным способом, в
том числе в информационных системах персональных данных с использованием средств автоматизации или без
использования таких средств.<br>
4.2. Пользователь соглашается с тем, что Администрация сайта вправе передавать персональные данные третьим лицам, в
частности, курьерским службам, организациями почтовой связи, операторам электросвязи, исключительно в целях сбора и
обработки статистической информации, выполнения заказа, включая доставку Товара.<br>
4.3. Персональные данные Пользователя могут быть переданы уполномоченным органам государственной власти
Российской Федерации только по основаниям и в порядке, установленным законодательством Российской Федерации.<br>
4.4. Администрация сайта принимает необходимые организационные и технические меры для защиты персональной
информации Пользователя от неправомерного или случайного доступа, уничтожения, изменения, блокирования,
копирования, распространения, а также от иных неправомерных действий третьих лиц.<br>
<span class="centered"><strong>5. Обязательства сторон</strong></span>
<strong>5.1. Пользователь обязан:</strong><br>
5.1.1. Предоставить информацию о персональных данных, необходимую для пользования Сайтом интернет-магазина.<br>
5.1.2. Обновить, дополнить предоставленную информацию о персональных данных в случае изменения данной информации.<br>
<strong>5.2. Администрация сайта обязана:</strong><br>
5.2.1. Использовать полученную информацию исключительно для целей, указанных в п. 4 настоящей Политики
конфиденциальности.<br>
5.2.2. Обеспечить хранение конфиденциальной информации в тайне, не разглашать без предварительного письменного
разрешения Пользователя, а также не осуществлять продажу, обмен, опубликование, либо разглашение иными возможными
способами переданных персональных данных Пользователя, за исключением п.п. 4.2. и 4.3. настоящей Политики
Конфиденциальности.<br>
5.2.3. Принимать меры предосторожности для защиты конфиденциальности персональных данных Пользователя согласно
порядку, обычно используемого для защиты такого рода информации в существующем деловом обороте.<br>
5.2.4. Осуществить блокирование персональных данных, относящихся к соответствующему Пользователю, с момента
обращения или запроса Пользователя или его законного представителя либо уполномоченного органа по защите прав
субъектов персональных данных на период проверки, в случае выявления недостоверных персональных данных или
неправомерных действий.
            </p>
        </div>

        
    <div class="modal fade" id="ostavit_zayavku_heder" role="dialog">
        <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">ОСТАВИТЬ ЗАЯВКУ</h4>
                    </div>
                    <div class="modal-body">
                      <?php mod( 'catalog.action.forms', array( 'alias' => 'ostavit_zayavku_heder' ) )?>                        
                        
                    </div>
                </div>
        </div>
    </div>

    <div class="modal fade" id="ostavit_zayavku_futer" role="dialog">
        <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">ОСТАВИТЬ ЗАЯВКУ</h4>
                    </div>
                      <div class="modal-body">
                      <?php mod( 'catalog.action.forms', array( 'alias' => 'ostavit_zayavku_futer' ) )?>                        
                        
                    </div>
                </div>
        </div>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAYg29Z7KaocCNIlQWIyf4giZAVw22e3SQ"></script>
    <script src="/static/js/jquery-3.2.1.min.js"></script>
    <script src="/static/js/jquery-ui.min.js"></script>
	  <script src="/ajax/?mod=catalog.action.forms_js&debug"></script>
    <script src="/static/js/slick.js"></script>
    <script src="/static/js/action.js"></script>
    <script src="/static/js/google_offer.js"></script>
    <script src="/static/js/arrow.js"></script>
    <script src="/static/js/fixed.js"></script>
    <script src="/static/js/bootstrap.js"></script>    
    <script src="/static/js/slider.js"></script>
    <script src="/static/js/calc.js"></script>
    <script src="/static/js/sempai.js"></script>
    <script src="/static/js/classie.js"></script>
    <script src="/static/js/demo1.js"></script>
    <script>
    an.addEventListener("mouseover", function() {pik.style.animation = "shake 0.5s 3";});
    an.addEventListener("mouseout", function() {pik.style.animation = "";});  
    ann.addEventListener("mouseover", function() {pikk.style.animation = "shake 0.5s 3";});
    ann2.addEventListener("mouseout", function() {pikk2.style.animation = "";});
    ann2.addEventListener("mouseover", function() {pikk2.style.animation = "shake 0.5s 3";});
    ann.addEventListener("mouseout", function() {pikk.style.animation = "";});
    pik.addEventListener("mouseover", function() {pik.style.animation = "shake 0.5s 3";});
    pik.addEventListener("mouseout", function() {pik.style.animation = "";});  
    pikk.addEventListener("mouseover", function() {pikk.style.animation = "shake 0.5s 3";});
    pikk.addEventListener("mouseout", function() {pikk.style.animation = "";});
    pikk2.addEventListener("mouseover", function() {pikk2.style.animation = "shake 0.5s 3";});
    pikk2.addEventListener("mouseout", function() {pikk2.style.animation = "";});
    </script>
	<?php mod( 'banner.show.counters' )?>
</body>
</html>