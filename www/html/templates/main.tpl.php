<?php include "html/templates/header.tpl.php" ?>


<div id="preloader-ajax">
    <div id="preloader-ajax-center"> 
        <div id="preloader-ajax-center-absolute">
            <div class="object" id="object_one"></div>
            <div class="object" id="object_two"></div>
            <div class="object" id="object_three"></div>
            <div class="object" id="object_four"></div>
        </div>
    </div>
</div>

<section class="carousel1 carousel1_main">

	<?php mod ('catalog.action.main_slider') ?>
            
    </section>   
    
    <section class="main1" id="main1">
        <div class="container-fluid">
            <div class="row">
                <div class="container">

                	<?php mod ('pages.show.content') ?>
                    
                </div>
            </div>
        </div>
    </section>
   
    <section class="adv21" id="adv21">
        <div class="container-fluid grey">
            <div class="row">
                <div class="container">
                    <div class="col-sm-12">
                        <h1 class="centered">ЧТО ВЫ ПОЛУЧАЕТЕ, РАБОТАЯ С НАМИ?</h1>
                        <ul>

                        <?php mod('catalog.action.bonus') ?>                            
                            
                        </ul>
                    </div>        
                </div>
            </div>
        </div>
    </section>
    
    <section class="tel">
        <div class="container-fluid">
            <div class="row">
                <div class="color_zayava"></div>
                <div class="container color_zayava_upless">
                    <div class="col-sm-12 centered">
                        <h1>ДЛЯ ОФОРМЛЕНИЯ ЗАЯВКИ — ЗВОНИТЕ ПО НОМЕРУ</h1>
                    </div>
                    <div class="col-sm-12 centered">
                        <?php
                          echo ' <a href="tel:'.Utils::phone_number(val ('banner.show.phone')).'">'.val ('banner.show.phone').'</a>';
                        ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
       
    <section class="sotr" id="sotr">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <h1 class="centered">КАК ПРОИСХОДИТ СОТРУДНИЧЕСТВО</h1>              
                    <div class="col-sm-12">

                    	<?php mod('catalog.action.coop') ?>
                       
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="cont grey">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <h1 class="centered">ЗАПОЛНИТЬ ФОРМУ ОБРАЩЕНИЯ</h1>

                    <?php mod( 'catalog.action.forms', array( 'alias' => 'forma_obracsheniya' ) )?>

                </div>
            </div>
        </div>
    </section>
    
    
    <section class="down" id="down">
        <div class="container-fluid">
            <div class="container">
                <div class="centered col-xs-12"><h1 id="map_showing">КОНТАКТЫ</h1></div>
                <div class="row mobilecontact">

                    <div class="col-sm-3">
                        <div class="row bangap">
                            <div class="col-sm-2"><img src="/static/img/icons/16_phone.svg" id="pikk"></div>
                            <div class="col-sm-10">
                                <span class="mobilecontact-tel"><?php
                              echo ' <a id="ann" href="tel:'.Utils::phone_number(val ('banner.show.phone_city')).'">'.val ('banner.show.phone_city').'</a>';
                                ?></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2"><img src="/static/img/icons/16_phone.svg" id="pikk2"></div>
                            <div class="col-sm-10">
                                <span class="mobilecontact-tel"><?php
                              echo ' <a id="ann2" href="tel:'.Utils::phone_number(val ('banner.show.phone')).'">'.val ('banner.show.phone').'</a>';
                                ?></span>
                            </div>
                        </div>
                        
                        
                    </div>

                    <div class="col-sm-3">
                        <div class="row">
                        <div class="col-sm-2"><img src="/static/img/icons/1_placeholder.svg"></div>
                        <div class="col-sm-10">
                                г. Екатеринбург<br> ул. Вишневая, д. 69, лит. С, оф. 302
                            </div>
                        </div>
                        
                        
                        
                    </div>

                    <div class="col-sm-3">
                        <div class="row">
                        <div class="col-sm-2"><img src="/static/img/icons/2_mail.svg"></div>
                        <div class="col-sm-10"><?php
                                    echo '<a href="mailto:'.(val ('banner.show.email')).'">'.val ('banner.show.email').'</a>';
                                ?></div>    
                        </div>
                        
                    </div>

                    <div class="col-sm-3">
                        <ul>
                            <li class="centered">
                                <a href="#">
                                    <div class="lilcircle"><i class="fa fa-vk" aria-hidden="true"></i></div>
                                </a> 
                            </li>
                               
                            <li class="centered">
                                <a href="#">
                                    <div class="lilcircle"><i class="fa fa-facebook" aria-hidden="true"></i></div>
                                </a>
                            </li>
                            
                            <li class="centered">
                                <a href="#">
                                    <div class="lilcircle"><i class="fa fa-twitter" aria-hidden="true"></i></div>
                                </a>
                            </li>
                            
                            <li class="centered">
                                <a href="#">
                                    <div class="lilcircle"><i class="fa fa-instagram" aria-hidden="true"></i></div>
                                </a>
                            </li>
                            
                            <li class="centered">
                                <a href="#">
                                    <div class="lilcircle"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></div>
                                </a>
                            </li>
                                                        
                            <li class="centered">
                                <a href="#">
                                    <div class="lilcircle"><i class="fa fa-youtube-play" aria-hidden="true"></i></div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>



       <div class="container-fluid hz-che">
        <div class="row">     
            <div class="col-xs-12 col-md-12 mapp" style="padding-left: 0!important; padding-right: 0!important;">              
            <div id="map" <?php echo 'data-lat="'.val ('banner.show.width').'" data-lon="'.val ('banner.show.longitude').'" data-zoom="'.val ('banner.show.zoom').'"'; ?> style="position: relative; overflow: hidden;">
            </div>
          </div>
        </div>
      </div>
      <div class="container-fluid maim-block" id="maims">
        <div class="row">
            <div class="container no-mobile-padding">
                
                <div class="mainblock col-xs-12">                
                    <?php mod('catalog.action.timer') ?>
                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 mobilet" style="margin:0">
                      <div class="timer" <?php

                        $table = new Table( 'catalog_section' );
                        $rows = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => 'tajmer' ) );
                        $row = end( $rows );
                        $section = $table -> select( 'SELECT * FROM `' . $row[ 'section_table' ] . '` WHERE `id`=:id LIMIT 1', array( 'id' => $row[ 'id' ] ) );
                        $section = end( $section );

                        $true_date = date('d/m/Y',$section [ 'datestamp_over' ] );


                        function create_countdown( $true_date ) {
                            $ex_date = explode( '/', $true_date );
                            $now_time = time( );
                            $end_time = mktime( 0, 0, 0, $ex_date[ 1 ], $ex_date[ 0 ], $ex_date[ 2 ] );
                            if ( $end_time - $now_time < 1 ) {
                                $end_time += 259200;
                                return create_countdown( date( 'd/m/Y', $end_time ) );
                            }
                            /*echo $end_time;*/
                            return date( 'Y/m/d', $end_time );
                        }                     

                        echo 'data-time="' .create_countdown( $true_date ). '"';
                            ?> 
                        >
                        <h3>До конца спецпредложения осталось: </h3> 
                        <div class="timerpic">
                           <img src="/static/img/timer-pic.png">
                           <script src="/static/js/jquery-3.2.1.min.js"></script>
                         <script src="/static/js/timer.js"></script>
                     <script src="/static/js/timer_0x767.js"></script>
                        </div>          
                </div>

               <div class="phoneform">
                  <div class="form-group">
                      <?php mod( 'catalog.action.forms', array( 'alias' => 'zayavka_akciya' ) )?>
                </div>
                </div>  
              </div>    
              
            </div>
        </div>
      </div>
    </div>

    



<?php include "html/templates/footer.tpl.php" ?>