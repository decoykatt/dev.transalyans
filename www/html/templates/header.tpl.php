<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?php $title = $registry->title; if($title) echo $title; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="<?php out('description')?>" />
    <meta name="keywords" content="<?php out('keywords')?>" />
    <base href="<?php echo $base ?>" />
    <?php mod('catalog.editor.header')?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="/static/css/style.css">    
    <link rel="stylesheet" href="/static/css/bootstrap.css">
    <link rel="stylesheet" href="/static/css/forms_js.css">
    <link rel="stylesheet" href="/static/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="/static/css/style3.css">
    <link rel="stylesheet" href="/static/css/font-awesome.min.css">
    <link rel="stylesheet" href="/static/css/slick.css">
    <link rel="stylesheet" href="/static/css/slick-theme.css">
    <link rel="stylesheet" href="/static/css/jquery-ui.min.css">
    <script src="/static/js/modernizr.custom.js"></script>
    <link rel="stylesheet" type="text/css" href="/static/css/full/normalize.css" />
    <link rel="stylesheet" type="text/css" href="/static/css/full/demo.css" />
    <link rel="stylesheet" type="text/css" href="/static/css/full/style1.css" />
    <?php mod('catalog.action.change_color') ?>
</head>
            
<body>
    <div class="arrow fixed" id="arr">
               <a id="arrow-up" href="#"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
            </div>
    <header>

        <div class="container-fluid" id="header_cont_fluid">
            <div class="row">
                <div class="container bot-pad">
                    <div class="col-sm-3 logo">
                        <a href="#"><img src="/static/img/map.svg"></a>
                    </div>

                    <div class="col-sm-3 numb">
                        <?php
                          echo ' <a href="tel:'.Utils::phone_number(val ('banner.show.phone_city')).'"><h3>'.val ('banner.show.phone_city').'</h3></a>';
                        ?>
                    </div>
                        
                    <div class="col-sm-3 buttxd">
                        <button data-toggle="modal" data-target="#ostavit_zayavku_heder">ОСТАВИТЬ ЗАЯВКУ</button>
                    </div>

                    <div class="col-sm-3 contacts">
                        <div class="row">
                            <div class="col-sm-2"><img src="/static/img/icons/1_placeholder.svg"></div>
                            <div class="col-sm-10">
                                <a href="#map" class="jakor"><p>г. <?php 
                                    $ip = $_SERVER['REMOTE_ADDR'];
                                    $a = geoip_record_by_name($ip);
                                    $city =  $a['city'] ;
                                    $file_read = 'RU_write.data';
                                    $handle_read = file( $file_read );
                                    if ( $handle_read ) {
                                        foreach ( $handle_read as $key => $str ) {
                                            $str = trim($str);
                                            $splashed = explode( '|', $str );
                                            $search = explode( ',' , $splashed[0]);
                                            foreach ($search as $key => $value) {
                                                if ( $value == $city) {
                                                    $city = $splashed[1];
                                                    break;
                                                } 
                                            }
                                        }
                                    }
                                    echo $city;
                                 ?></p></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2"><img src="/static/img/icons/2_mail.svg"></div>
                            <div class="col-sm-10">
                                <?php
                                    echo '<a href="mailto:'.(val ('banner.show.email')).'">'.val ('banner.show.email').'</a>';
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="menu1" class="col-sm-12 default">
                    <div class="container">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainNav" style="border-color: #fff;">
                            <span class="icon-bar" style="background-color: #fff;"></span>
                            <span class="icon-bar" style="background-color: #fff;"></span>
                            <span class="icon-bar" style="background-color: #fff;"></span>
                        </button>
                        
                        <div class="collapse navbar-collapse" id="mainNav">
                        	<?php mod ('menus.show.header-menu') ?>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </header>