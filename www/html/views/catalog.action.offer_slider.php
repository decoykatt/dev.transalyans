<?php

$table = new Table( 'position_slides' );

$rows = $table -> select( "SELECT * FROM `position_slides_offer` WHERE `visible`=1 ORDER BY `position`" );


$goroda = val ('pages.show.title');
$goroda = trim($goroda);
$razdeliel = explode('—', $goroda);
$gorod_1 = $razdeliel[0];
$gorod_2 = $razdeliel[1];
$mesta_gorodov = file('./dt_towns_latlon.data');

foreach ($mesta_gorodov as $key => $value) {
    $goroda = trim($value);
    $goroda = explode('|', $goroda);
    if ($goroda[0] == trim($gorod_1)) {
        $gorod_1_location = $goroda[1];
    }
    if ($goroda[0] == trim($gorod_2)) {
        $gorod_2_location = $goroda[1];
    }
}

    $gorod_1_location = explode(' ', $gorod_1_location);
    $gorod_2_location = explode(' ', $gorod_2_location);
    $gorod_1_location_1p = $gorod_1_location[0]*1;
    $gorod_1_location_2p = $gorod_1_location[1]*1;
    $gorod_2_location_1p = $gorod_2_location[0]*1;
    $gorod_2_location_2p = $gorod_2_location[1]*1;

	echo '<div class="carous">';

		foreach ( $rows as $row ) {
			echo '
				<div class="slide-item"><img class="slide_offer_img" src="/' .$row[ 'img' ]. '">

                    <div id="cswipe" class="col-sm-12 mocha-dino offer_slider_all">
                        <div class="container car-cont">
                            <div class=" col-md-offset-1 col-md-10 offer_slider_wrap">
                                <h1>' .$row[ 'title' ]. '</h1>
                                <div class="col-xs-12 col-md-6 offer_slider_p_l nopadd none_display_city">
                                    <p class="city_1_offer" data-location="'.$gorod_2_location_1p.','.$gorod_2_location_2p.'"><img src="/static/img/offer_slider.png">' .$gorod_1. '</p>
                                    <div class="libe"></div>
                                </div>
                                <div class="col-xs-12 col-md-6 offer_slider_p_r nopadd none_display_city">
                                    <p class="city_2_offer" data-location="'.$gorod_1_location_1p.','.$gorod_1_location_2p.'"><img src="/static/img/offer_slider.png">' .$gorod_2. '</p>
                                </div>
                                <form id="f_forma_slajder" method="post" novalidate="">
                                <input type="hidden" name="form_act" value="resender" tabindex="0">
                                <div class="col-xs-12 col-md-6 offer_slider_p_l nopadd">
                                    <p class="city_1_offer_saaa" data-location="'.$gorod_2_location_1p.','.$gorod_2_location_2p.'"><img src="/static/img/offer_slider.png">
                        <input type="text" required="" class="form-control ui-autocomplete-input" name="from"  value="'.$gorod_1.'" autocomplete="off" tabindex="0"></p>
                                    <div class="libe"></div>
                                </div>
                                <div class="col-xs-12 col-md-6 offer_slider_p_r nopadd">
                                <input type="hidden" name="form_alias" value="forma_slajder" tabindex="0">
                                    <p class="city_2_offer_saaa" data-location="'.$gorod_1_location_1p.','.$gorod_1_location_2p.'"><img src="/static/img/offer_slider.png">
                        <input type="text" required="" class="form-control ui-autocomplete-input" name="where" value="'.$gorod_2.'" autocomplete="off" tabindex="0"></p>
                                </div>
                                <button type="submit" tabindex="0">ПЕРЕЙТИ</button>
                                </form>
                                <img class="offer_car" src="/static/img/offer_slider_car.png">
                            </div>
                        </div>  
                    </div>

                </div>
			';

		}

	echo '</div>';
