<?php 

$table = new Table( 'position_slides' );

$rows = $table -> select( "SELECT * FROM `section_change_color` LIMIT 1" );
$row = end( $rows );
$css = '';

$css = '
	<style>

	header 
	{
	    background-color: '.$row['Header'].';
	}

	header .col-sm-12, .arrow
	{
	    background-color: '.$row['Header_navbar'].';
	}

	header .col-sm-12 a
	{
	    border-bottom: 4px solid '.$row['Header_navbar'].';
	}

	header .col-sm-3 h3 {
		color: '.$row['elements_color'].';
	}

	.down a:hover, header .col-sm-3 a:hover {
		color: '.$row['elements_color'].'!important;
		border-bottom: 1px dotted '.$row['elements_color'].'!important;
	}

	.down .lilcircle:hover {
		border-color: '.$row['elements_color'].';
		color: '.$row['elements_color'].';
	}

	.number-zayavka a, .number-zayavka a:hover {
		color: '.$row['elements_color'].';
	}

	.foot-adress a:hover, .lilcircle2 a:hover {
		color: '.$row['elements_color'].';
	}

	.mobilecontact-tel a {
		color: '.$row['elements_color'].'!important;
	}

	.hcontainer ul li a:hover {
		color: '.$row['elements_color'].'!important;
		border-bottom-color: '.$row['elements_color'].';
	}

	.lilcircle2:hover {
		border: 2px solid '.$row['elements_color'].';
	}

	.tel a {
		color: '.$row['elements_color'].';
	}

	.tel a:hover {
		color: '.$row['elements_color'].';
		border-bottom: 6px dashed '.$row['elements_color'].';
	}

	#menu1.fixed .header-tel {
		color: '.$row['elements_color'].';
	}

	header button, .carousel1 .mocha-dino button, .cont button, .yb button, .navbutton2, .offer_form button {
		background-color: '.$row['elements_color'].';
	}

	header .col-sm-12 a:hover {
		color: '.$row['elements_color'].';
		border-bottom-color: '.$row['elements_color'].';
	}

	.carousel1 .col-sm-7 h1
	{
	    background-color:  '.$row['Slider_h1'].';
	}

	.offer_slider_wrap {
    	background-color: '.$row['slider_form_offer'].';
	}

	.offer_slider_p_l p, .offer_slider_p_r p {
		background-color: '.$row['Slider_city_back'].';
	}

	.carousel1 .col-sm-7 h2
	{
	    background-color:  '.$row['Slider_h2'].';
	}

	.carousel1 .col-sm-5
	{
	    background-color:  '.$row['Slider_form'].';
	}

	.color_slider {
		background-color: '.$row['Slider_back'].';
	}

	.color_zayava {
		background-color: '.$row['banner'].';
	}

	.color_gori {
		background-color: '.$row['gori'].';
	}

	.down
	{
	    background-color:  '.$row['contacts'].';

	}

	.fblock{
		background-color: '.$row['footer'].';
	}

	</style>

	';


echo $css;

