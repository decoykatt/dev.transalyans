<?php

$str = '';

$table = new Table( 'position_slides' );

$rows = $table -> select( "SELECT * FROM `position_slides` WHERE `visible`=1 ORDER BY `position`" );

$display = '';
if ( count($rows) == 1 ) {
    $display = 'none';
} else {
    $display = 'block';
}

	echo '<div class="carous">';

		foreach ( $rows as $row ) {
			echo '
				<div class="slide-item"><img src="/' .$row[ 'img' ]. '">
                <div class="color_slider"></div>

                <div id="cswipe" class="col-sm-12 mocha-dino">
                    <div class="container car-cont">
                        <div id="prev" style="display:'.$display.'">
                        <img src="/static/img/arr_left.png" id="previmg">
                        </div>
                        <div class="col-sm-7">
                        <h1>' .$row[ 'title' ]. '</h1>
                            <h2>' .$row[ 'title2' ]. '</h2>
                        </div>

                        <div class="col-sm-5 slider-object">
                            ' .val('catalog.action.slider-form'). '
                        </div>
                        <div id="next" style="display:'.$display.'">
                        <img src="/static/img/arr_right.png" id="nextimg">
                        </div>
                    </div>  
                </div>

                </div>
			';

		}

	echo '</div>';
