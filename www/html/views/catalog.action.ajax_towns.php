<?php


$moy_json = array ('s' => true, 'data' => array() );

$table = new Table( 'catalog_section' );

$rows = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => 'goroda' ) );
$row = end( $rows );

$towns = $table -> select( 'SELECT * FROM `position_setting` WHERE `section_id`=:parent_id ORDER BY `position`', array( 'parent_id' => $row['id'] ) );

$moy_json['data'] = $towns;

$moy_json = json_encode($moy_json);

header("Content-type:application/json");

echo $moy_json;