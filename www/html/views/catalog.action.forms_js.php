<?php

	$js = '';
	$app = '';
	$rcrm = '';

	$referer = str_replace( 'http://', '', ( isset( $_SERVER[ 'HTTP_REFERER' ] ) ) ? $_SERVER[ 'HTTP_REFERER' ] : '' );
	$e_referer = explode( '/', $referer );

	$url = ( isset( $e_referer[ 0 ] ) && trim( $e_referer[ 0 ] ) != '' ) ? 'http://' . $e_referer[ 0 ] : '';
	$uri = ( isset( $e_referer[ 1 ] ) && trim( $e_referer[ 0 ] ) != '' ) ? '/' . $e_referer[ 1 ] : '';

	$table = new Table( 'section_forms' );
	
	$forms = $table -> select( "SELECT `t2`.`title`,`t2`.`position`,`t2`.`alias`,`t1`.* FROM `section_forms` as `t1` LEFT JOIN `catalog_section` AS `t2` ON `t1`.`id`=`t2`.`id` WHERE (`t1`.`method`='ajax-post' || `t1`.`method`='ajax-get') ORDER BY `t2`.`position`" );

	if ( !count( $forms ) ) return false;
	
		//////////////////////////////////////////////////////////////
		// S E T T I N G   F O R M
		// Настройки форм
		$settings = $table -> select( 'SELECT `t1`.* FROM `section_forms_settings` as `t1` INNER JOIN `catalog_section` as `t2` ON (`t1`.`id`=`t2`.`id`) WHERE `t2`.`alias`="forms_section" LIMIT 1' );
		if ( empty( $settings ) || !count( $settings ) ) return false;
		$settings = end( $settings );
		/////////////////////////////////////////////////////////////


	$js .= "
;( function( $, undefined ) {

	$( window  ).on( 'load', function() {

		window.formAjax = {

			conf: {
				url: '" . $url . "',
				uri: '" . $uri . "',
				bg: '#ebebeb',
				errBg: '#e4ae99',
				preloader: function() {
					var arr = [];
					arr.push( '<div id=\"preloader-ajax\">' );
					arr.push( 	'<div id=\"preloader-ajax-center\">' );
					arr.push(		'<div id=\"preloader-ajax-center-absolute\">' );
					arr.push(			'<div class=\"object\" id=\"object_one\"></div>' );
					arr.push(			'<div class=\"object\" id=\"object_two\"></div>' );
					arr.push(			'<div class=\"object\" id=\"object_three\"></div>' );
					arr.push(			'<div class=\"object\" id=\"object_four\"></div>' );
					arr.push(		'</div>' );
					arr.push(	'</div>' );
					arr.push( '</div>' );
					return arr.join( '' );
				},

			},";


	foreach ( $forms as $form ) {

		$method = str_replace( 'ajax-', '', $form[ 'method' ] );
		$method = mb_strtoupper( $method );
	
		// для выхода другое событие
		if ( $form[ 'action' ] == 'logout' ) {
			;
		}
		else {
			// application concat
			$app .= "
				$( '#" . $form[ 'html_id' ] . "' ).on( 'click', 'button[type=\"submit\"]', function() {

					$( '#" . $form[ 'html_id' ] . "' ).find( '.err-tooltip' ).remove();
					$( '#" . $form[ 'html_id' ] . "' ).hide();
					$( '#" . $form[ 'html_id' ] . "' ).after( window.formAjax.conf.preloader );";

			////////////////////////////////////////////////////////////
			// Яндекс Метрика = вешаем событие на кнопки
			if ( isset( $settings[ 'ya_metrika_id' ] ) && isset( $form[ 'ya_metrika_target_id_button' ] ) ) {
				$app .= "
							console.log( 'button : ' + yaCounter" . $settings[ 'ya_metrika_id' ] . " );
					if ( yaCounter" . $settings[ 'ya_metrika_id' ] . " !== 'undefined' ) {
							yaCounter" . $settings[ 'ya_metrika_id' ] . ".reachGoal( \"" . $form[ 'ya_metrika_target_id_button' ] . "\" );
					}";
			}
			///////////////////////////////////////////////////////////
			$app .= "
					window.formAjax." . $form[ 'alias' ] . "();
					return false;
				});
				";
		}

		$js .= "

			// " . $form[ 'title' ] . "
			" . $form[ 'alias' ] . ": function() {

				var selectors = {
					form_act: '#" . $form[ 'html_id' ] . " input[name=\"form_act\"]',
					form_alias: '#" . $form[ 'html_id' ] . " input[name=\"form_alias\"]',";

		$fields = $table -> select( "SELECT * FROM `position_forms` WHERE `section_id`=:sid ORDER BY `position`", array( 'sid' => $form[ 'id' ] ) );

		//if ( !count( $fields ) ) continue;

		$array_uniq = array( );

		foreach ( $fields as $field ) {

			if ( $field[ 'type_id' ] == 'submit' || $field[ 'type_id' ] == 'label' ) continue;

			if ( isset( $array_uniq[ $field[ 'nameid' ] ] ) ) continue;

			$array_uniq[ $field[ 'nameid' ] ] = true;

			if (	$field[ 'type_id' ] == 'text' ||
					$field[ 'type_id' ] == 'pwd' ||
					$field[ 'type_id' ] == 'file' ||
					$field[ 'type_id' ] == 'upload' ) {

				$js .= "
					" . $field[ 'nameid' ] . ": '#" . $form[ 'html_id' ] . " input[name=\"" . $field[ 'nameid' ] . "\"]',";

			}
			else if ( $field[ 'type_id' ] == 'select' ) {

				$js .= "
					" . $field[ 'nameid' ] . ": '#" . $form[ 'html_id' ] . " select[name=\"" . $field[ 'nameid' ] . "\"]',";

			}
			else if ( $field[ 'type_id' ] == 'memo' ) {

				$js .= "
					" . $field[ 'nameid' ] . ": '#" . $form[ 'html_id' ] . " textarea[name=\"" . $field[ 'nameid' ] . "\"]',";

			}
			else if ( $field[ 'type_id' ] == 'check' ) {

				$js .= "
					" . $field[ 'nameid' ] . ": '#" . $form[ 'html_id' ] . " input[name=\"" . $field[ 'nameid' ] . "\"]',";

			}
			else if ( $field[ 'type_id' ] == 'radiobox' ) {

				$js .= "
					" . $field[ 'nameid' ] . ": '#" . $form[ 'html_id' ] . " input[name=\"" . $field[ 'nameid' ] . "\"]:checked',";

			}

		}

		$js = substr( $js, 0, -1 );
		$js .= "
				}";

		$js .= "
				var formData = new FormData( $( '#" . $form[ 'html_id' ] . "' )[ 0 ] );
				var selectors_value = {};
				for ( var key in selectors ) {

					if ( ($( selectors[ key ] ).attr( 'type' ) == 'checkbox') || ($( selectors[ key ] ).attr( 'type' ) == 'radio') ) {
						selectors_value[ key ] = $( selectors[ key ] + ':checked' ).val( );
					}
					else {
						selectors_value[ key ] = $( selectors[ key ] ).val( );
					}
				}";


				$js .= "
				$.ajax({
					url: '/ajax/?mod=catalog.action.forms_ajax',
					dataType: 'JSON',
					cache: false,
					contentType: false,
					processData: false,
					method: '" . $method . "',
					data: formData,
				}).done( function( data ) {

					//console.log( data );
				
					$( '#" . $form[ 'html_id' ] . "' ).nextAll( '#preloader-ajax' ).remove();

					if ( data.s ) {";

				// форма выхода
				if ( $form[ 'action' ] == 'logout' ) {
					;
				}
				// остальные формы
				else {

					$js .= "
						for( var key in selectors ) {
							$( selectors[ key ] ).css( 'background-color', window.formAjax.conf.bg );
						}

						$( '#" . $form[ 'html_id' ] . "' ).after( '<p class=\"success-message\">' + data.mess + '</p>' );";


					//////////////////////////////////////////////////////////////
					// Y A N D E X   M E T R I K A
					$yandex_metrika_enabled = false;
					if (

							// настройка всех форм, синхронизация с метрикой
							isset( $settings[ 'ya_metrika_on' ] ) && $settings[ 'ya_metrika_on' ] &&
							// настройка всех форм, ид счетчика
							isset( $settings[ 'ya_metrika_id' ] ) && $settings[ 'ya_metrika_id' ] &&

							// настройки формы, синхронизация с метрикой
							isset( $form[ 'ya_metrika_on' ] ) && $form[ 'ya_metrika_on' ] &&
							// настройки формы, ид цели
							isset( $form[ 'ya_metrika_target_id' ] ) && $form[ 'ya_metrika_target_id' ]

					) $yandex_metrika_enabled = true;

					// Добавляем цель для метрики
					if ( $yandex_metrika_enabled ) {

						$js .= "
							console.log( 'target' + yaCounter" . $settings[ 'ya_metrika_id' ] . " );
							if ( yaCounter" . $settings[ 'ya_metrika_id' ] . " !== 'undefined' ) {
									yaCounter" . $settings[ 'ya_metrika_id' ] . ".reachGoal( \"" . $form[ 'ya_metrika_target_id' ] . "\" );
							}";

					}
					//////////////////////////////////////////////////////////////
					// R E T A I L   C R M
					$retailcrm_enabled = false;
					if (
							// настройка всех форм
							isset( $settings[ 'retailcrm_on' ] ) && $settings[ 'retailcrm_on' ] &&
							// настройки формы
							isset( $form[ 'retailcrm_on' ] ) && $form[ 'retailcrm_on' ]

					) $retailcrm_enabled = true;

					// Интеграция форм с retailcrm
					if ( $retailcrm_enabled ) {

						$js .= "
								_rc( 'send', 'order', {
								";

								// if( !empty( $settings['retailcrm_transaction_id'] ) ) {
								// 	$js .= "'customTransactionId': url('?transaction_id'),
								// 	";
								// }

								foreach ( $fields as $field ) {
									if ( empty( $field[ 'retailcrm_name' ] ) ) continue;
									$js .= "'" . $field[ 'retailcrm_name' ] . "': selectors_value[ '" . $field[ 'nameid' ] . "' ],
									";
								}

								if ( !empty( $form[ 'retailcrm_order_method' ] ) ) {
									$js .= "'orderMethod': '" . $form[ 'retailcrm_order_method' ] . "',
									";
								}

						$js .= "});
						";

					} // retailcrm enabled
				} // form action != 'logout'


				$js .= "

						if ( data.action != undefined && Object.keys( data.action ).length ) {
							window.formAjax.action_js( data.action );
						}

						console.log( 'Все ок' );

					}
					else {

						$( '#" . $form[ 'html_id' ] . "' ).show();

						for( var key in data.err ) {

							//console.log( selectors[ key ] );

							if ( key == 'system_message' ) continue;

							if ( $( selectors[ key ] ).hasClass( 'hidden-field' ) ) {
								$( selectors[ key ] ).next().append( '<div class=\"err-tooltip\">' + data.err[ key ] + '</div>' );
								$( selectors[ key ] ).next().css( 'background', window.formAjax.conf.errBg );
								$( selectors[ key ] ).on( 'click', function() {
									$( this ).next().removeAttr( 'style' );
									$( this ).next().find( '.err-tooltip' ).remove();
								});

								continue;
							}

							if ( $( selectors[ key ] ).attr('type') == 'radio' && !$( selectors[ key ] ).closest('.custom-radio').find('.err-tooltip').length ) {
								$( selectors[ key ] ).closest('.custom-radio').append( '<div class=\"err-tooltip\">' + data.err[ key ] + '</div>');

								$( selectors[ key ] ).on( 'click', function() {
									$( this ).closest('.custom-radio').find('.err-tooltip').remove();
								});

								continue;
							}

							$( selectors[ key ] ).css( 'background-color', window.formAjax.conf.errBg );
							$( selectors[ key ] ).after( '<div class=\"err-tooltip\">' + data.err[ key ] + '</div>' );

							$( selectors[ key ] ).on( 'click', function() {
								$( this ).css( 'background-color', window.formAjax.conf.bg );
								$( this ).siblings( '.err-tooltip' ).remove();
							});

						}

					}

				});

			},";

	}


	$js .= "
			action_js: function( arr ) {
				for( var key in arr ) {
					// перенаправления
					if ( key == 'location' ) {
						// текущая страничка
						if ( arr[ key ] == 'self' ) {
							location.href = window.formAjax.conf.url + window.formAjax.conf.uri;
						}
						else {
							location.href = window.formAjax.conf.url + '/' + arr[ key ];
						}
					}
					// ...
				}
			},

			app: function() {
				" . $app . "
			}
		}

		window.formAjax.app();

	});";


	// Инициализация захватчика форм для RetailCRM
	if ( isset( $settings[ 'retailcrm_key' ] ) && $settings[ 'retailcrm_key' ] ) {
		$rcrm .= "

(function(_,r,e,t,a,i,l){_['retailCRMObject']=a;_[a]=_[a]||function(){(_[a].q=_[a].q||[]).push(arguments)};_[a].l=1*new Date();l=r.getElementsByTagName(e)[0];i=r.createElement(e);i.async=!0;i.src=t;l.parentNode.insertBefore(i,l)})(window,document,'script','https://collector.retailcrm.pro/w.js','_rc');
_rc('create', '" . $settings[ 'retailcrm_key' ] . "');
_rc('send', 'pageView');

";

	}

 	$js .= $rcrm;


$js .= "})( jQuery )";


	header( 'Content-Type: application/javascript; charset=utf-8' );

	// минимизация js
	if ( !isset( $_GET[ 'debug' ] ) ) {
		$min_js = new Min_js();
		$js = $min_js -> squeeze( $js, true, false );
	}
	//

	echo $js;


