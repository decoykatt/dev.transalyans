<?php

	$pay_order_id = ( isset( $_GET[ 'orderId' ] ) ) ? $_GET[ 'orderId' ] : 'none';

	$pay = Registry :: __instance() -> pay;
	$order = $pay -> get_order( array( 'pay_order_id' => $pay_order_id ) );

	if ( !count( $order ) ) return false;

	$pay_status = $pay -> status( $pay_order_id );
	if ( isset( $pay_status[ 'OrderStatus' ] ) ) {

		echo '<h1>';
		if ( $pay_status[ 'OrderStatus' ] == 2 ) { // Проведена полная авторизация суммы заказа
			echo 'Успешная оплата';
		}
		else if (	$pay_status[ 'OrderStatus' ] == 0 || // Заказ зарегистрирован, но не оплачен
					$pay_status[ 'OrderStatus' ] == 2 || // Предавторизованная сумма захолдирована (для двухстадийных платежей)
					$pay_status[ 'OrderStatus' ] == 5 ) { // Инициирована авторизация через ACS банка-эмитента
			echo 'Ожидание оплаты';
		}
		else if (	$pay_status[ 'OrderStatus' ] == 3 || // Авторизация отменена
					$pay_status[ 'OrderStatus' ] == 4 || // По транзакции была проведена операция возврата
					$pay_status[ 'OrderStatus' ] == 6 ) { // Авторизация отклонена
			echo 'Отмена платежа';
		}
		echo '</h1>';

		echo '<p>';
		echo $pay -> order_status[ $pay_status[ 'OrderStatus' ] ];
		echo '</p>';
	}

	echo '<p>&nbsp;</p>';