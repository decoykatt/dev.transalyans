<ul class="nav navbar-nav">
<?php

    $items = $data->getItems($menu->id);

    $uri_orig = $_SERVER['REQUEST_URI'];
    $uris = explode('?', $uri_orig);
    $uri = $uris[0];

    foreach($items as $row)
    {
        if($row['visible'] != 1) continue;

        $href = SF.$row['type_link'];

        if($uri == $href)
        {
            $select = ' class="menu-select"';
        }
        else
        {
            $select = '';
        }

        if($row['type'] == '_label')
        {
            $href = $uri_orig."#";
        }
        $sub_items = $data->getItems($menu->id,$row['id']);
        $cnt = count( $sub_items );
        if ($cnt!=0){
             echo '<li class="dropdown menuu"'.$select.'><a href="'.$href.'" '.$row['attr'].'>'.$row['title'].'</a> '; 
        }
        else{
            if ( $row['attr'] == 'class="header-tel"' ) {
                echo '<li '.$select.'> <a href="tel:'.Utils::phone_number(val ('banner.show.phone')).'" '.$row['attr'].' >'.val ('banner.show.phone').'</a>';
            } else {
           echo '<li '.$select.'><a href="' .$href. '"  '.$row['attr'].'>'.$row['title'].'</a> ';
            }
        }
        

       
        if(count($sub_items) > 0)
        {
            echo ' <ul class="dropdown-menu"> ';
            foreach($sub_items as $sub_row)
            {
                if($sub_row['visible'] != 1) continue;

                $href = SF.$sub_row['type_link'];

                if($uri == $href)
                {
                    $select = ' class="menu-select"';
                }
                else
                {
                    $select = '';
                }             

                if($sub_row['type'] == '_label')
                {
                    $href = $uri_orig."#";
                }

                echo '<li '.$select.'><a href="'.$href.'" '.$sub_row['attr'].'>'.$sub_row['title'].'</a></li> ';
            }
            echo " </ul> ";
        }

        echo '</li> ';
    }
?>
</ul>
