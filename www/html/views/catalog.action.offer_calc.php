<?php

$table = new Table( 'catalog_section' );
$rows = $table -> select( 'SELECT * FROM `catalog_section` WHERE `alias`=:alias LIMIT 1', array( 'alias' => 'ceny' ) );
$row = end( $rows );

$childs = $table -> select( 'SELECT * FROM `position_offer` WHERE `section_id`=:id', array( 'id' => $row[ 'id' ] ) );

$umnojitel = val ('banner.show.cost');
$kilmeteri = val ('pages.show.title2');

$kilmeteri = trim($kilmeteri);
$longboard = explode( '|' , $kilmeteri);
$cena = ($umnojitel*1) * ($longboard[0]*1);
$cena = number_format($cena,0,","," ");


echo '
<div class="container">
		<div class="row">
			<div class="offer_calc_wrap">
				<h2>Расчёт стоимости</h2>
				<div class="offer_dimensions">
					<p class="car_feels">Ширина груза, м <input type="text" name="car_width" value="0"></p>
					<p class="car_feels">Высота груза, м <input type="text" name="car_height" value="0"></p>
					<p class="car_feels">Длинна груза, м <input type="text" name="car_length" value="0"></p>
				</div>
				<div class="offer_mileage">
					<p class="price-item" data-price="' .$longboard[0]. '">Километраж <span id="kolmetraj">' .$cena. ' руб.</span></p>					
				</div>
				<div class="offer_consult">
					<p><input type="checkbox" checked name="free_consult" id="free_consult"><label for="free_consult">Консультация специалиста бесплатно</label></p>
				</div>
				<div class="offer_insurance">
					<p><input type="checkbox"  name="insurance" id="insurance"><label for="insurance">Страхование груза</label></p>
					<div class="offer_insurance_active">
						<div class="offer_insurance_item">
							<div class="blue_bear">100 000 руб.</div>
   							<input type="radio" id="radio_100" name="selector"><label for="radio_100"></label>
   						</div>

   						<div class="offer_insurance_item">
   							<div class="blue_bear">200 000 руб.</div>
   							<input type="radio" id="radio_200" name="selector"><label for="radio_200"></label>
   						</div>

   						<div class="offer_insurance_item">
   							<div class="blue_bear">300 000 руб.</div>
   							<input type="radio" id="radio_300" name="selector"><label for="radio_300"></label>
   						</div>

   						<div class="offer_insurance_item">
   							<div class="blue_bear">400 000 руб.</div>
   							<input type="radio" id="radio_400" name="selector"><label for="radio_400"></label>
   						</div>

   						<div class="offer_insurance_item">
   							<div class="blue_bear">500 000 руб.</div>
   							<input type="radio" id="radio_500" name="selector"><label for="radio_500"></label>
   						</div>

   						<div class="offer_insurance_item">
   							<div class="blue_bear">600 000 руб.</div>
   							<input type="radio" id="radio_600" name="selector"><label for="radio_600"></label>
   						</div>
   						<div class="offer_insurance_item">
   							<div class="blue_bear">700 000 руб.</div>
   							<input type="radio" id="radio_700" name="selector"><label for="radio_700"></label>
   						</div>
   						<div class="offer_insurance_item">
   							<div class="blue_bear">800 000 руб.</div>
   							<input type="radio" id="radio_800" name="selector"><label for="radio_800"></label>
   						</div>
   						<div class="offer_insurance_item">
   							<div class="blue_bear">900 000 руб.</div>
   							<input type="radio" id="radio_900" name="selector"><label for="radio_900"></label>
   						</div>
   						<div class="offer_insurance_item">
   							<div class="blue_bear">1 000 000 руб.</div>
   							<input type="radio" id="radio_1000" name="selector"><label for="radio_1000"></label>
   						</div>
   					</div>
   				</div>
   				<div class="offer_calculator">
					<div class="row">

';

$i = 0;


$str_l_s = '<div class="col-lg-6 offer_calculator_l">';
$str_left = ''; 
$str_l_e = '</div>';

$str_r_s = '<div class="col-lg-6 offer_calculator_r">';
$str_right = '';
$str_r_e = '</div>';


foreach ($childs as $key => $clids2) {
	$section = $table -> select( 'SELECT * FROM `position_offer` WHERE `id`=:id', array( 'id' => $clids2[ 'id' ] ) );
	$section = end( $section );
	$count_sec = count($childs);
	$cena_for_users = number_format($section['title2'],0,","," ");

	++$i;

	if ($i <= $count_sec/2 ) {
		if ( $section['public'] == 1 ) {
			if ($section['public_type']) {
				$str_left .= '<p class="price-item values_item" data-price="'.$section['title2'].'"><input type="checkbox" name="item'.$i.'" id="item'.$i.'"><label for="item'.$i.'">'.$section['title'].' <span>+'.$cena_for_users.' руб.</span></label><span class="minus" onmousedown="return false" onselectstart="return false">—</span><input type="text" value="0" name="item_qua'.$i.'"><span class="plus" onmousedown="return false" onselectstart="return false">+</span></p>';
			} else {
				$str_left .= '<p class="price-item label-price"  data-price="'.$section['title2'].'"><input type="checkbox" name="item'.$i.'" id="item'.$i.'"><label for="item'.$i.'">'.$section['title'].' <span>+'.$cena_for_users.' руб.</span></label></p>';
			}
		}
	} else {
		if ( $section['public'] == 1 ) {
			if ($section['public_type']) {
				if ($section['title'] == 'Машина сопровождения') {
					$str_right .= '<p class="price-item values_item" data-price="'.$section['title2'].'"><input type="checkbox" name="item'.$i.'" id="item'.$i.'"><label class="value_label" for="item'.$i.'">'.$section['title'].' <span>+'.$cena_for_users.' руб.</span></label><span class="minus" onmousedown="return false" onselectstart="return false">—</span><input type="text" value="0" name="item_qua'.$i.'"><span class="plus" onmousedown="return false" onselectstart="return false">+</span></p>';
				} else {
				$str_right .= '<p class="price-item values_item" data-price="'.$section['title2'].'"><input type="checkbox" name="item'.$i.'" id="item'.$i.'"><label for="item'.$i.'">'.$section['title'].' <span>+'.$cena_for_users.' руб.</span></label><span class="minus" onmousedown="return false" onselectstart="return false">—</span><input type="text" value="0" name="item_qua'.$i.'"><span class="plus" onmousedown="return false" onselectstart="return false">+</span></p>';
				}
			} else {
				$str_right .= '<p class="price-item label-price"  data-price="'.$section['title2'].'"><input type="checkbox" name="item'.$i.'" id="item'.$i.'"><label for="item'.$i.'">'.$section['title'].' <span>+'.$cena_for_users.' руб.</span></label></p>';
			}
		}
	}
}

echo $str_l_s . $str_left . $str_l_e;
echo $str_r_s . $str_right . $str_r_e;

echo '

					
					</div>
				</div>	
			</div>			
		</div>		
	</div>

';




?>