<?php

	$json = array(	's' => false,
							'err' => array( ),
							'mess' => false,
							'action' => false );

	header( 'content-type: application/json; charset=utf-8' );

	$form_act = Utils :: getVar( 'form_act' );
	$form_alias = Utils :: getVar( 'form_alias' );

	// не указаны параметры
	if ( !$form_act || !$form_alias ) {
		$json[ 'mess' ] = 'Ошибка #' . __LINE__;
		echo json_encode( $json );
		exit( );
	}



	$form = Form :: getInstance();
	$form -> setRows( $form_alias );
	if ( $form -> errorInfo ) {
		$json[ 'err' ] = $form -> errorInfo;
		$json[ 'mess' ] = 'Ошибка #' . __LINE__;
		echo json_encode( $json );
		exit( );
	}

	$table = new Table( 'catalog_section' );
	// нет формы
	$section_form = $table -> select( "SELECT `t2`.`title`,`t2`.`position`,`t2`.`alias`,`t1`.*
	FROM `section_forms` as `t1` LEFT JOIN `catalog_section` AS `t2` ON `t1`.`id`=`t2`.`id`
	WHERE `t2`.`alias`=:alias && `t2`.`section_table`='section_forms' && `t2`.`position_table`='position_forms' &&
	(`t1`.`method`='ajax-post' || `t1`.`method`='ajax-get') ORDER BY `t2`.`position`",
	array( 'alias' => $form_alias ) );

	if ( !count( $section_form ) ) {
		$json[ 'mess' ] = 'Ошибка #' . __LINE__;
		echo json_encode( $json );
		exit( );
	}

	$fields = $table -> select( "SELECT * FROM `position_forms` WHERE `section_id`=:sid AND `type_id`!='submit' ORDER BY `position`", array( 'sid' => $section_form[ 0 ][ 'id' ] ) );


	// нет полей формы
	if ( !count( $fields ) ) {
		$json[ 'mess' ] = 'Ошибка #' . __LINE__;
		echo json_encode( $json );
		exit( );
	}

	$arr_err = array( );
	$arr_req = array( );
	$pwd = null;

	foreach ( $fields as $field ) {

		$arr_req[ $field[ 'nameid' ] ] = Utils :: getVar( $field[ 'nameid' ] );
		$arr_req[ $field[ 'nameid' ] ] = trim( $arr_req[ $field[ 'nameid' ] ] );

		if ( ( !$arr_req[ $field[ 'nameid' ] ] || $arr_req[ $field[ 'nameid' ] ] == 'false' ) && $field[ 'valid_empty' ] ) {
			$arr_err[ $field[ 'nameid' ] ] = 'Не заполнено обязательное поле';
		}
		if ( $field[ 'valid_email' ] && !preg_match( "/[0-9a-zA-Z_\-]+@[0-9a-zA-Z_\.\-]+\.[a-zA-Z_]{2,3}/i", $arr_req[ $field[ 'nameid' ] ] ) ) {
			$arr_err[ $field[ 'nameid' ] ] = 'Неверный формат';
		}

		// валидация паролей при регистрации
		if ( $form_act == 'register' ) {
			if ( $field[ 'type_id' ] == 'pwd' ) {
				// пароль
				if ( !$pwd ) {
					if ( mb_strlen( $arr_req[ $field[ 'nameid' ] ] ) < 5 ) {
						$arr_err[ $field[ 'nameid' ] ] = 'Пароль не должен быть менее 5ти символов';
					}
					$pwd = $arr_req[ $field[ 'nameid' ] ];
				}
				// подтверждение пароля
				else {
					if ( $pwd != $arr_req[ $field[ 'nameid' ] ] ) {
						$arr_err[ $field[ 'nameid' ] ] = 'Пароль и подтверждение пароля не совпадают';
					}
				}
			}
		}
		// валидация паролей при авторизации
		// ...

		// валидация паролей при смене пароля
		// ...


	}

	// есть ошибки
	if ( count( $arr_err ) ) {
		$json[ 'err' ] = $arr_err;
		$json[ 'mess' ] = 'Ошибка #' . __LINE__;
		echo json_encode( $json );
		exit( );
	}

	$section_form = $form -> getSection( );

	Registry :: __instance( ) -> section_form = $section_form;

	// нет ошибок
	switch ( $form_act ) {

		// регистрация
		case 'register':

			$auth = Auth_new :: getInstance( );

			$register = $auth -> register( $arr_req );
			// ошибка регистрации
			if ( !$register ) {
				$json[ 'err' ] = $auth -> err;
				echo json_encode( $json );
				exit( );
			}
			// все ок
			else {
				$json[ 's' ] = true;
				$json[ 'mess' ] = $auth -> mess;
			}

		break;

		// авторизация
		case 'login':
			$auth = Auth_gup :: getInstance( );
			$login = $auth -> login( $arr_req );
			if ( !$login ) {
				$json[ 'err' ] = $auth -> err;
				echo json_encode( $json );
				exit( );
			}
			else {
				$json[ 's' ] = true;
				$json[ 'mess' ] = $auth -> mess;
				$json[ 'action' ][ 'location' ] = 'self';
			}


		break;

		case 'resender':
			$json[ 's' ] = true;
			$json[ 'action' ][ 'location' ] = Utils :: translit( $arr_req[ 'from' ] ) . '-' . Utils :: translit( $arr_req[ 'where' ] ) . '.html'; 
			$json[ 'mess' ] = '';
			echo json_encode( $json );
			
		break;

		// выход
		case 'logout':

			$auth = Auth_gup :: getInstance( );
			$auth -> logout( );
			$json[ 's' ] = true;
			$json[ 'action' ][ 'location' ] = 'self';

		break;

		// воостановить пароль
		case 'restore_pass':

			//$auth = Auth_gup :: login( );

		break;

		
		case 'payment':

			$stamp = md5( microtime() . var_export( $_SESSION, true ) . '1' );
			$section_id = 1234; // __YOU_SECTION_ID__
		

			$insert_row = array(	'stamp' => $stamp,
											'section_id' => $section_id,
											'num_1cid' => $number_id,
											'title' => $title,
											'price' => $price,
											'summ' => $sum,
											'client_name' => $name,
											'client_email' => $email,
											'client_phone' => $phone,
											'client_comm' => $comment );

			$table -> execute( 'INSERT INTO `position_payment` (`stamp`, `section_id`, `date_create`, `num_1cid`, `title`, `price`, `summ`, `client_name`, `client_email`, `client_phone`, `client_comm`)
			VALUES (:stamp, :section_id, now(), :num_1cid, :num_type, :title, :price, :summ, :client_name, :client_email, :client_phone, :client_comm)',
			$insert_row );

			$error_info = $table -> errorInfo;

			if ( !$error_info ) {

				$order = $table -> select( 'SELECT `id` FROM `position_orders` WHERE `stamp`=:stamp LIMIT 1', array( 'stamp' => $stamp ) );
				$order = end( $order );
				$order_id = $order[ 'id' ];

				$description =	$title;
	
				$json[ 'order_id' ] = $order_id;
				$pay = Registry :: __instance() -> pay;
				$pay_register = $pay -> register( $order_id, ( $sum * 100 ), $description );
				
				// ___THIS_DEBUG___
				$json[ 'pay_register' ] = $pay_register;

				// ЗАПРОС В ЭКВАЙРИНГ
				if ( $pay_register ) {

					if ( isset( $pay_register[ 'errorCode' ] ) && $pay_register[ 'errorCode' ] ) {
						$json[ 'mess' ] = 'Ошибка #' . __LINE__ . ', ' . $pay_register[ 'errorMessage' ] . ', попробуйте повторить запрос через 30 мин.';
						echo json_encode( $json );
						exit( );
					}
					else {
					
						// записываем в сессию данные созданного заказа
						$order = $pay -> get_order( array( 'id' => $order_id ) );
						$order = end( $order );
						$_SESSION[ 'orders' ][ $stamp ] = $order;
					
						$section_form[ 'email' ] .= ',' . $email;
						
						$arr_req[ 'YOU_PARAMS_FOR_EMAIL1' ] = '';
						$arr_req[ 'YOU_PARAMS_FOR_EMAIL2' ] = '';
						$arr_req[ 'YOU_PARAMS_FOR_EMAIL3' ] = '';
						$arr_req[ 'YOU_PARAMS_FOR_EMAIL4' ] = '';
						
						$json = send_emails( $json, '', $section_form, $arr_req, $arr_err );

					}
				}
				// Ошибка создания заказа
				else {
					$json[ 'mess' ] = 'Ошибка #' . __LINE__ . ', попробуйте повторить запрос через 30 мин.';
					echo json_encode( $json );
					exit( );
				}
			}
			
			echo json_encode( $json );

		
		break;
		
		
		
		// Отправить письмо администратору
		case 'send_email':

			$json = send_emails( $json, '', $section_form, $arr_req, $arr_err );
			echo json_encode( $json );

		break;


		// Сохранить в базе данных и отправить письмо администратору
		case 'send_email_save_bd':

				$table = new Table( $section_form -> save_table ); // YOU_TABLE
				$insert = $table -> getEntity();
				$insert -> section_id = 340; // YOU_SECTION_ID
				// ...
				$table -> save( $insert );
				$json = send_emails( $json, '', $section_form, $arr_req, $arr_err );
				echo json_encode( $json );

		break;
		
		

		
		
	}




















	function send_emails( $json, $emails='', $section_form, $arr_req, $arr_err ) {
	
			if ( $emails == '' ) $emails = $section_form[ 'email' ];
			$ex_emails = explode( ',', $emails );

			foreach ( $ex_emails as $e ) {

				$e = trim( $e );
				if ( !$e ) continue;

				$mailer = new Mailer( );
				$mailer	-> IsHTML( true );
				$mailer -> From		= 'itfactory@mail.ru';
				$mailer -> FromName	= 'IT-FACTORY SENDER';
				$mailer -> Subject	= Form :: parse_mail_tpl( $section_form[ 'esubject' ], $arr_req );
				$mailer -> Body		= Form :: parse_mail_tpl( $section_form[ 'html' ], $arr_req );

				$mailer -> AddAddress( $e );

				$send = $mailer -> Send( );

				if ( !$send ) {
					$arr_err[ 'system_message' ] = 'Ошибка #' . __LINE__;
				}

			}

				if ( count( $arr_err ) ) {
					$json[ 'err' ] = $arr_err;
					$json[ 'mess' ] = 'Ошибка #' . __LINE__;
					echo json_encode( $json );
					exit( );
				}
				else {

					$json[ 's' ] = true;
					$json[ 'mess' ] = Form :: parse_mail_tpl( $section_form[ 'success_message' ] );
				}
				
				return $json;
	}


