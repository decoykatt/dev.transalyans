<?php

/***
 * api_key = server_name . sole;
 * openssl genrsa -out private_key.pem 2028
 * openssl rsa -in private_key.pem -pubout > public_key.pub
 *
 */

Class Api_Base_Client {

	public $url = 'http://dev.project';
	public $sole = 'GQL#f3yyR&F9T&NK0N4MTMT$J(^)@)Y0#@$G%F@#%0&*)@)@&*==&*@&&#JH0JU(H#';
	public $token = null;
	public $refresh_token = null;

	private $_public_key = null;
	private $_private_key = null;
	private $_api_key = null;


	private $_auth_methods = array(
		'Basic Auth',
//		'Digest Auth',
//		'OAuth 1.0',
//		'OAuth 2.0',
//		'Hawk Authenticate',
//		'AWS Signature'
	);

	private $_auth_method = null;
	private $_auth_user = null;
	private $_auth_password	= null;

	/**
		config = array(
			'public_key_path' => '',
			'private_key_path' => '',
			'auth' => array(
				'method' => 'Basic Auth',
				'user' => '',
				'password' => '' );
		)
	*/
	public function __construct( $conf=array( ), $api_key='' ) {

		if ( !$api_key ) throw new Exception( 'api_key undefined' );
		$this -> _api_key = $api_key;
		if ( isset( $conf[ 'public_key_path' ] ) && $conf[ 'public_key_path' ] ) $this -> _public_key = openssl_get_publickey( file_get_contents( $conf[ 'public_key_path' ] ) );
		if ( isset( $conf[ 'private_key_path' ] ) && $conf[ 'private_key_path' ] ) $this -> _private_key = openssl_get_privatekey( file_get_contents( $conf[ 'private_key_path' ] ) );
		if ( isset( $conf[ 'auth' ] ) && $conf[ 'auth' ] ) {
			if ( isset( $conf[ 'auth' ][ 'method' ] ) && $conf[ 'auth' ][ 'method' ] ) $this -> _auth_method =  $conf[ 'auth' ][ 'method' ];
			if ( isset( $conf[ 'auth' ][ 'user' ] ) && $conf[ 'auth' ][ 'user' ] ) $this -> _auth_user =  $conf[ 'auth' ][ 'user' ];
			if ( isset( $conf[ 'auth' ][ 'password' ] ) && $conf[ 'auth' ][ 'password' ] ) $this -> _auth_password =  $conf[ 'auth' ][ 'password' ];
		}
		// $auth = $this -> _auth( );
		// var_dump( $auth );
	}



	// ��������������
	private function _auth( ) {
		$this -> _request( 'GET', 'init', array( ), array( ), 'auth' );
	}

	// �������� �����
	public function tokenRefresh( $refresh_token ) {
		$this -> _request( 'GET', 'refresh', array( ), array( ), 'auth' );
	}

	// �������� ������
	public function tokenCheck( $token ) {
		$this -> _request( 'GET', 'check', array( ), array( ), 'auth' );
	}


/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
�� ����� ���������� �������, ����� ���������� ������
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/
	
	
	
	

	////////////////////////////////////////////////////////////////////
	// GET 
	////////////////////////////////////////////////////////////////////

	public function getAbstract( $data=array(), $params=array() ) {
		$res = $this -> _request( 'GET', 'abstract', $data, $params );
		return $res;
	}



	////////////////////////////////////////////////////////////////////
	// POST � �������
	////////////////////////////////////////////////////////////////////

	public function getAbstract( $data=array() ) {
		$res = $this -> _request( 'POST', 'abstract', $data );
		return $res;
	}



	////////////////////////////////////////////////////////////////////
	// PUT � ��������
	////////////////////////////////////////////////////////////////////

	public function editAbstract( $data=array(), $params=array() ) {
		$res = $this -> _request( 'PUT', 'abstract', $data, $params );
		return $res;
	}



	////////////////////////////////////////////////////////////////////
	// DELETE � �������
	////////////////////////////////////////////////////////////////////
	
	public function deleteAbstract( $data=array() ) {
		$res = $this -> _request( 'DELETE', 'abstract', $data );
		return $res;
	}

	
	
	
	// request
	public function _request( $rest_method, $method, $data, $params=array(), $api='api' ) {

		$ch = curl_init( );
		$api_request_parameters = $params;
		$api_request_url = $this -> url . '/' . $api . '/' . $method . '/';
		$api_request_url .= '?' . http_build_query( $api_request_parameters );

/*
		$api_request_parameters = array(
		  'api_key' => 'gF7mc05157W726sL',
		  'token' => 'hnFGJSpNzMzft8GE8AVTdvyZaydNYorgthkO46Rt',
		  'user_id' => 5428
		);
*/

		$ch = curl_init( );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

		if ( $rest_method == 'DELETE' )
		{
		  curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'DELETE' );
		}

		if ( $rest_method == 'GET' )
		{
		  curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );
		}

		if ( $rest_method == 'POST' )
		{
		  curl_setopt( $ch, CURLOPT_POST, TRUE );
		}

		if ( $rest_method == 'PUT' )
		{
		  curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'PUT' );
		}

		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $data ) );
        curl_setopt( $ch, CURLOPT_URL, $api_request_url );
        curl_setopt( $ch, CURLOPT_HTTPHEADER,
          array(
			'Content-Type: application/json'
		  )
        );
		curl_setopt( $ch, CURLOPT_ENCODING, 'gzip' ); // 6947 --decode-gzip-result--> 9319 =)
		curl_setopt( $ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13' );
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );

		if ( $this -> _auth_method == 'Basic Auth' ) {
			curl_setopt( $ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC );
			curl_setopt( $ch, CURLOPT_USERPWD, $this -> _auth_user . ':' . $this -> _auth_password );
		}

		$response = curl_exec( $ch );
		$http = curl_getinfo( $ch );
		curl_close( $ch );
		/*
		var_dump( $http );
		var_dump( $response );
		var_dump( $data );

		if ( $rest_method == 'PUT' ) {
		}
		*/
		$response = json_decode( $response, true );
		if ( $this -> _private_key ) $response = $this -> _decrypt( $response );
		return $response;

	}

	
	private function _decrypt( $data ) {

		// $public_key_details = openssl_pkey_get_details( $this -> _public_key );
		// there are 11 bytes overhead for PKCS1 padding
		// $encrypt_chunk_size = ceil( $public_key_details[ 'bits' ] / 8 ) - 11;
		// var_dump( $encrypt_chunk_size );

		$private_key_details = openssl_pkey_get_details( $this -> _private_key );
		$decrypt_chunk_size = ceil( $private_key_details[ 'bits' ] / 8 );

		$maxlength = $decrypt_chunk_size;
		if ( $this -> _private_key ) {
			if ( isset( $data[ 'data' ] ) && is_array( $data[ 'data' ] ) ) {
				foreach( $data[ 'data' ] as $key => $row ) {
					if ( !is_array( $data[ 'data' ][ $key ] ) ) continue;
					foreach( $data[ 'data' ][ $key ] as $col => $val ) {
						$val = base64_decode( $val );
						$decode = '';
						$decode = $this -> ssl_decrypt( $val, 'private', $this -> _private_key );
						$data[ 'data' ][ $key ][ $col ] = $decode;
					}
				}
			}
		}
		return $data;
	}




	public function ssl_encrypt( $source, $type, $key ) {
		$private_key_details = openssl_pkey_get_details( $key );
		$decrypt_chunk_size = ceil( $private_key_details[ 'bits' ] / 8 ) - 11;
		$maxlength = $decrypt_chunk_size;
		$output = '';
		while( $source ) {
		  $input = substr( $source, 0, $maxlength );
		  $source = substr( $source, $maxlength );
		  if( $type == 'private' ) {
			$ok = openssl_private_encrypt( $input, $encrypted, $key );
		  } else {
			$ok = openssl_public_encrypt( $input, $encrypted, $key );
		  }
		  $output .= $encrypted;
		}
		return $output;
	}


	function ssl_decrypt( $source, $type, $key ) {
		$private_key_details = openssl_pkey_get_details( $key );
		$decrypt_chunk_size = ceil( $private_key_details[ 'bits' ] / 8 );
		$maxlength = $decrypt_chunk_size;
		$output = '';
		while( $source ) {
		  $input = substr( $source, 0, $maxlength );
		  $source = substr( $source, $maxlength );
		  if( $type == 'private' ) {
			$ok = openssl_private_decrypt( $input, $out, $key );
		  } else {
			$ok = openssl_public_decrypt( $input, $out, $key );
		  }
		  $output.=$out;
		}
		return $output;
	}





}	





