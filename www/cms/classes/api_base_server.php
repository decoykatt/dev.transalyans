<?php

class Api_Base_Server {

	private $_debug = false;

	private $_key;
	private $_permissions;

	private $_route = array( );

	private $_db;
	private $_table = 'catalog_section';

	private $_params;

	public $_type;
	public $_section;
	public $_data_tables = array( );
	
	
	private $_header = 200;
	
/*
200 OK
400 Bad Request (������������ ������)
500 Internal Server Error (���������� ������ �������)

backend_error (503)	������ �������.
invalid_parameter (400)	������� ����� ��������.
not_found (404)	��������� ������ �� ������.
missing_parameter (400)	�� ������ ����������� ��������.
access_denied (403)	������ ��������.
unauthorized (401)	���������������� ������������.
quota (429)	�������� ����� ���������� �������� � API.
query_error (400)	������ ������� �������.
conflict (409)	��������� ����������� ������
invalid_json (400)	���������� JSON ����� �������� ������.
*/

	private $_headers = array(
		200 => 'HTTP/1.1 OK',
		400 => 'HTTP/1.1 400 Bad Request',
		401 => 'HTTP/1.1 401 Unauthorized',
		403 => 'HTTP/1.0 403 Forbidden',
		409 => 'HTTP/1.1 409 Conflict',
		429 => 'HTTP/1.1 429 Too Many Requests',
		500 => 'HTTP/1.1 500 Internal Server Error',
		503 => 'HTTP/1.1 503 Service Unavailable'
	);


	private $_auth_methods = array(
		'Basic Auth',
//		'Digest Auth',
//		'OAuth 1.0',
//		'OAuth 2.0',
//		'Hawk Authenticate',
//		'AWS Signature'
	);
	
	
	private $_access_methods = array(
		'GET',
		'POST',
		'PUT',
		'DELETE'
		);

	private $_access_format = array(
		'JSON',
//		'XML',
//		'CSV'
	);

	private $_method = 'GET';
	private $_format = 'JSON';
	private $_body = false;

	/**
		'params'
		'format'
		'route'
		'debug'
	*/
	
	
	private $_encrypt_key = '';
	private $_encrypt_type = 'public';
	
	
	
	
	
	
	public function __construct( $conf=array( ) ) {

		if ( !isset( $conf[ 'params' ] ) ) throw new Exception( 'invalid request ' . __LINE__ );
		if ( !isset( $conf[ 'method' ] ) ) throw new Exception( 'invalid request ' . __LINE__ );
		if ( array_search( $conf[ 'method' ], $this -> _access_methods ) === false ) throw new Exception( 'invalid request ' . __LINE__ );
		$this -> _method = $conf[ 'method' ];
		if ( isset( $conf[ 'route' ] ) && !is_array( $conf[ 'route' ] ) ) throw new Exception( 'invalid request ' . __LINE__ );

		$this -> _route = ( isset( $conf[ 'route' ] ) ) ? $conf[ 'route' ] : array( );
		$this -> _format = ( isset( $conf[ 'format' ] ) && array_search( $conf[ 'format' ], $this -> _access_format ) !== false ) ? $conf[ 'format' ] : 'JSON';
		$this -> _debug = ( isset( $conf[ 'debug' ] ) && $conf[ 'debug' ] ) ? true : false;
		//$this -> _body = $this -> { '_decode' . $this -> _format }( file_get_contents( 'php://input' ) );

		$path_rsa_public = realpath( dirname( __FILE__ ) . '/../..' ) . DS . 'cms' . DS . 'rsa' . DS . 'pck';
		$pk = file_get_contents( $path_rsa_public );
		$this -> _encrypt_key = openssl_get_publickey( $pk );

		$this -> _db = new Table( 'catalog_section' );
		
		//////////////////////////////////////////////////////////////
		// ����� ������� ��������� ������ 
		if ( $conf[ 'params' ][ 0 ] == 'get_chain' ) {
			//ob_start( 'ob_gzhandler' );
			$this -> _headerJSON( );
			$r[ 's' ] = true;
			$r[ 'code' ] = 200;
			$r[ 'message' ] = '������� ������';
			$r[ 'data' ] = $this -> get_chain( );
			$r[ 'route' ] = $this -> _route;

			//////////////////////////////////////////////////////////////////////////
			// ���������� ���������� ����� �������
			$encrypt_filter = $this -> make_encrypt_filter( );
			$r[ 'filter' ] = $encrypt_filter;
			$val = serialize( $r[ 'data' ] );
			$val = $this -> _encrypt( $val );
			$val = base64_encode( $val );
			$r[ 'data' ] = $val;
			// ������� �������� ������ ��������������� ����
			$val = serialize( $r[ 'route' ] );
			$val = $this -> _encrypt( $val );
			$val = base64_encode( $val );
			$r[ 'route' ] = $val;
			echo json_encode( $r );
			//ob_end_flush( );
			exit( 1 );
		}
		//////////////////////////////////////////////////////////

		$p = $this -> _router_map_params( $conf[ 'params' ] );
		$this -> _params = $p[ 'params' ];
		$this -> _type = $p[ 'type' ];
		//echo '</pre>';

		unset( $p );

		$this -> _section = $this -> _router_check_params( );

	}

	
	// �������� �������� ������������� �����
	public function make_encrypt_filter( ) {
		$crypt_fields = array( );
		if ( count( $this -> _data_tables ) ) {
			$c = Config :: getConfig( 'catalog' );
			foreach( $this -> _data_tables as $table => $b ) {
			if ( !isset( $c -> { $table } ) ) continue;
			$cols = $c -> { $table };
				foreach ( $cols as $col => $v ) {
					if ( $col == 'id' ) continue;
					if ( !isset( $c -> { $table }[ $col ] ) ) continue;
					if ( $c -> { $table }[ $col ][ 'type' ] == 'encode' ) {
						$crypt_fields[ $col ] = $table;
					}
				}
			}
		}
		return $crypt_fields;
	}



	// ������� ������
	private function _encrypt( $source ) {
		$key_details = openssl_pkey_get_details( $this -> _encrypt_key );
		$decrypt_chunk_size = ceil( $key_details[ 'bits' ] / 8 );
		$maxlength = $decrypt_chunk_size;
		$output = '';
		while ( $source ) {
		  $input = substr( $source, 0, $maxlength );
		  $source = substr( $source, $maxlength );
		  if ( $this -> _encrypt_type == 'private' ) {
			$ok = openssl_private_encrypt( $input, $encrypted, $this -> _encrypt_key );
		  } else {
			$ok = openssl_public_encrypt( $input, $encrypted, $this -> _encrypt_key );
		  }

		  $output .= $encrypted;
		}
		return $output;
	}
	
	
	
	
	
	
	
	
	
	
	//////////////////////////////////////////////////////////////////
	// INDEXED
	//////////////////////////////////////////////////////////////////

	/**
		�������� ������ ������
	*/
	public function get_chain( ) {
		$id_parent = array( );
		$parent_id = array( );
		$items = array( );
		$array = array( );
		$return = array( );
		$chains = $this -> _chain_flat_array( );
		foreach( $chains as $key => $chain ) {
			$a = $this -> _chain_build_tree( $chain ); // ������� ������ � ������
			foreach ( $chain as $c ) {
				$items[ $c[ 'alias' ] ] = $c;
			}
			$a = end( $a );  // ������� ����������� ������
			$this -> _chain_tree_to_path( $a, 'alias', 'children', '/', $array ); // ����� � array �������� ���������
		}
		unset( $a, $key, $chain, $last );
		//var_dump( '11111111111111', $items, '11111111111111' );
		//var_dump( '22222222222222', $array, '22222222222222' );
		foreach ( $array as $path ) {
			$ex = explode( '/', $path );
			$last = array_pop( $ex );
			if ( $last == '_items' ) {
				$last2 = array_pop( $ex );
				$last = $last2 . '/' . $last;
			}
			$items[ $last ][ 'alias' ] = str_replace( '/_items', '', $items[ $last ][ 'alias' ] );
			$return[ $path . '/' ] = $items[ $last ];
		}
		
		return $return;
	}

	
	/**
		��������� ���� ����� ��� �� ������
	*/
	public function _chain_tree_to_path( $a, $keyname, $parent, $glue, & $rtn, $pre="" ) {
	  $_pre = "";
	  if ( $a[ $keyname ] ) {
		$rtn[ ] = $_pre = $pre . $glue . $a[ $keyname ];
	  }
	  if ( $a[ $parent ] ) {
		if( is_array($a[ $parent ] ) ) {
		  foreach( $a[ $parent ] as $c )
			$this -> _chain_tree_to_path($c, $keyname, $parent, $glue, $rtn, $_pre );
		} else {
		  $this -> _chain_tree_to_path( $a[ $parent ], $keyname, $parent, $glue, $rtn, $_pre );
		}
	  }
	  $qtd = count( $rtn );
	  return ( isset( $rtn[ -1 ] ) ) ? $rtn[ -1 ] : null;
	}



	/**
		��������������� ������� ������ ������ � ������ ������
	*/
	protected function _chain_build_tree( $tree, $root = null ) {
		$return = array( );
		foreach( $tree as $child => $parent ) {
			if ( isset( $parent[ 'parent_id' ] ) && $parent[ 'parent_id' ] == $root ) {
				unset( $tree[ $child ] );
				$parent[ 'children' ] = $this -> _chain_build_tree( $tree, $child );
				$return[ $parent[ 'id' ] ] = $parent;
			}
		}
		return empty( $return ) ? null : $return;    
	}


	/**
		��������� ������� ������ ������
	*/
	protected function _chain_flat_array( $parents=array( ), $index=0, $main='', $ret=array( ) ) {
		$str = '';
		$parent_cnt = count( $parents );
		if ( !$parent_cnt ) {
			$rows = $this -> _db -> select( 'SELECT * FROM `catalog_section` WHERE isNull(`parent_id`) ORDER BY `position`' );
			++$index;
			$ret = $this -> _chain_flat_array( $rows, $index, $main, $ret );
			--$index;
		}
		else {

			foreach ( $parents as $parent ) {

				if ( $index == 1 ) {
					$main = $parent[ 'alias' ];
					$parent_id = 0;
				}
				else {
					$parent_id = $parent[ 'parent_id' ];
				}

				if ( !isset( $ret[ $main ][ $parent[ 'id' ] ] ) ) $ret[ $main ][ $parent[ 'id' ] ] = array( );

				$ret[ $main ][ $parent[ 'id' ] ][ 'parent_id' ] = $parent_id;				
				$ret[ $main ][ $parent[ 'id' ] ][ 'id' ] = $parent[ 'id' ];
				$ret[ $main ][ $parent[ 'id' ] ][ 'alias' ] = $parent[ 'alias' ];

				$tpl = $parent[ 'children_tpl' ];
				$tpl = str_replace( array( "\n","\r","\t"," " ), '', $tpl );
				$ret[ $main ][ $parent[ 'id' ] ][ 'tpl' ] = $tpl;

				//$ret[ $main ][ $parent[ 'id' ] ][ 'level_tree' ] = $index;
				
				if ( $parent[ 'position_table' ] ) {
					$ret[ $main ][ $parent[ 'id' ] ][ 'alias' ] = $ret[ $main ][ $parent[ 'id' ] ][ 'alias' ] . '/_items';
					$ret[ $main ][ $parent[ 'id' ] ][ 'position' ] = $parent[ 'position_table' ];
					$position_cols = $this -> _db -> select( 'SHOW COLUMNS FROM `' . $parent[ 'position_table' ] . '`' );
					if ( count( $position_cols ) ) {
						foreach ( $position_cols as $p_key => $p_col ) {
							$ret[ $main ][ $parent[ 'id' ] ][ 'position_c' ][ $p_key ] = $p_col[ 'Field' ];
							$ret[ $main ][ $parent[ 'id' ] ][ 'position_ctype' ][ $p_key ] = $p_col[ 'Type' ];
							$ret[ $main ][ $parent[ 'id' ] ][ 'position_cnull' ][ $p_key ] = ( ( $p_col[ 'Null' ] == 'YES' ) ? true : false );
							$ret[ $main ][ $parent[ 'id' ] ][ 'position_cdeft' ][ $p_key ] = $p_col[ 'Default' ];
						}
					}
				}

				if ( $parent[ 'section_table' ] ) {
					$ret[ $main ][ $parent[ 'id' ] ][ 'section' ] = $parent[ 'section_table' ];
					$section_cols = $this -> _db -> select( 'SHOW COLUMNS FROM `' . $parent[ 'section_table' ] . '`' );
					if ( count( $section_cols ) ) {
						foreach ( $section_cols as $s_key => $s_col ) {
							$ret[ $main ][ $parent[ 'id' ] ][ 'section_c' ][ $s_key ] = $s_col[ 'Field' ];
							$ret[ $main ][ $parent[ 'id' ] ][ 'section_ctype' ][ $s_key ] = $s_col[ 'Type' ];
							$ret[ $main ][ $parent[ 'id' ] ][ 'section_cnull' ][ $s_key ] = ( ( $s_col[ 'Null' ] == 'YES' ) ? true : false );
							$ret[ $main ][ $parent[ 'id' ] ][ 'section_cdeft' ][ $s_key ] = $s_col[ 'Default' ];
						}
					}
				}

				$rows = $this -> _db -> select( 'SELECT * FROM `catalog_section` WHERE `parent_id`=:parent_id ORDER BY `position`', array( 'parent_id' => $parent[ 'id' ] ) );
				$cnt = count( $rows );
				if ( $cnt ) {
					++$index;
					$ret = $this -> _chain_flat_array( $rows, $index, $main, $ret );
					--$index;
				}
			}
		}
		return $ret;
	}
	//////////////////////////////////////////////////////////////////



	
	


	//////////////////////////////////////////////////////////////////
	// SECTIONS
	//////////////////////////////////////////////////////////////////

	/**
		�������� ������ ������
	*/
	public function get_sections( $data = array( ), $fields=array( ) ) {

		if ( $this -> _method != 'GET' ) throw new Exception( 'invalid request ' . __LINE__ );
		if ( $this -> _type != 'section' ) throw new Exception( 'invalid request ' . __LINE__ );

		$r = array( );
		$r[ 'data' ] = array( );

		if ( isset( $fields[ 'uri' ] ) ) unset( $fields[ 'uri' ] );
		if ( isset( $fields[ 'mod' ] ) ) unset( $fields[ 'mod' ] );
		if ( isset( $fields[ 'alias' ] ) ) unset( $fields[ 'alias' ] );

		$data = $this -> { '_decode' . $this -> _format }( $data );

		$filter = ( isset( $data[ 'filter' ] ) && is_array( $data[ 'filter' ] ) ) ? $data[ 'filter' ] : array( );
		$fields = ( isset( $fields[ 'fields' ] ) && $fields[ 'fields' ] ) ? $fields[ 'fields' ] : '';

		$filter = $this -> _filter( $filter );
		$sql_filter = $filter[ 'sql' ];
		$params = $filter[ 'params' ];
		unset( $filter );
		$sql_fields = $this -> _fields( $fields );

		$child_sections = $this -> _db -> select( 'SELECT ' . $sql_fields . ' FROM `catalog_section` WHERE `parent_id`=:pid ORDER BY `position`',
			array( 'pid' => $this -> _section[ 'id' ] ) );

		if ( count( $child_sections ) ) {
			foreach ( $child_sections as $child ) {

				if ( !isset( $this -> _data_tables[ $child[ $this -> _type . '_table' ] ] ) && $child[ $this -> _type . '_table' ] != '' ) {
					$this -> _data_tables[ $child[ $this -> _type . '_table' ] ] = true;
				}
				$property = $this -> _db -> select(
					'SELECT ' . $sql_fields . 
					' FROM `' . $child[ 'section_table' ] . 
					'` WHERE `id`=:id' . $sql_filter . ' LIMIT 1',
					array_merge( $params, array( 'id' => $child[ 'id' ] ) )
				);
				if ( !count( $property ) ) continue;
				$property = array_diff( end( $property ), array( '' ) );
				if ( !count( $property ) ) continue;
				$r[ 'data' ][ ] = $property;
			}
			$r[ 's' ] = true;
			//$r[ 'filter' ] = $sql_filter;
			$r[ 'code' ] = 200;
		}
		return $r;
	}



	/**
		��������� ������
	*/
	public function post_sections( $data = array( ) ) {

		if ( $this -> _method != 'POST' ) throw new Exception( 'invalid request ' . __LINE__ );
		if ( $this -> _type != 'section' ) throw new Exception( 'invalid request ' . __LINE__ );

		$data = $this -> { '_decode' . $this -> _format }( $data );
		$items = ( isset( $data[ 'items' ] ) && is_array( $data[ 'items' ] ) ) ? $data[ 'items' ] : array( );
		if ( !count( $items ) ) throw new Exception( 'invalid request ' . __LINE__ );

		$sql = "";

		if ( !isset( $items[ 0 ][ 'section' ] ) || !$items[ 0 ][ 'section' ] ) {
			$result_sql = 'SELECT `cs`.`parent_id`, `cs`.`title`, `cs`.`alias`, `cs`.`position_table` AS `position`, `cs`.`section_table` as `section` from `catalog_section` AS `cs` WHERE `id` IN (';
		}
		else {
			$result_sql = 'SELECT `cs`.`parent_id`, `cs`.`title`, `cs`.`alias`, `cs`.`position_table` AS `position`, `cs`.`section_table` as `section`, `' . $items[ 0 ][ 'section' ] . '`.* from `' . $items[ 0 ][ 'section' ] . '` LEFT JOIN `catalog_section` AS `cs` ON `cs`.`id`=`' . $items[ 0 ][ 'section' ] . '`.`id` WHERE `cs`.`id` IN (';
		}

		$result_arr = array( );
		foreach( $items as $key => $item ) {
			$new_alias = ( ( isset( $item[ 'alias_prefix' ] ) ) ? $item[ 'alias_prefix' ] : '' ) . Utils :: translit( $item[ 'title' ] );
			$sql .= "
INSERT INTO `catalog_section` (`id`,`parent_id`,`title`,`alias`,`position_table`,`section_table`,`children_tpl`,`leaf`) VALUES (NULL," . $this -> _section[ 'id' ] . ",'" . $item[ 'title' ] . "','" . $new_alias . "','" . ( ( isset( $item[ 'position' ] ) ) ? $item[ 'position' ] : '' ) . "','" . ( ( isset( $item[ 'section' ] ) && $item[ 'section' ] ) ? $item[ 'section' ] : '' ) . "','" . ( ( isset( $item[ 'tpl' ] ) ) ? $item[ 'tpl' ] : '' ) . "',NULL);
SET @last_id" . $key . " := LAST_INSERT_ID();";
			$result_arr[ $new_alias ] = '@last_id' . $key;
			if ( isset( $item[ 'section' ] ) && $item[ 'section' ] ) {
				$col_sql = '';
				$val_sql = '';
				foreach ( $item as $col => $val ) {
					// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					// REQURSIVE ADDED STRUCTURE
					////////////////////////////////////////////
					if ( $col == 'children_tpl' ) {
						;
					}
					if ( $col == 'section' || $col == 'position' ) continue;
					if ( $col == 'alias_prefix' ) continue;
					if ( $col == 'title' ) continue;
					$col_sql .= '`' . $col . '`,';
					$val_sql .= '"' . $val . '",';
				}
				$col_sql = substr( $col_sql, 0, -1 );
				$val_sql = substr( $val_sql, 0, -1 );

				$sql .= "
INSERT INTO `" . $item[ 'section' ] . "` (`id`," . $col_sql . ") VALUES (@last_id" . $key . "," . $val_sql . ");";
			}
		}

		$sql .= "";

		$this -> _db -> execute( $sql );
		if ( $this -> _db -> errorInfo ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			//$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = '������ ����������';
			return $r;
		}

		$result_sql .= implode( ',', $result_arr ) . ') ORDER BY `cs`.`id`;';
		$r[ 'data' ] = $this -> _db -> select( $result_sql );
		if ( $this -> _db -> errorInfo || !count( $r[ 'data' ] ) ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_result_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			//$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = '������ ���������� ����������';
			$r[ 'sql' ] = $result_sql;
			return $r;
		}
		else {
			$r[ 's' ] = true;
			$r[ 'code' ] = 200;
			//$r[ 'sql' ] = $sql;
			return $r;
		}

	}



	/**
		����������� ������
	*/
	public function put_sections( $data = array( ) ) {

		if ( $this -> _method != 'PUT' ) throw new Exception( 'invalid request ' . __LINE__ );
		if ( $this -> _type != 'section' ) throw new Exception( 'invalid request ' . __LINE__ );

		$r = array( );
		$data = $this -> { '_decode' . $this -> _format }( $data );
		$items = ( isset( $data[ 'items' ] ) && is_array( $data[ 'items' ] ) ) ? $data[ 'items' ] : array( );
		if ( !count( $items ) ) throw new Exception( 'invalid request ' . __LINE__ );

		$r[ 'errors' ] = array( );

		$sql = '';
/*
		$sql = "
START TRANSACTION;";
*/
		foreach( $items as $key => $item ) {
			if ( !isset( $item[ 'id' ] ) || !preg_match( "/^[0-9]{1,}$/i", $item[ 'id' ] ) ) {
				$r[ 'errors' ][ $key ] = 'param item[ ' . $key . ' ][ id ] must be int';
				continue;
			}
			$find = $this -> _db -> select( 'SELECT `section_table`,`position_table` FROM `catalog_section` WHERE `id`=:id LIMIT 1', array( 'id' => $item[ 'id' ] ) );
			if ( !count( $find ) ) {
				$r[ 'errors' ][ $key ] = 'item[ ' . $key . ' ][ id ] not exist';
				continue;
			}
			$find = end( $find );
			$set_sql = '';
			$i = 0;
			foreach ( $item as $col => $val ) {
				if ( $col == 'alias_prefix' ) continue;
				if ( $col == 'id' ) {
					$id = $val;
					continue;
				}
				if ( $col == 'title' || $col == 'children_tpl' ) {
					continue;
				}
				if ( $i ) $set_sql .= ' &&';
				$set_sql .= ' `' . $col . '`="' . $val . '",';
			}
			$set_sql = substr( $set_sql, 0, -1 );

			$set_cs_sql = '';
			$set_cs_sql .= ( isset( $item[ 'title' ] ) && $item[ 'title' ] ) ? ' `title`="' . $item[ 'title' ] . '",' : '';
			$set_cs_sql .= ( isset( $item[ 'children_tpl' ] ) && $item[ 'children_tpl' ] ) ? ' `children_tpl`="' . $item[ 'children_tpl' ] . '",' : '';
			if ( strlen( $set_cs_sql ) > 0 ) {
				$set_cs_sql = substr( $set_cs_sql, 0, -1 );
				$sql .= "
UPDATE `catalog_section` SET " . $set_cs_sql . " WHERE `id`='" . $item[ 'id' ] . "';";
			}

			$sql .= "
UPDATE `" . $find[ 'section_table' ] . "` SET" . $set_sql . " WHERE `id`='" . $item[ 'id' ] . "';";
			++$i;
		}
/*
		$sql .= "
COMMIT;";
*/
		if ( count( $r[ 'errors' ] ) ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = '������ ��������������';
			return $r;
		}

		$this -> _db -> execute( $sql );
		if ( $this -> _db -> errorInfo ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = '������ ��������������';
			return $r;
		}
		else {
			$r[ 's' ] = true;
			$r[ 'code' ] = 200;
			return $r;
		}

	}



	/**
		������� ������

	public function delete_sections( $data = array( ) ) {
		if ( $this -> _method != 'DELETE' ) throw new Exception( 'invalid request ' . __LINE__ );
		if ( $this -> _type != 'section' ) throw new Exception( 'invalid request ' . __LINE__ );
		$r = array( );
		
		
		
		return $r;
	}
	*/
	
	
	//////////////////////////////////////////////////////////////////
	// POSITIONS
	//////////////////////////////////////////////////////////////////

	
	/**
		�������� ������ �������
	*/
	public function get_positions( $data = array( ), $fields = array( ) ) {
		if ( $this -> _method != 'GET' ) throw new Exception( 'invalid request ' . __LINE__ );
		if ( $this -> _type != 'position' ) throw new Exception( 'invalid request ' . __LINE__ );

		$r = array( );
		$r[ 'data' ] = array( );

		if ( isset( $fields[ 'uri' ] ) ) unset( $fields[ 'uri' ] );
		if ( isset( $fields[ 'mod' ] ) ) unset( $fields[ 'mod' ] );
		if ( isset( $fields[ 'alias' ] ) ) unset( $fields[ 'alias' ] );
		
		$data = $this -> { '_decode' . $this -> _format }( $data );
		$fields = ( isset( $fields[ 'fields' ] ) && $fields[ 'fields' ] ) ? $fields[ 'fields' ] : '';
		$filter = ( isset( $data[ 'filter' ] ) && is_array( $data[ 'filter' ] ) ) ? $data[ 'filter' ] : array( );
		$filter = $this -> _filter( $filter );
		$sql_filter = $filter[ 'sql' ];
		$params = $filter[ 'params' ];
		unset( $filter );
		$sql_fields = $this -> _fields( $fields );

		if ( !isset( $this -> _data_tables[ $this -> _section[ 'position_table' ] ] ) )
			$this -> _data_tables[ $this -> _section[ 'position_table' ] ] = true;

		$child_positions = $this -> _db -> select(
			'SELECT ' . $sql_fields . 
			' FROM `' . $this -> _section[ 'position_table' ] . 
			'` WHERE `section_id`=:id' . $sql_filter . ' ORDER BY `id`',
			array_merge( $params, array( 'id' => $this -> _section[ 'id' ] ) )
		);

		if ( $this -> _db -> errorInfo ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			//$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = '������ ����������';
			return $r;
		}
		else {
			$r[ 's' ] = true;
			$r[ 'code' ] = 200;
			$r[ 'data' ] = $child_positions;
		}
	
		return $r;
	}


	
	/**
		��������� ������ �������
	*/
	public function post_positions( $data = array( ) ) {
		if ( $this -> _method != 'POST' ) throw new Exception( 'invalid request ' . __LINE__ );
		if ( $this -> _type != 'position' ) throw new Exception( 'invalid request ' . __LINE__ );

		$r = array( );
		$data = $this -> { '_decode' . $this -> _format }( $data );
		$items = ( isset( $data[ 'items' ] ) && is_array( $data[ 'items' ] ) ) ? $data[ 'items' ] : array( );
		if ( !count( $items ) ) throw new Exception( 'invalid request ' . __LINE__ );
		
		
		$sql = '';
/*
		$sql = "
START TRANSACTION;";
*/
		$result_sql = 'SELECT * from `' . $this -> _section[ 'position_table' ] . '` WHERE `id` IN (';
		$result_arr = array( );
		foreach( $items as $key => $item ) {
			$col_sql = '';
			$val_sql = '';
			foreach ( $item as $col => $val ) {
				if ( $col == 'title' ) continue;
				$col_sql .= '`' . $col . '`,';
				$val_sql .= '"' . $val . '",';
			}
			$col_sql = substr( $col_sql, 0, -1 );
			$val_sql = substr( $val_sql, 0, -1 );

			$sql .= "
INSERT INTO `" . $this -> _section[ 'position_table' ] . "` (`id`,`section_id`," . $col_sql . ") VALUES (NULL,'" . $this -> _section[ 'id' ] . "'," . $val_sql . ");
SET @last_id" . $key . " := LAST_INSERT_ID();";
			$result_arr[ ] = '@last_id' . $key;
		}
/*
		$sql .= "
COMMIT;";
*/
		$this -> _db -> execute( $sql );
		if ( $this -> _db -> errorInfo ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = '������ ����������';
			return $r;
		}

		$result_sql .= implode( ',', $result_arr ) . ') ORDER BY `id`;';
		$result = $this -> _db -> select( $result_sql );
		$r[ 'data' ] = $result;
		if ( $this -> _db -> errorInfo || !count( $r[ 'data' ] ) ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_result_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = '������ ���������� ����������';
			return $r;
		}
		else {
			$r[ 's' ] = true;
			$r[ 'code' ] = 200;
			return $r;
		}
	}


	/**
		�������� �������
	*/
	public function put_positions( $data = array( ) ) {
		if ( $this -> _method != 'PUT' ) throw new Exception( 'invalid request ' . __LINE__ );
		if ( $this -> _type != 'position' ) throw new Exception( 'invalid request ' . __LINE__ );

		$r = array( );
		$data = $this -> { '_decode' . $this -> _format }( $data );

		$items = ( isset( $data[ 'items' ] ) && is_array( $data[ 'items' ] ) ) ? $data[ 'items' ] : array( );
		if ( !count( $items ) ) throw new Exception( 'invalid request ' . __LINE__ );
		
		$r[ 'errors' ] = array( );
		$sql = "";

		foreach( $items as $key => $item ) {
			if ( !isset( $item[ 'id' ] ) || !preg_match( "/^[0-9]{1,}$/i", $item[ 'id' ] ) ) {
				$r[ 'errors' ][ $key ] = 'param item[ id ] must be int';
				continue;
			}
			$col_sql = '';
			$i = 0;
			foreach ( $item as $col => $val ) {
				if ( $col == 'alias_prefix' ) continue;
				if ( $col == 'id' ) {
					$id = $val;
					continue;
				}
				if ( $col == 'title' ) {
					$items[ $key ][ 'alias' ] = ( ( isset( $items[ $key ][ 'alias_prefix' ] ) ) ? $items[ $key ][ 'alias_prefix' ] : '' ) .
						( ( isset( $items[ $key ][ 'title' ] ) && $items[ $key ][ 'title' ] ) ? Utils :: translit( $items[ $key ][ 'title' ] ) : '' );
				}
				if ( $i ) $col_sql .= ' &&';
				$col_sql .= ' `' . $col . '`="' . $val . '",';
			}
			$col_sql = substr( $col_sql, 0, -1 );

			$sql .= 'UPDATE `' . $this -> _section[ 'position_table' ] . '` SET' . $col_sql . ' WHERE `id`=' . $item[ 'id' ] . ';';
			++$i;
		}

	
		$sql .= "";

		if ( count( $r[ 'errors' ] ) ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = '������ ��������������';
			return $r;
		}

		$this -> _db -> execute( $sql );
		if ( $this -> _db -> errorInfo ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = '������ ��������������';
			return $r;
		}
		else {
			$r[ 'sql' ] = $sql;
			$r[ 's' ] = true;
			$r[ 'code' ] = 200;
			return $r;
		}
	}



	/**
		������� �������

	public function delete_positions( $data = array( ) ) {
		if ( $this -> _method != 'DELETE' ) throw new Exception( 'invalid request ' . __LINE__ );
		if ( $this -> _type != 'position' ) throw new Exception( 'invalid request ' . __LINE__ );

		$r = array( );
		$data = $this -> { '_decode' . $this -> _format }( $data );
		$items = ( isset( $data[ 'items' ] ) && is_array( $data[ 'items' ] ) ) ? $data[ 'items' ] : array( );
		if ( !count( $items ) ) throw new Exception( 'invalid request ' . __LINE__ );

		$r[ 'errors' ] = array( );
		$sql = '';

		$sql = '';

		$ids = array( );
		foreach( $items as $key => $item ) {
			if ( !isset( $item[ 'id' ] ) || !preg_match( "/^[0-9]{1,}$/i", $item[ 'id' ] ) ) {
				$r[ 'errors' ][ $key ] = 'param item[ id ] must be int';
				continue;
			}
			$ids[ ] = $item[ 'id' ];
		}
		$sql .= "
DELETE FROM `" . $this -> _section[ 'position_table' ] . "` WHERE `id` IN (" . implode( ',', $ids ) . ");";

		if ( count( $r[ 'errors' ] ) ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = '������ ��������';
			return $r;
		}

		$this -> _db -> execute( $sql );
		if ( $this -> _db -> errorInfo ) {
			$r[ 's' ] = false;
			$r[ 'code' ] = 500;
			$r[ 'errors' ][ 0 ][ 'error_type' ] = 'transaction_error';
			$r[ 'errors' ][ 0 ][ 'message' ] = $this -> _db -> errorInfo;
			$r[ 'errors' ][ 0 ][ 'sql' ] = $sql;
			$r[ 'errors' ][ 0 ][ 'location' ] = __LINE__;
			$r[ 'message' ] = '������ ��������';
			return $r;
		}
		else {
			$r[ 's' ] = true;
			$r[ 'code' ] = 200;
			return $r;
		}
	}
	*/

	
	
	
	
	
	/**
		strout to browser
	*/
	public function strout( $ret ) {

		// var_dump( $ret );
	
		if ( !isset( $ret[ 'code' ] ) || !isset( $this -> _headers[ $ret[ 'code' ] ] ) ) throw new Exception( 'invalid request ' . __LINE__ );

		$this -> _header = $ret[ 'code' ];
		if ( $this -> _header != 200 && ( !isset( $ret[ 'errors' ] ) || !is_array( $ret[ 'errors' ] ) ) ) {
			 throw new Exception( 'invalid request param[ errors ] cant be array ' . __LINE__ );
		}

		if ( isset( $ret[ 'query' ][ 'uri' ] ) ) unset( $ret[ 'query' ][ 'uri' ] );
		if ( isset( $ret[ 'query' ][ 'mod' ] ) ) unset( $ret[ 'query' ][ 'mod' ] );
		if ( isset( $ret[ 'query' ][ 'alias' ] ) ) unset( $ret[ 'query' ][ 'alias' ] );


		$ret[ 'code' ] = $this -> _header;
		$ret[ 'errors' ] = ( isset( $ret[ 'errors' ] ) && is_array( $ret[ 'errors' ] ) ) ? $ret[ 'errors' ] : array( );
		$ret[ 'message' ] = ( isset( $ret[ 'message' ] ) && $ret[ 'message' ] ) ? $ret[ 'message' ] : '';
		$ret[ 'data' ] = ( isset( $ret[ 'data' ] ) && $ret[ 'data' ] ) ? $ret[ 'data' ] : array( );


		if ( $this -> _debug == true ) {
			$ret[ 'route' ] = $_SERVER[ 'REQUEST_URI' ];
			$ret[ 'method' ] = $this -> _method;
			$ret[ 'header' ] = $this -> _header;
			if ( isset( $ret[ 'body' ] ) && $ret[ 'body' ] ) {
				// $this -> { '_decode' . $this -> _format }( file_get_contents( 'php://input' ) )
			}
			// $ret[ 'body' ] = $this -> _body;
			// $ret[ 'query' ] = $query;
		}

		ob_start( 'ob_gzhandler' );
		$this -> { '_header' . $this -> _format }( );
		//header( $this -> _headers[ $this -> _header ] );
		echo $this -> { '_encode' . $this -> _format }( $ret );
		ob_end_flush( );
		exit( ( $this -> _header == 200 ) ? 0 : 1 );
	}





	private function _headerJSON( ) {
		header( 'Content-type: application/json; charset=utf-8' );
	}
	
	private function _headerXML( ) {
		header( 'Content-type: text/xml; charset=utf-8' );
	}
	
	private function _headerCSV( ) {
		header( 'Content-Type: text/plain; charset=utf-8' );
	}





	private function _encodeJSON( $str ) {
		return json_encode( $str );
	}
	
	private function _encodeXML( $str ) {
		return $str;
	}
	
	private function _encodeCSV( $str ) {
		return $str;
	}
	
	
	
	private function _decodeJSON( $str ) {
		return json_decode( $str, true );
	}
	
	private function _decodeXML( $str ) {
		return $str;
	}
	
	private function _decodeCSV( $str ) {
		return $str;
	}
	
	









	

	private function _fields( $fields='' ) {
		if ( !$fields ) return '*';
		if ( strlen( $fields ) > 0 ) {
			$fields = explode( ',', $fields );
		}
		return ( ( count( $fields ) ) ? implode( ',', $fields ) : '*' );
	}


	private function _filter( $filter = null ) {
		$sql = '';
		$params = array( );
		if ( isset( $filter ) && is_array( $filter ) ) {
			if ( count( $filter ) ) {
				$sql .= ' && ';
				foreach( $filter as $k => $f ) {
					if ( is_string( $f ) ) $sql .= '`' . $k . '`=:' . $k . ' && ';
					else {
						$sql .= ' `' . $k . '` IN (' . implode( ',', $f ) . ') && ';
						unset( $filter[ $k ] );
					}
				}
			}
			$sql = substr( $sql, 0, -3 );
		}
		return array(
			'sql' => $sql,
			'params' => $filter
		);
	}



	/**
		������ ������ 
		$ret[ 'type' ] = 'section' or 'position';
		$ret[ 'params' ] = array route;
	*/
	private function _router_map_params( $params ) {
		if ( !$params[ 0 ] ) {
			throw new Exception( 'invalid request' );
		}

		if ( count( $this -> _route ) ) {
			$params = array_diff( $params, array( '' ) );
			$real_params = '/' . implode( '/', $params ) . '/';
			foreach( $this -> _route as $k_r => $r ) {
				if ( !$r ) continue;
				$real_params = preg_replace( $k_r, $r, $real_params, 1 );
			}
			$params = explode( '/', $real_params );
			array_shift( $params );
			array_pop( $params );
		}

		$ret = array( );
		$ret[ 'type' ] = 'section';
		foreach ( $params as $k => $param ) {
			if ( !$param ) continue;
			if ( $param == '_items' ) {
				$ret[ 'type' ] = 'position';
				continue;
			}
			$ret[ 'params' ][ ] = $param;
		}
		return $ret;
	}



	/**
		recursive function
		parent_id = id ������������� �������� catalog_section
	*/
	private function _router_check_params( $parent_id = null, $r = 0 ) {

		$sql = '';
		$pdo_params = array( );
		$pdo_params[ 'alias' ] = $this -> _params[ $r ];
		if ( !$r ) {
			$sql .= ' && ( isNull( `parent_id` ) || `parent_id` = 0 )';
		}
		else {
			$sql .= ' && `parent_id`=:parent_id';
			$pdo_params[ 'parent_id' ] = $parent_id;
		}

		$section = $this -> _db -> select( 'SELECT * FROM `' . $this -> _table . '` WHERE `alias`=:alias' . $sql . ' LIMIT 1', $pdo_params );
		/*
		echo "<pre>";
		var_dump( 'SELECT * FROM `' . $this -> _table . '` WHERE `alias`=:alias' . $sql . ' LIMIT 1' );
		var_dump( $pdo_params);
		var_dump( $section );
		echo "</pre>";
		*/

		if ( !count( $section ) ) {
			throw new Exception( 'invalid request' );
		}
		$section = end( $section );
		if ( isset( $this -> _params[ $r + 1 ] ) ) {
			$section = $this -> _router_check_params( $section[ 'id' ], $r + 1 );
		}
		return $section;
	}
	

	
	
	
	
	/*
		$users = array( 'login1' => 'password1', 'login2' => 'password2' ... );
	*/
	public function auth_basic( $users = array( ), $realm="Base API Realm" ) {

		// var_dump( $_REQUEST );
		// var_dump( getallheaders( ) );
	
		$valid_passwords = $users;
		$valid_users = array_keys( $valid_passwords );
		list( $_SERVER[ 'PHP_AUTH_USER' ], $_SERVER[ 'PHP_AUTH_PW' ] ) = explode( ':', base64_decode( substr( $_SERVER[ 'HTTP_AUTHORIZATION' ], 6 ) ) );
		$user = $_SERVER[ 'PHP_AUTH_USER' ];
		$pass = $_SERVER[ 'PHP_AUTH_PW' ];
		$validated = ( in_array( $user, $valid_users ) ) && ( $pass == $valid_passwords[ $user ] );
		if ( !$validated ) {
			header( 'WWW-Authenticate: Basic realm="' . $realm . '"' );
			header( 'HTTP/1.1 401 Unauthorized' );
			$r[ 's' ] = false;
			$r[ 'code' ] = 401;
			$r[ 'errors' ][ 0 ] = 'Not authorized';
			$r[ 'message' ] = 'Not authorized';
			$r[ 'location' ] = __LINE__;
			$this -> strout( $r );
		}
		return true;	
	}


	public function auth_digest( ) {
		
		
		
	}




	public function ssl_encrypt( $source, $type, $key ) {
		$private_key_details = openssl_pkey_get_details( $key );
		$decrypt_chunk_size = ceil( $private_key_details[ 'bits' ] / 8 ) - 11;
		$maxlength = $decrypt_chunk_size;

		$output = '';
		while( $source ) {
		  $input = substr( $source, 0, $maxlength );
		  $source = substr( $source, $maxlength );
		  if( $type == 'private' ) {
			$ok = openssl_private_encrypt( $input, $encrypted, $key );
		  } else {
			$ok = openssl_public_encrypt( $input, $encrypted, $key );
		  }
		  $output .= $encrypted;
		}
		return $output;
	}


	function ssl_decrypt( $source, $type, $key ) {
		$private_key_details = openssl_pkey_get_details( $key );
		$decrypt_chunk_size = ceil( $private_key_details[ 'bits' ] / 8 );
		$maxlength = $decrypt_chunk_size;
		$output = '';
		while( $source ) {
		  $input = substr( $source, 0, $maxlength );
		  $source = substr( $source, $maxlength );
		  if( $type == 'private' ) {
			$ok = openssl_private_decrypt( $input, $out, $key );
		  } else {
			$ok = openssl_public_decrypt( $input, $out, $key );
		  }
		  $output.=$out;
		}
		return $output;
	}
	
	
	
}
