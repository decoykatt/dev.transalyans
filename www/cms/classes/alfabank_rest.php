<?php
 /**
  * Autor:		Startcev PV
  * Email:		cefar@mail.ru
  * Phone:		+7(912)323-84-94
  * Company:	IT-FACTORY
  */
	class Alfabank_rest {

		// логин банка
		protected $_username;
		// пароль банка
		protected $_password;
		// URL Адрес сайта
		protected $_siteurl;
		// Адрес, на который надо перенаправить пользователя в случае успешной оплаты
		protected $_return_url;	
		// Адрес, на который надо перенаправить пользователя в случае неуспешной оплаты
		protected $_fail_url;
		// урл REST API
		// protected $_gatewey_url = 'https://test.paymentgate.ru/testpayment/rest/';
		protected $_gatewey_url = 'https://pay.alfabank.ru/payment/rest/';

		// sessionTimeoutSecs, Продолжительность жизни заказа в секундах.
		protected $_session_timeout;
		
		
		protected $_sole = 'lf234gvg2c4vvv2gKRVG$B$gVvgGCgGGgb';

		// описание статуса заказов
		public $order_status =	array(
											0 => 'Заказ зарегистрирован, но не оплачен',
											1 => 'Предавторизованная сумма захолдирована (для двухстадийных платежей)',
											2 => 'Проведена полная авторизация суммы заказа',
											3 => 'Авторизация отменена',
											4 => 'По транзакции была проведена операция возврата',
											5 => 'Инициирована авторизация через ACS банка-эмитента',
											6 => 'Авторизация отклонена'
									);


		// таблица содержащая заказы
		protected $_order_table = 'position_orders';	
		// плаьежные поля в таблице
		
		public static $table_rows =	array(
											// идентификатор товара на сайте
											'id' => 'id',
											// Номер заказа в платежной системе. Уникален в пределах системы. Отсутствует если регистрация заказа
											// не удалась по причине ошибки, детализированной в errorCode.
											'orderId' => 'pay_order_id',
											// URL платежной формы, на который надо перенаправить броузер клиента. Не возвращается если
											// регистрация заказа не удалась по причине ошибки, детализированной в errorCode.
											'formUrl' => 'pay_form_url',
											// статус платежа
											'OrderStatus' => 'pay_status',
											// Описание статуса платежа
											'pay_status_message' => 'pay_status_message',
											// Код ошибки
											'pay_error' => 'pay_error',
											// Описание ошибки на языке, переданном в параметре language в запросе.
											'pay_error_message' => 'pay_error_message',
											// урл возврата средств
											'pay_refund_url' => 'pay_refund_url',
											// Код ошибки возврата средств
											'pay_refund_error' => 'pay_refund_error',
											// Описание ошибки на языке, переданном в параметре language в запросе.
											'pay_refund_message' => 'pay_refund_message',
											// Сумма возврата
											'pay_refund_amount' => 'pay_refund_amount',
											// Маскированный номер карты, которая использовалась для оплаты. Указан только после оплаты заказа.
											'Pan' => 'pay_pan',
											// Срок истечения действия карты в формате YYYYMM. Указан только после оплаты заказа.
											'expiration' => 'pay_expiration',
											// Имя держателя карты. Указан только после оплаты заказа.
											'cardholderName' => 'pay_cardholder_name',
											// Код валюты платежа ISO 4217. Если не указан, считается равным 810 (российские рубли).
											'currency' => 'pay_currency',
											// Сумма платежа в копейках или центах
											'Amount' => 'pay_amount',
											// AN6 нет Код авторизации МПС. Поле фиксированной длины (6 символов), может содержать цифры и латинские буквы.
											'approvalCode' => 'pay_approval_сode',
											// IP адрес пользователя, который оплачивал заказ
											'Ip' => 'pay_ip',
											// Дата регистрации заказа
											'date' => 'pay_date',
											// Описание заказа, переданное при его регистрации
											'orderDesctiption' => 'pay_description',
											// Расшифровка кода ответа на языке, переданном в параметре Language в запросе.
											'actionCodeDesctiption' => 'pay_action_code_desctiption',
											// Номер (идентификатор) клиента в системе магазина, переданный при регистрации заказа. Присутствует только если магазину разрешено создание связок.
											'clientId' => 'pay_client_id',
											// Идентификатор связки созданной при оплате заказа или использованной для оплаты. Присутствует только если магазину разрешено создание связок.
											'bindingId' => 'pay_binding_id'

									);

		// Объкт коннекта к БД
		protected $_db;

		// Код валюты платежа ISO 4217. Если не указан, считается равным 810 (российские рубли).
		protected $_currency = 810; 



		public function __construct( $conf ) {

			if ( empty( $conf[ 'siteurl' ] ) || !$conf[ 'siteurl' ] ) {
				throw new Exception( 'params siteurl not found' );
			}
			if ( empty( $conf[ 'username' ] ) || !$conf[ 'username' ] ) {
				throw new Exception( 'params username not found' );
			}
			if ( empty( $conf[ 'password' ] ) || !$conf[ 'password' ] ) {
				throw new Exception( 'params password not found' );
			}
			// Адрес, на который надо перенаправить пользователя в случае успешной оплаты
			if ( empty( $conf[ 'return_url' ] ) || !$conf[ 'return_url' ] ) {
				throw new Exception( 'params return_url not found' );
			}
			// Адрес, на который надо перенаправить пользователя в случае неуспешной оплаты
			if ( empty( $conf[ 'fail_url' ] ) || !$conf[ 'fail_url' ] ) {
				throw new Exception( 'params fail_url not found' );
			}
			
			$this -> _username = $conf[ 'username' ];
			$this -> _password = $conf[ 'password' ];
			$this -> _siteurl = $conf[ 'siteurl' ];
			$this -> _return_url = $conf[ 'return_url' ];
			$this -> _fail_url = $conf[ 'fail_url' ];

			if ( isset( $conf[ 'session_timeout' ] ) ) {
				$this -> _session_timeout = $conf[ 'session_timeout' ];
			}

			$this -> _db = new Table( $this -> _order_table );

		}


		public function __destruct( ) {
			;
		}

		// получение заказа по ид сайта
		public function get_order( $cols=array(), $orderby=array() ) {

			$pdo_where = '';
			$pdo_orderby = '';
			if ( count( $cols ) ) {
				$pdo_where .= ' WHERE ';
				foreach ( $cols as $key => $c ) {
					$pdo_where .= '`' . $key . '`=:' . $key . ' && ';
				}
				$pdo_where = substr( $pdo_where, 0, -4 );
			}

			if ( count( $orderby ) ) {
				$pdo_orderby .= ' ORDER BY ';
				$pdo_orderby .= implode( ',', $orderby );
			}

			return $this -> _db -> select( 'SELECT * FROM `' . $this -> _order_table . '`' . $pdo_where . $pdo_orderby, $cols );
		}


	/**
	 * ЗАПРОС РЕГИСТРАЦИИ ОДНОСТАДИЙНОГО ПЛАТЕЖА В ПЛАТЕЖНОМ ШЛЮЗЕ
	 *      register.do
	 *
	 * ПАРАМЕТРЫ
	 *      site_order_id	Ид заказа на сайте
	 *      amount          Сумма заказа в копейках.
	 *      description		Описание заказа в свободной форме
	 *		json			Поля дополнительной информации для последующего хранения, вида {"param":"value","param2":"value2"}.
	 *						Данные поля могут быть переданы в процессинг банка для последующего
	 *						отображения в реестрах.
	 *						( * Включение данного функционала возможно по согласованию с банком в период интеграции. )
	 *						Если для продавца настроена отправка уведомлений покупателю, адрес электронной почты
	 *						покупателя должен передаваться в этом блоке в параметре с именем "email".
	 *
	 * ОТВЕТ
	 *      В случае ошибки:
	 *          errorCode       Код ошибки. Список возможных значений приведен в таблице ниже.
	 *          errorMessage    Описание ошибки.
	 *
	 *      В случае успешной регистрации:
	 *          orderId         Номер заказа в платежной системе. Уникален в пределах системы.
	 *          formUrl         URL платежной формы, на который надо перенаправить браузер клиента.
	 *
	 *  Код ошибки      Описание
	 *      0           Обработка запроса прошла без системных ошибок.
	 *      1           Заказ с таким номером уже зарегистрирован в системе.
	 *      3           Неизвестная (запрещенная) валюта.
	 *      4           Отсутствует обязательный параметр запроса.
	 *      5           Ошибка значения параметра запроса.
	 *      7           Системная ошибка.
	 */
		public function register( $site_order_id, $amount, $description='', $json=null ) {

			if ( empty( $site_order_id ) || !$site_order_id ) {
				throw new Exception( 'params site_order_id not found' );
			}

			if ( empty( $amount ) || !$amount ) {
				throw new Exception( 'params amount not found' );
			}

			$data = array(
				'userName' => $this -> _username,
				'password' => $this -> _password,
				'orderNumber' => urlencode( $site_order_id ),
				'amount' => urlencode( $amount ),
				'returnUrl' => $this -> _return_url,
				'currency' => $this -> _currency
			);

			// Продолжительность жизни заказа в секундах.
			if ( isset( $this -> _session_timeout ) ) {
				$data[ 'sessionTimeoutSecs' ] = $this -> _session_timeout;
			}
			
			if ( $description != '' ) $data[ 'description' ] = $description;
			if ( isset( $json ) ) $data[ 'jsonParams' ] = $json;

			$response = $this -> gateway( 'register.do', $data );
			if ( gettype( $response ) != 'array' ) return $response;

			// В случае ошибки вывести ее
			if ( isset( $response[ 'errorCode' ] ) ) {

				// МЕНЯЕМ ЗАКАЗ
				$this -> _db -> execute(
					'UPDATE `' . $this -> _order_table . '`
					 SET `' . ( self :: $table_rows[ 'pay_error' ] ) . '`=' . $response[ 'errorCode' ] . ',
						 `' . ( self :: $table_rows[ 'pay_status_message' ] ) . '`="' . $response[ 'errorMessage' ] . '"
					 WHERE `' . ( self :: $table_rows[ 'id' ] ) . '` = "' . $site_order_id . '";'
				);

				/*
					echo 'Ошибка #' . $response[ 'errorCode' ] . ': ' . $response[ 'errorMessage' ];
				*/
			}
			// В случае успеха перенаправить пользователя на плетжную форму
			else if ( isset( $response[ 'formUrl' ] ) && isset( $response[ 'orderId' ] ) ) {

				// МЕНЯЕМ ЗАКАЗ
				$this -> _db -> execute(
					'UPDATE `' . $this -> _order_table . '`
					 SET `' . ( self :: $table_rows[ 'formUrl' ] ) . '`="' . $response[ 'formUrl' ] . '",
						 `' . ( self :: $table_rows[ 'orderId' ] ) . '`="' . $response[ 'orderId' ] . '"
					 WHERE `' . ( self :: $table_rows[ 'id' ] ) . '` = "' . $site_order_id . '";'
				);
			}

			return $response;
		}


		/**
		 * ЗАПРОС СОСТОЯНИЯ ЗАКАЗА
		 *      getOrderStatus.do
		 *
		 * ПАРАМЕТРЫ
		 *		pay_order_id         Номер заказа в платежной системе. Уникален в пределах системы.
		 *
		 * ОТВЕТ
		 *      ErrorCode			Код ошибки. Список возможных значений приведен в таблице ниже.
		 *      OrderStatus		По значению этого параметра определяется состояние заказа в платежной системе.
		 *								Список возможных значений приведен в таблице ниже. Отсутствует, если заказ не был найден.
		 *
		 *  Код ошибки      Описание
		 *      0           Обработка запроса прошла без системных ошибок.
		 *      2           Заказ отклонен по причине ошибки в реквизитах платежа.
		 *      5           Доступ запрещён;
		 *                  Пользователь должен сменить свой пароль;
		 *                  Номер заказа не указан.
		 *      6           Неизвестный номер заказа.
		 *      7           Системная ошибка.
		 *
		 *  Статус заказа   Описание
		 *      0           Заказ зарегистрирован, но не оплачен.
		 *      1           Предавторизованная сумма захолдирована (для двухстадийных платежей).
		 *      2           Проведена полная авторизация суммы заказа.
		 *      3           Авторизация отменена.
		 *      4           По транзакции была проведена операция возврата.
		 *      5           Инициирована авторизация через ACS банка-эмитента.
		 *      6           Авторизация отклонена.
		 */
		public function status( $pay_order_id ) {

			$data = array(
					'userName' => $this -> _username,
					'password' => $this -> _password,
					'orderId' => $pay_order_id,
			);

			$response = $this -> gateway( 'getOrderStatus.do', $data );
			if ( gettype( $response ) != 'array' ) return $response;
			
				// МЕНЯЕМ ЗАКАЗ
			$sql = 'UPDATE `' . $this -> _order_table . '` SET 
					 `' . ( self :: $table_rows[ 'pay_refund_url' ] ) . '`="' . ( $this -> cashback_url( $pay_order_id ) ) . '"
					 ,`' . ( self :: $table_rows[ 'OrderStatus' ] ) . '`="' . $response[ 'OrderStatus' ] . '"
					 ,`' . ( self :: $table_rows[ 'Amount' ] ) . '`="' . $response[ 'Amount' ] . '"
					 ,`' . ( self :: $table_rows[ 'pay_status_message' ] ) . '`="' . $this -> order_status[ $response[ 'OrderStatus' ] ] . '" ';
			
			if ( isset( $response[ 'ErrorCode' ] ) ) {
				$sql .= ',`' . ( self :: $table_rows[ 'pay_error' ] ) . '`="' . $response[ 'ErrorCode' ] . '" ';
			}
			if ( isset( $response[ 'ErrorMessage' ] ) ) {
					 ',`' . ( self :: $table_rows[ 'pay_error_message' ] ) . '`="' . $response[ 'ErrorMessage' ] . '" ';
			}
			if ( isset( $response[ 'Pan' ] ) ) {
				$sql .= ',`' . ( self :: $table_rows[ 'Pan' ] ) . '`="' . $response[ 'Pan' ] . '" ';
			}
			if ( isset( $response[ 'expiration' ] ) ) {
				$sql .= ',`' . ( self :: $table_rows[ 'expiration' ] ) . '`="' . $response[ 'expiration' ] . '" ';
			}
			if ( isset( $response[ 'cardholderName' ] ) ) {
				$sql .= ',`' . ( self :: $table_rows[ 'cardholderName' ] ) . '`="' . $response[ 'cardholderName' ] . '" ';
			}
			if ( isset( $response[ 'currency' ] ) ) {
				$sql .= ',`' . ( self :: $table_rows[ 'currency' ] ) . '`="' . $response[ 'currency' ] . '" ';
			}
			if ( isset( $response[ 'approvalCode' ] ) ) {
				$sql .= ',`' . ( self :: $table_rows[ 'approvalCode' ] ) . '`="' . $response[ 'approvalCode' ] . '" ';
			}
			if ( isset( $response[ 'Ip' ] ) ) {
				$sql .= ',`' . ( self :: $table_rows[ 'Ip' ] ) . '`="' . $response[ 'Ip' ] . '" ';
			}
			if ( isset( $response[ 'date' ] ) ) {
				$sql .= ',`' . ( self :: $table_rows[ 'date' ] ) . '`="' . $response[ 'date' ] . '" ';
			}
			if ( isset( $response[ 'orderDesctiption' ] ) ) {
				$sql .= ',`' . ( self :: $table_rows[ 'orderDesctiption' ] ) . '`="' . $response[ 'orderDesctiption' ] . '" ';
			}
			if ( isset( $response[ 'actionCodeDesctiption' ] ) ) {
				$sql .= ',`' . ( self :: $table_rows[ 'actionCodeDesctiption' ] ) . '`="' . $response[ 'actionCodeDesctiption' ] . '" ';
			}
			if ( isset( $response[ 'clientId' ] ) ) {
				$sql .= ',`' . ( self :: $table_rows[ 'clientId' ] ) . '`="' . $response[ 'clientId' ] . '" ';
			}
			if ( isset( $response[ 'bindingId' ] ) ) {
				$sql .= ',`' . ( self :: $table_rows[ 'bindingId' ] ) . '`="' . $response[ 'bindingId' ] . '" ';
			}
				$sql .= 'WHERE `' . ( self :: $table_rows[ 'orderId' ] ) . '` = "' . $pay_order_id . '";';

			$this -> _db -> execute( $sql );
			
			return $response;
		}

		/** ЗАПРОС ВОЗВРАТА СРЕДСТВ ОПЛАТЫ ЗАКАЗА
		 *			refund.do
		 *
		 * По этому запросу средства по указанному заказу будут возвращены плательщику. Запрос закончится ошибкой в случае, если средства
		 * по этому заказу не были списаны. Система позволяет возвращать средства более одного раза, но в общей сложности не более
		 * первоначальной суммы списания.
		 * первоначальной суммы списания.
		 * Для выполнения операции возврата необходимо наличие соответствующих права в системе.
		 *
		 *		ПАРАМЕТРЫ
		 *			cashback			MD5() значение возврата денежных средств			
		 *			pay_order_id 		Номер заказа в платежной системе. Уникален в пределах системы.
		 *			amount				Сумма платежа в копейках (или центах)
		 *
		 *		ОТВЕТ
		 *			errorCode			Код ошибки.
		 *			errorMessage		Описание ошибки на языке.
		 *
		 *
		 */
		public function refund( $pay_order_id, $amount ) {

			$rows = $this -> _db -> select( 'SELECT * FROM `' . $this -> _order_table . '` WHERE `pay_refund_url`="' . ( $this -> cashback_url( $pay_order_id ) ) . '"' );
			if ( !count( $rows ) ) return false;

			$data = array(
				'userName' => $this -> _username,
				'password' => $this -> _password,
				'orderId' => $pay_order_id,
				'amount' => $amount
			);

			$response = $this -> gateway( 'refund.do', $data );
			if ( gettype( $response ) != 'array' ) return $response;

			if ( isset( $response[ 'errorCode' ] ) && isset( $response[ 'errorMessage' ] ) ) {
				// МЕНЯЕМ ЗАКАЗ
				$this -> _db -> execute(
					'UPDATE `' . $this -> _order_table . '`
					 SET `' . ( self :: $table_rows[ 'pay_refund_error' ] ) . '`="' . $response[ 'errorCode' ] . '",
						 `' . ( self :: $table_rows[ 'pay_refund_message' ] ) . '`="' . $response[ 'errorMessage' ] . '"

					 WHERE `' . ( self :: $table_rows[ 'orderId' ] ) . '` = "' . $pay_order_id . '";'
				);
			}
			return $response;
		}
		

		public function cashback_url( $pay_order_id ) {
			return $this -> _siteurl . 'refund-url/?cashback=' . md5( $pay_order_id . $this -> _sole );
		}


		/**
		 * ФУНКЦИЯ ДЛЯ ВЗАИМОДЕЙСТВИЯ С ПЛАТЕЖНЫМ ШЛЮЗОМ
		 *
		 * Для отправки POST запросов на платежный шлюз используется
		 * стандартная библиотека cURL.
		 *
		 * ПАРАМЕТРЫ
		 *      method      Метод из API.
		 *      data        Массив данных.
		 *
		 * ОТВЕТ
		 *      response    Ответ.
		 */
		public function gateway( $method, $data ) {
			$curl = curl_init( ); // Инициализируем запрос
			curl_setopt_array( $curl, array(
				CURLOPT_URL => $this -> _gatewey_url . $method, // Полный адрес метода
				CURLOPT_RETURNTRANSFER => true, // Возвращать ответ
				CURLOPT_POST => true, // Метод POST
				//CURLOPT_CONNECTTIMEOUT => 20,
				//CURLOPT_TIMEOUT => 20,
				//CURLOPT_VERBOSE => true,
				//CURLOPT_HEADER => true,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_SSL_VERIFYHOST => false,
				CURLOPT_POSTFIELDS => http_build_query( $data ) // Данные в запросе
			) );
			$response = curl_exec( $curl ); // Выполненяем запрос
			$info = curl_getinfo( $curl );
			$response = json_decode( $response, true ); // Декодируем из JSON в массив
			//$response[ 'curl_getinfo' ] = $info;
			curl_close( $curl ); // Закрываем соединение
			return $response; // Возвращаем ответ
		}
		
		
		
		
		
		
		
		
		
	}
