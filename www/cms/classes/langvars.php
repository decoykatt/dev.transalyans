<?php

  class Langvars {


   /**********************************
     * Метод замены месяцев кириллицей
     *********************************/
    public static function replaceMonth( $month ) {
  
      $new_month = '';
  
      switch ( $month ) {
  
        case '01' : $new_month = 'Января';
                    break;
  
        case '02' : $new_month = 'Февраля';
                    break;
  
        case '03' : $new_month = 'Марта';
                    break;
  
        case '04' : $new_month = 'Апреля';
                    break;
  
        case '05' : $new_month = 'Мая';
                    break;
  
        case '06' : $new_month = 'Июня';
                    break;
      
        case '07' : $new_month = 'Июля';
                    break;
  
        case '08' : $new_month = 'Августа';
                    break;
  
        case '09' : $new_month = 'Сентября';
                    break;
  
        case '10' : $new_month = 'Октября';
                    break;
  
        case '11' : $new_month = 'Ноября';
                    break;
  
        case '12' : $new_month = 'Декабря';
                    break;
  
        default : $new_month = 'Января';
  
      }
  
      return $new_month;
    
    }
  
  
    public static function replaceWeek( $week ) {
  
      $new_week = '';
  
      switch ( $week ) {
  
        case '1' : $new_week = 'Понедельник';
                    break;
  
        case '2' : $new_week = 'Вторник';
                    break;
  
        case '3' : $new_week = 'Среда';
                    break;
  
        case '4' : $new_week = 'Четверг';
                    break;
  
        case '5' : $new_week = 'Пятница';
                    break;
  
        case '6' : $new_week = 'Суббота';
                    break;
      
        case '0' : $new_week = 'Воскресение';
                    break;

        default : $new_week = 'Пятница';
 
      }
 
      return $new_week;
    
    }
  
  
  
  }