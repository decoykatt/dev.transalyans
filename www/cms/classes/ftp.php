<?php


class Ftp {

	private $_server;	// ftp server
	private $_user;		// ftp user
	private $_pass;		// ftp pass

	public $connect;	// connection_id

	// set option form connect
	public function __construct( $conf ) {

		if ( isset( $conf[ 'server' ] ) ) $this -> _server = $conf[ 'server' ];
		if ( isset( $conf[ 'user' ] ) ) $this -> _user = $conf[ 'user' ];
		if ( isset( $conf[ 'pass' ] ) ) $this -> _pass = $conf[ 'pass' ];

		if ( !$this -> _server ) throw new Exception( 'param `server` not found' );
		if ( !$this -> _user ) throw new Exception( 'param `user` not found' );
		if ( !$this -> _pass ) throw new Exception( 'param `pass` not found' );

		// set up basic connection
		$this -> connect = ftp_connect( $this -> _server );

	}


	// close the connection	
	public function __destruct( $conf ) {
		ftp_close( $this -> connect );
	}

	// upload file form server
	public function upload( $from, $to ) {
		// login with username and password
		$login_result = ftp_login( $this -> connect, $this -> _user, $this -> _pass );
		// upload a file
		if ( !ftp_put( $this -> connect, $from, $to, FTP_ASCII ) ) {
			throw new Exception( "There was a problem while uploading $file\n" );
		}
		return true;
	}


	// upload recursive files in directory from server
	public function uploadTree( $from, $to ) {
		$d = dir( $from );
		while ( $file = $d -> read( ) ) { // do this for each file in the directory
			if ( $file != "." && $file != ".." ) { // to prevent an infinite loop
				if ( is_dir( $form . "/" . $file ) ) { // do the following if it is a directory
					if ( !@ftp_chdir( $this -> connect, $to . " /" . $file ) ) {
						ftp_mkdir( $this -> connect, $to . "/" . $file ); // create directories that do not yet exist
					}
					$this -> uploadTree( $this -> connect, $from . "/" . $file, $to . "/" . $file ); // recursive part
				}
				else {
					$upload = ftp_put( $this -> connect, $to . "/" . $file, $from . "/" . $file, FTP_BINARY ); // put the files
				}
			}
		}
		$d -> close();
	}

}











