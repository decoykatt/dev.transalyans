<?php

class mysiteConfig extends Config_Base
{
    public $template = "/cms/modules/mysite/html/templates/cm_template.php";

    public $template_login = "/cms/modules/mysite/html/templates/cm_login_template.php";

    public $title = "CMS";

    public $version = "2.2.1";
}

?>
