<?php
/**
 *
 */
class CatalogInstall extends Install_Base
{
    public $required = true;

    public $title = "Разделы";

    public $dirs = array('catalog/cache','catalog/upload','catalog/modcache');

        public $sql =
"CREATE TABLE IF NOT EXISTS `catalog_section` (
  `id` int(11) NOT NULL auto_increment,
  `parent_id` int(11) default NULL,
  `title` varchar(500) collate utf8_unicode_ci NOT NULL,
  `position` int(11) default NULL,
  `alias` varchar(250) collate utf8_unicode_ci default NULL,
  `position_table` varchar(250) collate utf8_unicode_ci default NULL,
  `section_table` varchar(250) collate utf8_unicode_ci default NULL,
  `children_tpl` text collate utf8_unicode_ci,
  `leaf` smallint(6) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;

CREATE TABLE IF NOT EXISTS `section_news` (
  `id` int(11) NOT NULL,
  `page_size` int(11) default NULL,
  `head_title` varchar(500) collate utf8_unicode_ci default NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci default NULL,
  `meta_description` text COLLATE utf8_unicode_ci default NULL,
  `tag` text COLLATE utf8_unicode_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;

CREATE TABLE IF NOT EXISTS `position_news` (
  `id` int(11) NOT NULL auto_increment,
  `section_id` int(11) NOT NULL,
  `title` varchar(500) collate utf8_unicode_ci default NULL,
  `datestamp` int(11) default NULL,
  `description` text collate utf8_unicode_ci,
  `content` LONGTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `img` varchar(200) collate utf8_unicode_ci default NULL,
  `public` tinyint(2) DEFAULT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;

CREATE TABLE IF NOT EXISTS  `position_setting` (
  `id` int(11) NOT NULL auto_increment,
  `section_id` int(11) NOT NULL,
  `title` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `img` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `public` tinyint(2) DEFAULT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `section_form_feedback` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `email` varchar(250) collate utf8_unicode_ci default NULL,
  `efrom` varchar(500) collate utf8_unicode_ci default NULL,
  `efromname` varchar(500) collate utf8_unicode_ci default NULL,
  `esubject` varchar(500) collate utf8_unicode_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;

CREATE TABLE IF NOT EXISTS `section_page` (
  `id` int(11) NOT NULL,
  `content` LONGTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `visible` smallint(6) default '1',
  `head_title` varchar(500) collate utf8_unicode_ci default NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci default NULL,
  `meta_description` text COLLATE utf8_unicode_ci default NULL,
  `tag` text COLLATE utf8_unicode_ci default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `position_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `type_id` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `valid_empty` smallint(6) DEFAULT '0',
  `valid_email` smallint(6) DEFAULT '0',
  `select_options` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `nameid` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `html_before` text,
  `html_after` text,
  `retailcrm_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `section_forms` (
  `id` int(11) NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `efrom` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `efromname` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `esubject` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `captcha` smallint(6) DEFAULT '0',
  `header_mail` text COLLATE utf8_unicode_ci,
  `title_form` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `success_message` text COLLATE utf8_unicode_ci,
  `html` text COLLATE utf8_unicode_ci,
  `html_id` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class_form` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `method` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'post',
  `action` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `save_table` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `retailcrm_order_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `retailcrm_on` tinyint(2) DEFAULT NULL,
  `ya_metrika_target_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ya_metrika_target_id_button` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ya_metrika_on` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



CREATE TABLE IF NOT EXISTS `section_forms_settings` (
  `id` int(11) NOT NULL,

  `ya_metrika_key` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ya_metrika_id` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ya_metrika_on` tinyint(2) DEFAULT NULL,

  `retailcrm_key` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `retailcrm_on` tinyint(2) DEFAULT NULL,

  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- --------------------------------------------------------

--
-- Структура таблицы `forms_field_action`
--

CREATE TABLE IF NOT EXISTS `forms_field_action` (
  `id` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `position` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `forms_field_action`
--

INSERT INTO `forms_field_action` (`id`, `name`, `position`) VALUES
('auth', 'Авторизация', 2),
('is_active', 'Активация учетной запись', 4),
('register', 'Регистрация', 1),
('restore_pass', 'Восстановить пароль', 3),
('send_email', 'Отправка на email', 5);

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `forms_field_type` (
  `id` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `position` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `forms_field_type`
--

INSERT INTO `forms_field_type` (`id`, `name`, `position`) VALUES
('check', 'Флаг', 7),
('date', 'Дата', 5),
('hidden', 'Скрытое поле', 11),
('html', 'Произвольный HTML', 15),
('info', 'Информация', 14),
('label', 'Подпись', 13),
('link', 'Ссылка', 10),
('memo', 'Многострочный текст', 2),
('pwd', 'Пароль', 6),
('radiobox', 'Радиобокс', 12),
('select', 'Выпадающий список', 3),
('submit', 'Кнопка подачи запроса', 8),
('text', 'Текстовая строка', 1),
('upload', 'Обзор', 4);
";

            public $config =
'<?xml version="1.0" encoding="UTF-8"?>
<config>
    <params>
        <param type="array">
            <description>Таблицы разделов</description>
            <name>tables_section</name>
            <items>
                <item name="section_news" type="array">
                    <value name="title">Новости</value>
                    <value name="table">section_news</value>
                </item>
                <item name="section_forms" type="array">
                    <value name="title">Форма</value>
                    <value name="table">section_forms</value>
                </item>
                <item name="section_forms_settings" type="array">
                    <value name="title">Настройки форм</value>
                    <value name="table">section_forms_settings</value>
                </item>
                <item name="section_page" type="array">
                    <value name="title">Страница</value>
                    <value name="table">section_page</value>
                </item>
            </items>
        </param>
        <param type="array">
            <description>Таблицы позиций</description>
            <name>tables</name>
            <items>

                <item name="position_setting" type="array">
                    <value name="title">Опции</value>
                    <value name="table">position_setting</value>
                    <value name="order">ORDER BY position</value>
                </item>
                <item name="position_news" type="array">
                    <value name="title">Новости</value>
                    <value name="table">position_news</value>
                    <value name="order">ORDER BY datestamp DESC</value>
                </item>
                <item name="position_forms" type="array">
                    <value name="title">Поля формы</value>
                    <value name="table">position_forms</value>
                    <value name="sql">
                        select p1.*, p2.name as type_name
                        from position_forms p1
                        left outer join forms_field_type p2 on p1.type_id=p2.id
                        where section_id=:section_id
                        ORDER BY position
                    </value>
                </item>
            </items>
        </param>
        <param type="array">
            <name>section_news</name>
            <items>
                <item name="page_size" type="array">
                    <value name="field">page_size</value>
                    <value name="title">Количество новостей на странице</value>
                    <value name="type">int</value>
                </item>
            </items>
        </param>
		
        <param type="array">
            <name>position_setting</name>
            <items>
                <item name="title" type="array">
                    <value name="field">title</value>
                    <value name="title">Ключ</value>
                    <value name="type">text</value>
                </item>
                <item name="value" type="array">
                    <value name="field">value</value>
                    <value name="title">Значение</value>
                    <value name="type">text</value>
                </item>
                <item name="description" type="array">
                    <value name="field">description</value>
                    <value name="title">Описание</value>
                    <value name="type">memo</value>
                    <value name="mode">edit</value>
                </item>
                <item name="position" type="array">
                    <value name="field">position</value>
                    <value name="title">Позиция</value>
                    <value name="type">int</value>
                </item>
                <item name="public" type="array">
                    <value name="field">public</value>
                    <value name="title">Опция включена</value>
                    <value name="type">check</value>
                </item>
            </items>
        </param>

        <param type="array">
            <name>position_news</name>
            <items>
                <item name="datestamp" type="array">
                    <value name="field">datestamp</value>
                    <value name="title">Дата</value>
                    <value name="type">date</value>
                </item>
                <item name="img" type="array">
                    <value name="field">img</value>
                    <value name="title">Картинка</value>
                    <value name="type">image</value>
                    <value name="mode">edit</value>
                </item>
                <item name="title" type="array">
                    <value name="field">title</value>
                    <value name="title">Заголовок новости</value>
                    <value name="type">text</value>
                </item>
                <item name="description" type="array">
                    <value name="field">description</value>
                    <value name="title">Анонс</value>
                    <value name="type">memo</value>
                    <value name="mode">edit</value>
                </item>
                <item name="content" type="array">
                    <value name="field">content</value>
                    <value name="title">Содержание</value>
                    <value name="type">richtext</value>
                    <value name="mode">edit</value>
                </item>
                <item name="public" type="array">
                    <value name="field">public</value>
                    <value name="title">Публикация</value>
                    <value name="type">check</value>
                </item>
            </items>
        </param>

        <param type="array">
            <name>position_forms</name>
            <items>
                <item name="position" type="array">
                    <value name="field">position</value>
                    <value name="title">Порядок</value>
                    <value name="type">int</value>
                </item>
				
                <item name="html_before" type="array">
                    <value name="field">html_before</value>
                    <value name="title">HTML текст перед полем</value>
                    <value name="type">memo</value>
                    <value name="mode">edit</value>
                </item>

				<item name="html_after" type="array">
                    <value name="field">html_after</value>
                    <value name="title">HTML текст после поля</value>
                    <value name="type">memo</value>
                    <value name="mode">edit</value>
                </item>
				
                <item name="name" type="array">
                    <value name="field">name</value>
                    <value name="title">Значение поля</value>
                    <value name="type">text</value>
                </item>

                <item name="nameid" type="array">
                    <value name="field">nameid</value>
                    <value name="title">Название поля</value>
                    <value name="type">text</value>
                </item>

                <item name="type_id" type="array">
                    <value name="field">type_id</value>
                    <value name="display">type_name</value>
                    <value name="title">Тип поля</value>
                    <value name="type">select</value>
                    <value name="select">SELECT id, name AS display FROM forms_field_type ORDER BY position ASC</value>
                </item>

                <item name="select_options" type="array">
                    <value name="field">select_options</value>
                    <value name="title">Значения для выпадающего списка</value>
                    <value name="type">memo</value>
                    <value name="mode">edit</value>
                </item>
                <item name="valid_empty" type="array">
                    <value name="field">valid_empty</value>
                    <value name="title">Обязательно для заполнения</value>
                    <value name="type">check</value>
                    <value name="mode">edit</value>
                </item>
                <item name="valid_email" type="array">
                    <value name="field">valid_email</value>
                    <value name="title">Проверка Email</value>
                    <value name="type">check</value>
                    <value name="mode">edit</value>
                </item>
                <item name="id" type="array">
                    <value name="field">id</value>
                    <value name="title">ID</value>
                    <value name="type">text</value>
                    <value name="mode">browse</value>
                </item>
                <item name="retailcrm_name" type="array">
                    <value name="field">retailcrm_name</value>
                    <value name="title">Имя поля в RetailCRM</value>
                    <value name="type">text</value>
                    <value name="mode">edit</value>
                </item>
            </items>
        </param>

        <param type="array">
            <name>section_forms</name>
            <items>
                <item name="email" type="array">
                    <value name="field">email</value>
                    <value name="title">Email отправки формы</value>
                    <value name="type">text</value>
                </item>

                <item name="html_id" type="array">
                    <value name="field">html_id</value>
                    <value name="title">html id формы</value>
                    <value name="type">text</value>
                </item>

                <item name="class_form" type="array">
                    <value name="field">class_form</value>
                    <value name="title">class формы</value>
                    <value name="type">text</value>
                </item>

				<item name="action" type="array">
                    <value name="field">action</value>
                    <value name="title">Действие формы</value>
                    <value name="type">selecttext</value>
                    <value name="select">login;logout;is_active;register;restore_pass;pay_account;send_email;send_email_save_bd;save_only</value>
                </item>

				<item name="method" type="array">
                    <value name="field">method</value>
                    <value name="title">Метод отправки запроса</value>
                    <value name="type">selecttext</value>
                    <value name="select">post;get;ajax-post;ajax-get</value>
                </item>

                <item name="subject" type="array">
                    <value name="field">esubject</value>
                    <value name="title">Тема письма</value>
                    <value name="type">text</value>
                </item>
                
                <item name="html" type="array">
                    <value name="field">html</value>
                    <value name="title">Шаблон письма</value>
                    <value name="type">richtext</value>
                </item>

                <item name="success_message" type="array">
                    <value name="field">success_message</value>
                    <value name="title">Сообщение об успехе действия</value>
                    <value name="type">memo</value>
                </item>


                <item name="ya_metrika_on" type="array">
                    <value name="field">ya_metrika_on</value>
                    <value name="title">Синхронизировать форму с Яндекс Метрикой</value>
                    <value name="type">check</value>
                </item>
				
                <item name="ya_metrika_target_id_button" type="array">
                    <value name="field">ya_metrika_target_id_button</value>
                    <value name="title">Идентификатор цели Яндекс Метрики - Кнопка</value>
                    <value name="type">text</value>
                </item>
				
                <item name="ya_metrika_target_id" type="array">
                    <value name="field">ya_metrika_target_id</value>
                    <value name="title">Идентификатор цели Яндекс Метрики - Запрос</value>
                    <value name="type">text</value>
                </item>



                <item name="retailcrm_on" type="array">
                    <value name="field">retailcrm_on</value>
                    <value name="title">Синхронизировать форму с RetailCRM</value>
                    <value name="type">check</value>
                </item>

                <item name="retailcrm_order_method" type="array">
                    <value name="field">retailcrm_order_method</value>
                    <value name="title">Способ оформления в RetailCRM (символьный код)</value>
                    <value name="type">text</value>
                </item>



            </items>
        </param>

        <param type="array">
            <name>section_forms_settings</name>
            <items>

                <item name="ya_metrika_key" type="array">
                    <value name="field">ya_metrika_key</value>
                    <value name="title">Ключ в Яндекс Метрике</value>
                    <value name="type">text</value>
                </item>

                <item name="ya_metrika_id" type="array">
                    <value name="field">ya_metrika_id</value>
                    <value name="title">Идентификатор счетчика Яндекс Метрики</value>
                    <value name="type">text</value>
                </item>

                <item name="ya_metrika_on" type="array">
                    <value name="field">ya_metrika_on</value>
                    <value name="title">Синхронизировать формы с Яндекс Метрика</value>
                    <value name="type">check</value>
                </item>


                <item name="retailcrm_key" type="array">
                    <value name="field">retailcrm_key</value>
                    <value name="title">Ключ сайта в RetailCRM</value>
                    <value name="type">text</value>
                </item>

                <item name="retailcrm_on" type="array">
                    <value name="field">retailcrm_on</value>
                    <value name="title">Синхронизировать формы с RetailCRM</value>
                    <value name="type">check</value>
                </item>

            </items>
        </param>

        <param type="array">
            <name>section_page</name>
            <items>
                <item name="content" type="array">
                    <value name="field">content</value>
                    <value name="title">Содержание</value>
                    <value name="type">richtext</value>
                </item>
                <item name="visible" type="array">
                    <value name="field">visible</value>
                    <value name="title">Доступна</value>
                    <value name="type">check</value>
                </item>
                <item name="head_title" type="array">
                    <value name="field">head_title</value>
                    <value name="title">TITLE</value>
                    <value name="type">text</value>
                </item>
                <item name="meta_keywords" type="array">
                    <value name="field">meta_keywords</value>
                    <value name="title">META KEYWORDS</value>
                    <value name="type">text</value>
                </item>
                <item name="meta_description" type="array">
                    <value name="field">meta_description</value>
                    <value name="title">META DESCRIPTION</value>
                    <value name="type">text</value>
                </item>
                <item name="tag" type="array">
                    <value name="field">tag</value>
                    <value name="title">META TAG</value>
                    <value name="type">memo</value>
                </item>
            </items>
        </param>
    </params>
</config>';

    public function exec_sql()
    {
        $res = '';
        $table = new Table("catalog_section");
        $section = $table->getEntityAlias("forms_section");
        if(!$section)
        {
            $section = $table->getEntity();
			      $section = ( !$section ) ? new stdClass() : $section;
            $section->title = 'Формы';
            $section->alias = 'forms_section';
            $section->section_table = 'section_forms_settings';
            $section->children_tpl =
'<tpl>
  <section>section_forms</section>
  <position>position_forms</position>
  <leaf>1</leaf>
</tpl>';
            $table->save($section);
            if($table->errorInfo)
            {
                return $table->errorInfo;
            }
        }

        $rows = $table->select('select * from forms_field_action');

        if(count($rows) == 0)
        {
			$sql = "
INSERT INTO `forms_field_action` (`id`, `name`, `position`) VALUES
('auth', 'Авторизация', 2),
('is_active', 'Активация учетной запись', 4),
('register', 'Регистрация', 1),
('restore_pass', 'Восстановить пароль', 3),
('send_email', 'Отправка на email', 5);";
            $table->execute($sql);
		}
		
        $rows = $table->select('select * from forms_field_type');

        if(count($rows) == 0)
        {

            $sql .= "
INSERT INTO `forms_field_type` (`id`, `name`, `position`) VALUES
('check', 'Флаг', 7),
('date', 'Дата', 5),
('hidden', 'Скрытое поле', 11),
('html', 'Произвольный HTML', 15),
('info', 'Информация', 14),
('label', 'Подпись', 13),
('link', 'Ссылка', 10),
('memo', 'Многострочный текст', 2),
('pwd', 'Пароль', 6),
('radiobox', 'Радиобокс', 12),
('select', 'Выпадающий список', 3),
('submit', 'Кнопка подачи запроса', 8),
('text', 'Текстовая строка', 1),
('upload', 'Обзор', 4);";
            $table->execute($sql);
        }

        return $res;
    }

}
?>
