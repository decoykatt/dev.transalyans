{
    xtype: 'form',
    id: 'redirection-form-<?php echo $redirection->id?>',
    frame: true,
    bodyBorder : true,
    title: 'Перенаправление',
    autoScroll: true,
    defaults:
    {
        width: 400
    },
    items:
    [
        {
            xtype: 'hidden',
            name: 'id',
            value: <?php echo $redirection->id ?>
        },
        {
            xtype: 'textfield',
            fieldLabel: 'Название',
            name: 'name',
            allowBlank: false,
            value: <?php echo escapeJSON($redirection->name)?>
        },
        {
            xtype: 'textfield',
            fieldLabel: 'Символьный идентификатор',
            width: 180,
            name: 'alias',
            value: <?php echo escapeJSON($redirection->alias)?>
        },
        {
            xtype: 'textfield',
            fieldLabel: 'URL',
            width: 180,
            name: 'url',
            value: <?php echo escapeJSON($redirection->url)?>
        },
        
        
    ],
    labelAlign: 'top',
    buttonAlign: 'center',
    buttons:
    [
        {
            text: 'Сохранить',
            formBind: true,
            handler: function()
            {
                var form = this.ownerCt.ownerCt;
                form.getForm().submit({
                        url: '/ajax/mysite/redirection.cm.save',
                        method: 'POST',
                        waitTitle: 'Подождите',
                        waitMsg: 'Сохранение...',
                        success: function(form, action){
                                App.msg('Готово', action.result.msg);
                            },
                        failure: function(form, action){
                                Ext.MessageBox.alert('Ошибка', action.result.msg);
                            }
                    });
            }
        },
        {
            text: 'Закрыть',
            formBind: true,
            handler: function()
            {
                App.closeEditor({id : this.ownerCt.ownerCt.ownerCt.id});
            }
        }
    ]
}

