<?php

/**
 * Функция для загрузки классов «на лету»
 */
function __autoload($class_name)
{
    $filename = strtolower($class_name) . '.php';

    $file = SITE_PATH . 'cms' . DS . 'classes' . DS . $filename;

    if (file_exists($file) == false)
    {

        return false;

    }

    require_once ($file);

}

 $registry = Registry::__instance();

 // Создание глобального роутера

$registry->router = new Router();

/**
 * Функция вывода глобальной переменной
 *
 * @param string $name имя переменной
 */

function out($name)
{
    $registry = Registry::__instance();

    $val = $registry->__get($name);

    echo $val;
}

function mod_content($name, $arg=null)
{
    $registry = Registry::__instance();

    $names = explode(".", $name);

    $names = array_pad($names, 3, null);

    $module = $names[0];
    $controller = $names[1];
    $action = $names[2];

    $registry->router->dispatch($module,$controller,$action,$arg);
}

function mod($name, $arg=null)
{
    mod_content($name, $arg);

    out('mod_content');

    Registry::__instance()->mod_content = "";
}

function val($name, $arg=null)
{
    mod_content($name, $arg);
    
    $mod_content = Registry::__instance()->mod_content;

    Registry::__instance()->mod_content = "";

    return $mod_content;
}

Registry::__instance()->tohead = "";

function tohead($name)
{
    $registry = Registry::__instance();

    $registry->tohead = $registry->tohead." ".val($name."_tohead");
}

function memo()
{
    return  time();
}

// Создание глобального объекта для работы с базой данных

if(isset($db_host))
{
    try
    {
        $db = new PDO("mysql:host=$db_host;dbname=$db_name;charset=utf8",
            $db_user,
            $db_user_pass,
            array(
				PDO::MYSQL_ATTR_INIT_COMMAND => 'SET sql_mode=(SELECT REPLACE(@@sql_mode,"ONLY_FULL_GROUP_BY",""))'
						)
        );
        $registry->db = $db;
    }
    catch (PDOException $e)
    {
        //echo 'Ошибка соединения: ' . $e->getMessage();
        //exit;
    }

    $registry->site_name = $site_name;
}
// Создание глобального объекта для работы с эквайрингом
if ( isset( $pay_conf ) && count( $pay_conf ) && gettype( $pay_conf ) == 'array' ) {
	$pay = Acquiring :: __instance( $pay_conf );
    $registry -> pay = $pay;
}

/**
* Escapes special characters
* Function escapes ", \, /, \n and \r symbols so that not to cause JavaScript error or
* data loss
*
* @param string $string
* @return string
*/
function escapeJSON($string) {
//    return str_replace(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'), $string);
    return json_encode($string);
}

class phpException extends exception {
    public function __construct($errno, $errstr, $errfile, $errline) {
        parent::__construct();
        $this->code = $errno;
        $this->message = $errstr;
        $this->file = $errfile;
        $this->line = $errline;
    }
}

function err2exc($errno, $errstr, $errfile, $errline) {
    throw new phpException($errno, $errstr, $errfile, $errline);
}

function offset()
{
    exit;
}

//set_error_handler('err2exc', E_ERROR | E_NOTICE | E_WARNING);
//error_reporting(E_ERROR);

Sys :: __instance();

function get_cache_pic($photo, $w = 100, $h = 100, $orig_ratio=true, $watermark='',$url='files/catalog/cache/' )
{
	if ($photo == null || trim($photo)=='' || !is_file(SITE_PATH.$photo)) return '';

        $ps = explode('/', $photo);
        $p = $ps[count($ps)-1];
        if(count($ps) > 2 && $ps[1] != 'catalog')
        { 
            $p = str_replace('/', '_', $photo);
        }
	
	$cache_url = $url . $w . 'x' . $h . '_' . $p; 

	if ($w > 0 && $h > 0) $t = 2;
	else $t = 1;

	if (!is_file(SITE_PATH.$cache_url))
	{
            Utils::img_resize(  SITE_PATH.$photo,
                                SITE_PATH.$cache_url,
                                $w,
                                $h,
                                $orig_ratio);
            if($watermark)
            {
                Utils::img_watermark(SITE_PATH.$watermark, SITE_PATH.$cache_url,SITE_PATH.$cache_url);
            }
	}
	return $cache_url;
}


if (!isset($_SERVER['SCRIPT_URL']) && isset($_SERVER['SCRIPT_NAME']))
{
    $_SERVER['SCRIPT_URL'] = $_SERVER['SCRIPT_NAME'];
}




if( !function_exists('apache_request_headers') ) {
///
	function apache_request_headers() {
	  $arh = array();
	  $rx_http = '/\AHTTP_/';
	  foreach($_SERVER as $key => $val) {
		if( preg_match($rx_http, $key) ) {
		  $arh_key = preg_replace($rx_http, '', $key);
		  $rx_matches = array();
		  // do some nasty string manipulations to restore the original letter case
		  // this should work in most cases
		  $rx_matches = explode('_', $arh_key);
		  if( count($rx_matches) > 0 and strlen($arh_key) > 2 ) {
			foreach($rx_matches as $ak_key => $ak_val) $rx_matches[$ak_key] = ucfirst($ak_val);
			$arh_key = implode('-', $rx_matches);
		  }
		  $arh[$arh_key] = $val;
		}
	  }
	  return( $arh );
	}

}



if (!function_exists('codepoint_encode')) {

    function codepoint_encode($str) {
        return substr(json_encode($str), 1, -1);
    }

}

if (!function_exists('codepoint_decode')) {

    function codepoint_decode($str) {
        return json_decode(sprintf('"%s"', $str));
    }

}

if (!function_exists('mb_internal_encoding')) {

    function mb_internal_encoding($encoding = NULL) {
        return ($from_encoding === NULL) ? iconv_get_encoding() : iconv_set_encoding($encoding);
    }

}

if (!function_exists('mb_convert_encoding')) {

    function mb_convert_encoding($str, $to_encoding, $from_encoding = NULL) {
        return iconv(($from_encoding === NULL) ? mb_internal_encoding() : $from_encoding, $to_encoding, $str);
    }

}

if (!function_exists('mb_chr')) {

    function mb_chr($ord, $encoding = 'UTF-8') {
        if ($encoding === 'UCS-4BE') {
            return pack("N", $ord);
        } else {
            return mb_convert_encoding(mb_chr($ord, 'UCS-4BE'), $encoding, 'UCS-4BE');
        }
    }

}

if (!function_exists('mb_ord')) {

    function mb_ord($char, $encoding = 'UTF-8') {
        if ($encoding === 'UCS-4BE') {
            list(, $ord) = (strlen($char) === 4) ? @unpack('N', $char) : @unpack('n', $char);
            return $ord;
        } else {
            return mb_ord(mb_convert_encoding($char, 'UCS-4BE', $encoding), 'UCS-4BE');
        }
    }

}

if (!function_exists('mb_htmlentities')) {

    function mb_htmlentities($string, $hex = true, $encoding = 'UTF-8') {
        return preg_replace_callback('/[\x{80}-\x{10FFFF}]/u', function ($match) use ($hex) {
            return sprintf($hex ? '&#x%X;' : '&#%d;', mb_ord($match[0]));
        }, $string);
    }

}


if ( !defined( 'ENT_HTML401' ) ) define( 'ENT_HTML401', 0 );


if (!function_exists('mb_html_entity_decode')) {

    function mb_html_entity_decode($string, $flags = null, $encoding = 'UTF-8') {
        return html_entity_decode($string, ($flags === NULL) ? ENT_COMPAT | ENT_HTML401 : $flags, $encoding);
    }

}