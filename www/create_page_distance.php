<?php

	ini_set( 'memory_limit', '500M' );

	$path_cms = realpath( dirname( __FILE__ ) . '/..' ) . DIRECTORY_SEPARATOR . 'www' . DIRECTORY_SEPARATOR;
	require realpath( dirname( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'morphy' . DIRECTORY_SEPARATOR . 'changewords.php';
	require $path_cms . 'config.php';
	require $path_cms . 'cms.php';

	$sc_fp = file("./siparat.data");

	foreach ( $sc_fp as $key => $f ) {
			
		$ex_town = explode( '|', $f );
		$f = trim( $ex_town[ 1 ] );

		//ChangeWords
		$parent_page = 'Из ' .ChangeWords($f);
		$parent_page_alias = 'ne-gabaritnie-gruzoperevozki-iz-' . Utils :: translit(  $f );
		$position = 10;

		$table = new Table( 'pages' );
		$parent_page_exist = $table -> select( 'SELECT * FROM `pages` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $parent_page_alias ) );
		if ( !count( $parent_page_exist ) ) {
			// создаем родительскую страницу
			$parent = new stdClass( );
			$parent -> title = $parent_page;
			$parent -> title2 = '';
			$parent -> alias = $parent_page_alias;
			$parent -> visible = 1;
			$parent -> meta_keywords = '';
			$parent -> meta_description = '';
			$parent -> head_title = 'Товарное предложение города';
			$parent -> position = $position;
			$parent -> template = 'page.tpl.php';
			$parent -> mainpage = 0;
			$parent_id = $table -> save( $parent );
			if ( $table -> errorInfo ) {
				throw new Exception( 'pdo_error : ' . $table -> errorInfo );
			}
			echo "parent page is created! id - " . $parent_id . "\n";
		}
		else {
			$parent_page_exist = end( $parent_page_exist );
			$parent_page_exist = ( object ) $parent_page_exist;
			$parent_id = $parent_page_exist -> id;
/*			echo "parent page is exist! id - " . $parent_id . "\n";*/
			continue;
		}

		// Такси Челябинск - Город
		$town_id = array( );
		$position++;

		foreach ($sc_fp as $key2 => $f_sub) {

				$ex_town = explode( '|', $f_sub );
				$dist = trim( $ex_town[ 3 ] );
				$time = trim( $ex_town[ 4 ] );
				$price = $dist.'|'.$time;
				$f_sub = trim( $ex_town[ 2 ] );

				if ($ex_town[1] !== $f) {
					continue;
				} else {

					// создаем страницу межгорода
					$town_page_alias = Utils :: translit( $f  .' - '. $f_sub );
					$town_alias = Utils :: translit( $f );
					$town_page_exist = $table -> select( 'SELECT * FROM `pages` WHERE `alias`=:alias LIMIT 1', array( 'alias' => $town_page_alias ) );

					if ( !count( $town_page_exist ) ) {
						$town = new stdClass( );
						$town -> parent_id = $parent_id;
						$town -> title = $f .' - '. $f_sub;
						$town -> title2 = $price;
						$town -> alias = $town_page_alias;
						$town -> content = '<h2>ОПИСАНИЕ МАРШРУТА</h2>

<div class="liline"></div>

<p>Расстояние в '.$dist.' км. (в зависимости от конкретных адресов в городах) преодолевается легко. В рекомендованном скоростном режиме дорога займет '.$time.'.</p>
';
						$town -> visible = 1;
						$town -> meta_keywords = '';
						$town -> meta_description = 'Не габаритный груз - перевозка из ' .  $f   .' в '. $f_sub;
						$town -> announcement = '';
						$town -> head_title = $f .' - '. $f_sub;
						$town -> position = $position;
						$town -> template = 'offer.tpl.php';
						$town -> mainpage = 0;
						$town -> tag = 'Перевозка не габаритных грузов ' . $f .' - '. $f_sub . ', всего за ' . $dist*95 . ' руб.';
						$t_id = $table -> save( $town );
						$town_id[ $f ] = $t_id;
						if ( $table -> errorInfo ) {
							throw new Exception( 'pdo_error : ' . $table -> errorInfo );
						}
						echo "town page " . Utils :: translit( $town -> title ) . " is created! id - " . $t_id . "\n";
					}
					else {
						$town_page_exist = end( $town_page_exist );
						$town_page_exist = ( object ) $town_page_exist;
						$town_id[ $f ] = $town_page_exist -> id;
						$update =  'UPDATE `pages`
									SET `title2`=:title2,
									    `title`=:title,
										`meta_description`=:meta_description,
										`meta_keywords`=:meta_keywords,
										`head_title`=:head_title,
										`tag`=:tag,
										`announcement`=:announcement
									WHERE `id`=:id';
						$table -> execute( $update, array(
							'title2' => $price,
							'title' => $f  .' - '. $f_sub,
							'meta_description' => 'Не габаритный груз - перевозка из ' . ChangeWords( $f )  .'в '. $f_sub,
							'announcement' => '',
							'meta_keywords' => '',
							'head_title' => $f  .' - '. $f_sub,
							'tag' => 'Перевозка не габаритных грузов ' . $f .' - '. $f_sub . ', всего за ' . 0 . ' руб.',
							'id' => $town_page_exist -> id
						) );
						echo "town page " . $town_page_exist -> alias . " is exist! id - " . $town_page_exist -> id . "\n";
				}
			}
			}
	}


				