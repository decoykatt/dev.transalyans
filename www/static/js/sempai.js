$(document).ready(function(){

	$('body').on('click', '#insurance', function(){
		$('.offer_insurance_active').slideToggle();
	});

	$('body').on('click', '.offer_insurance_item input', function(){
		var check = $(this).prop('checked');
		if (check) {
			$('.blue_bear').removeClass('active');
			$(this).closest('.offer_insurance_item').find('.blue_bear').addClass('active');
		}
	});

	$('body').on('click', '.blue_bear', function(){
		var check = $(this).closest('.offer_insurance_item').find('input').prop('checked');
		if (!check) {
			$(this).closest('.offer_insurance_item').find('input').prop('checked' , true);
		}
		var check_2 = $(this).closest('.offer_insurance_item').find('input').prop('checked');
		if (check_2) {
			$('.blue_bear').removeClass('active');
			$(this).closest('.offer_insurance_item').find('.blue_bear').addClass('active');
		}
	});

	city_left = $('.city_1_offer_saaa input').val();
	city_right = $('.city_2_offer_saaa input').val();

	$('body').on('click', '.city_1_offer_saaa input', function(){
		if ( $(this).val() == city_left ) {
			$(this).val('');
			$(this).change();
		}
	});

	$('body').on('click', '.city_2_offer_saaa input', function(){
		if ( $(this).val() == city_right ) {
			$(this).val('');
			$(this).change();
		}
	});

	$('body').on('click', '#map_showing', function(){
		$('.hz-che').slideToggle();
	});

	
});