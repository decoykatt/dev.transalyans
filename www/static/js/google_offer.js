/* $(document).on( 'load', function() {*/
    //Карта 
    if( $('#map_offer').length ) { 

        var routeBuilder = new function() {

            var city_start_location = $('.city_1_offer').attr('data-location');
            var city_start_name = $('.city_1_offer_saaa input').val();
            var city_end_location = $('.city_2_offer').attr('data-location');
            var city_end_name = $('.city_2_offer_saaa input').val();
            city_start_location = city_start_location.split(',');
            city_end_location = city_end_location.split(',');

            city_start_name = toTranslit(city_start_name);
            city_end_name = toTranslit(city_end_name);

            var self = this; 

            var directionsService = new google.maps.DirectionsService(); 
            var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true}); 
            var destination = new google.maps.LatLng(city_end_location[1]*1 , city_end_location[0]*1); 

            var mapOptions = { 
            zoom: 10, 
            center: destination, 
            scrollwheel: false 
            } 

            var map = new google.maps.Map(document.getElementById("map_offer"), mapOptions); 

            directionsDisplay.setMap(map); 

            var end_icon = new google.maps.MarkerImage( 
	            // URL 
	            '/static/img/citys/'+city_end_name+'.svg', 
	            new google.maps.Size( 113, 113 ), 
	            // The origin point (x,y) 
	            new google.maps.Point( 0, 0 ), 
	            // The anchor point (x,y) 
	            new google.maps.Point( 76, 93 ) 
            ); 

            var start_icon = new google.maps.MarkerImage( 
	            '/static/img/citys/'+city_start_name+'.svg', 
	            new google.maps.Size( 113, 113 ), 
	            new google.maps.Point( 0, 0 ), 
	            new google.maps.Point( 76, 93 ) 
            ); 


            function makeMarker( position, icon, title ) { 
	            var infowindow = new google.maps.InfoWindow({ 
	            content: title 
	            }); 
	            var marker = new google.maps.Marker({ 
	            position: position, 
	            map: map, 
	            icon: icon, 
	            title: title 
	            }); 
	            google.maps.event.addListener(marker, 'click', function() { 
	            infowindow.open(map,marker); 
            }); 
            } 

            function toTranslit(text) {
			    return text.replace(/([а-яё])|([\s_-])|([^a-z\d])/gi,
			    function (all, ch, space, words, i) {
			        if (space || words) {
			            return space ? '' : '';
			        }
			        var code = ch.charCodeAt(0),
			            index = code == 1025 || code == 1105 ? 0 :
			                code > 1071 ? code - 1071 : code - 1039,
			            t = ['yo', 'a', 'b', 'v', 'g', 'd', 'e', 'zh',
			                'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p',
			                'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh',
			                'shch', '', 'y', '', 'e', 'yu', 'ya'
			            ]; 
			        return t[index];
			    });
			}

            var startPosition; 

            this.startPosition = new google.maps.LatLng(city_start_location[1]*1 , city_start_location[0]*1);

            this.directionsService = directionsService; 
            this.directionsDisplay = directionsDisplay; 
            this.destination = destination;
            this.map = map; 

            this.calcRoute = function() { 

                if( typeof this.startPosition == 'undefined' ) { 
                    return false; 
                } 

                var travelMode; 
                var transitOptions; 

                travelMode = google.maps.TravelMode.DRIVING; 

                var request = { 
                origin: this.startPosition, 
                destination: this.destination, 
                travelMode: travelMode,
                provideRouteAlternatives: false, 
                }; 

                directionsService.route(request, function(result, status) { 
                if (status == google.maps.DirectionsStatus.OK) { 
                directionsDisplay.setDirections(result); 
                var leg = result.routes[ 0 ].legs[ 0 ]; 
                makeMarker( leg.end_location, start_icon, 'Место отправки' ); 
                makeMarker( leg.start_location, end_icon, 'Место прибытия' ); 
                } 
                else if( status == google.maps.DirectionsStatus.ZERO_RESULTS ) { 
                alert('Этот вид проезда не поддерживается'); 
                } 
                });

            }; 

            this.calcRoute();

        } 
    }

/*});*/