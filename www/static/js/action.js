  	function initMap( mapSelector ) {

	   	if( !$( mapSelector ).length ) return;

	   	var map, marker;

	   	var latitude = $( mapSelector ).attr( 'data-lat' );
	   	var longitude = $( mapSelector ).attr( 'data-lon' );
	    var zoom = $( mapSelector ).attr( 'data-zoom' );


	   	map = new google.maps.Map( document.getElementById( mapSelector.substr( 1 ) ), {
		    center: {lat: +latitude, lng: +longitude},
		    zoom: parseFloat( zoom ),
		    scrollwheel: false,
		    disableDefaultUI: true,
		    zoomControl: true
	   	});

	   	var marker = new google.maps.Marker( {
	      	map: map,
	      	position: { lat: +latitude, lng: +longitude },
	      	icon: '/static/img/mapicon.png'
	   	} );

  	}

  	var maps = [ '#map' ];
  	for ( var i = 0; i < maps.length; i++ ) {
  		setTimeout( initMap, 0, maps[ i ] );
  	}
