(function() {
	var triggerBttn = document.getElementById( 'trigger-overlay-1' ),
		triggerBttn_2 = document.getElementById( 'trigger-overlay-2' ),
		triggerBttn_3 = document.getElementById( 'trigger-overlay-3' ),
		triggerBttn_4 = document.getElementById( 'trigger-overlay-4' ),
		triggerBttn_6 = document.getElementById( 'trigger-overlay-6' ),
		overlay = document.querySelector( 'div.overlay' ),
		closeBttn = overlay.querySelector( 'button.overlay-close' );
		transEndEventNames = {
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend',
			'OTransition': 'oTransitionEnd',
			'msTransition': 'MSTransitionEnd',
			'transition': 'transitionend'
		},
		transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
		support = { transitions : Modernizr.csstransitions };

	function toggleOverlay() {
		if( classie.has( overlay, 'open' ) ) {
			classie.remove( overlay, 'open' );
			classie.add( overlay, 'close' );
			var onEndTransitionFn = function( ev ) {
				if( support.transitions ) {
					if( ev.propertyName !== 'visibility' ) return;
					this.removeEventListener( transEndEventName, onEndTransitionFn );
				}
				classie.remove( overlay, 'close' );
			};
			if( support.transitions ) {
				overlay.addEventListener( transEndEventName, onEndTransitionFn );
			}
			else {
				onEndTransitionFn();
			}
		}
		else if( !classie.has( overlay, 'close' ) ) {
			classie.add( overlay, 'open' );
		}
	}

	closeBttn.addEventListener( 'click', toggleOverlay );
		triggerBttn.addEventListener( 'click', toggleOverlay );
		triggerBttn_2.addEventListener( 'click', toggleOverlay );
		triggerBttn_3.addEventListener( 'click', toggleOverlay );
		triggerBttn_4.addEventListener( 'click', toggleOverlay );
		triggerBttn_6.addEventListener( 'click', toggleOverlay );
	
})();