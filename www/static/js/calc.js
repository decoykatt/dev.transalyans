$(document).ready(function(){

	$('body').on('click', '.values_item label', function(){
		var vidadin = $(this).closest('p').find('input[type="text"]').val();
		var check = $(this).closest('p').find('input[type="checkbox"]').prop('checked');
		if (!check) {
			$(this).closest('p').find('input[type="text"]').val(parseInt(vidadin, 10) + 1);
		}
		else {
			$(this).closest('p').find('input[type="text"]').val(0);
		}
		reSum();
	});

	$('body').on('click', '.offer_calculator .minus', function(){
		var vidadin = $(this).closest('p').find('input[type="text"]').val();
		$(this).closest('p').find('input[type="text"]').val(parseInt(vidadin, 10) - 1);
		if ( vidadin <= 0 ) { 
			$(this).closest('p').find('input[type="text"]').val(0);
		}
		if ( vidadin <= 1 ) {
			$(this).closest('p').find('input[type="checkbox"]').prop('checked' , false);
		}
		reSum();
	});

	$('body').on('click', '.offer_calculator .plus', function(){
		var vidadin = $(this).closest('p').find('input[type="text"]').val();
		$(this).closest('p').find('input[type="text"]').val(parseInt(vidadin, 10) + 1); 
		$(this).closest('p').find('input[type="checkbox"]').prop('checked' , true);
		reSum();
	});

	$('body').on('click', '.label-price', function(){
		reSum();
	});

	$('.car_feels input').change(function(){
  		var width = $('.car_feels input[name="car_width"]').val();
  		var height = $('.car_feels input[name="car_height"]').val();
  		var length = $('.car_feels input[name="car_length"]').val();
  		var values1 = $('.value_label').closest('p').find('input[type="text"]').val();
  		if ( height > 3.8 ) {
  			$('.value_label').closest('p').find('input[type="text"]').val(1);
  			$('.value_label').closest('p').find('input[type="checkbox"]').prop('checked' , true);
  		}
  		if ( length > 25 || width > 3.5) {
  			$('.value_label').closest('p').find('input[type="text"]').val(2);
  			$('.value_label').closest('p').find('input[type="checkbox"]').prop('checked' , true);
  		}
  		reSum();
	});

	var summary_count = document.getElementById('summary_count');
	var kilometrjon = document.getElementById('kolmetraj');

	function reSum() {
		var sum = 0, price, qty, qty_price , label, label_price;

		$('.price-item').each(function( index ) {
			price = $(this).attr('data-price');
			qty = $( this ).find('input[type="text"]').val();
			if (!qty) {qty = 0;}
			label = $(this).closest('p').find('input[type="checkbox"]').prop('checked');
			if (label && !qty) {label_price = $(this).attr('data-price');}
			else {label_price = 0;}
			qty_price = parseInt(price, 10) * parseInt(qty, 10);
			sum = sum  + parseInt(qty_price , 10 ) + parseInt(label_price , 10 );
		});

		summary_count.innerHTML = parseInt(kilometrjon.innerHTML.replace(" ", ""), 10) + parseInt(sum, 10) ;
		summary_count.innerHTML = parseInt(summary_count.innerHTML, 10);
		summary_count.innerHTML = summary_count.innerHTML.replace(/\B(?=(\d{3})+(?!\d))/g, " ")
	}


});