<?php


require_once( realpath( dirname(__FILE__) . '/phpmorphy-0.3.7/src/common.php'  ) );



function ChangeWords($word)
{

	$morphy = new phpMorphy(	realpath( dirname(__FILE__) . '/phpmorphy-0.3.7/dicts/utf-8' ),
												'ru_RU',
												array( 'storage' => PHPMORPHY_STORAGE_FILE ) );

	switch ( $word ) {

		case 'Щучье': return 'Щучье';
		case 'Карталы': return 'Карталов';
		case 'Курган': return 'Кургана';
		case 'Аэропорт Курган': return 'Аэропорта в Кургане';
		case 'Лесной': return 'Лесного';
		case 'Полевской': return 'Полевского';
		case 'Самара': return 'Самары';
		case 'Касли': return 'Каслей';

	}

	$Sentences = explode(", ", $word);
	$Sent = '';
	foreach ($Sentences as $s) 
	{
		$Words = explode(" ", $s);	

		$count = -1;
		foreach ($Words as $w)
		{
			$w = mb_strtolower($w, 'utf-8');
			if (preg_match('/^[а-яА-ЯёЁ]/',$w) and mb_strlen($w,'UTF-8') > 2)
			{
				$w = mb_strtoupper($w, 'utf-8');

				$case = 'РД';
				$gender = GetGender($w, $morphy);
/*
				if ($count > 0) $case = 'РД';

				$pos = $morphy->getPartOfSpeech($w);
				if ($pos[0] == 'С') 
					{
						if ($count>-1) $case = 'РД';	
						$count++;	
					}
Челябинск|санаторий Ассы|5500
Челябинск|санаторий Зелёная Роща|6800
Челябинск|санаторий Карагай|5500
Челябинск|санаторий Металлург|2300
Челябинск|санаторий Танып|8100
Челябинск|санаторий УралВО|1700
Челябинск|санаторий Жемчужина Зауралья|3600
Челябинск|санаторий Кособродск|2800
Челябинск|санаторий Красноусольск|8900
Челябинск|санаторий Солнечный|2300
Челябинск|санаторий Талкас|7300
Челябинск|санаторий Дюжонок|4400
Челябинск|санаторий Жемчужина Урала|1900
Челябинск|санаторий Лавита|3300
Челябинск|санаторий Озеро Медвежье|7500
Челябинск|санаторий Черёмушки|600
Челябинск|санаторий Юбилейный|5200
Челябинск|санаторий Увильды|1400
Челябинск|санаторий Еловое|1400
Челябинск|санаторий Озеро Горькое|1500
Челябинск|санаторий Кисегач|1500
Челябинск|санаторий Большой Боляш|1500
Челябинск|санаторий - профилакторий Лесная сказка|4100
Челябинск|санаторий Сосновая горка|1500
Челябинск|санаторий Соколиный камень|4100
Челябинск|санаторий Юматово|7100
Челябинск|санаторий Сунгуль|2000
Челябинск|санаторий Тургояк|2000
Челябинск|санаторий Дальняя дача|1700
Челябинск|санаторий Карагайский бор|3600
Челябинск|санаторий Курьи|4900
Челябинск|санаторий Синегорье|1900
Челябинск|санаторий Урал|1700
*/

				var_dump( $w );

				//Исключения
				switch ( $w ) {

    				case 'Карталы': $case = 'ИП'; break;
					case 'Курган': $case = 'ИП'; break;
					case 'Лесной': $case = 'ИП'; break;
					case 'Полевской': $case = 'ИП'; break;
					case 'Самара': $case = 'ИП'; break;
					case 'Касли': $case = 'ИП'; break;

    			}
				$new_word = $morphy->castFormByGramInfo($w, GetPoS($w, $morphy), array ( 'ЕД', $case, $gender ), true);
				var_dump( $new_word );
				$nw = ( isset( $new_word[ 0 ] ) ) ? $new_word[ 0 ] : '';
				$nw = mb_substr( $nw, 0, 1 ) . mb_strtolower( mb_substr( $nw, 1 ), 'UTF-8' );
				$Sent .= $nw . ' ';
			}
			else {
				$w = trim( $w );
				$w = mb_substr( $w, 0, 1 ) . mb_strtolower( mb_substr( $w, 1 ), 'UTF-8' );
				$Sent .= $w . ' ';
			}
		}
	}
	$Sent = trim( $Sent );
	//$Sent = mb_substr( $Sent, 0, 1 ) . mb_strtolower( mb_substr( $Sent, 1 ), 'UTF-8' );
	return $Sent;
}

function GetPoS( $word, $morphy )
{

	$PoS = $morphy->getPartOfSpeech($word);
	// if ( !isset( $PoS ) || !count( $PoS ) ) return $word;
	if ( !$PoS ) {
		return 'С';
	}
	$pos = null;

	foreach ( $PoS as $PoSes ) {

		if ($PoSes == 'С')
		{
			$pos = $PoSes;			
		}

		if ($PoSes == 'П')
		{
			$pos = $PoSes;					
		}
	}
	return $pos;

}

function GetGender($word, $morphy)
{
	$GramInfo = $morphy-> getGramInfo($word);
	$Gender = null;
	if ( !isset( $GramInfo[0][0]['grammems'] ) ) return $Gender;
	foreach ($GramInfo[0][0]['grammems'] as $gi) 
	{
		if ($gi == 'ЖР' or $gi == 'МР' or $gi == 'СР')
			$Gender = $gi;
		//Исключения
		switch ($word) {
    		case 'КАРТАЛЫ': $Gender = 'МР'; break;
    		case 'КУРГАН': $Gender = 'МР'; break;
			case 'ЛЕСНОЙ': $Gender = 'МР'; break;
			case 'ПОЛЕВСКОЙ': $Gender = 'МР'; break;
			case 'САМАРА': $Gender = 'ЖР'; break;			
			case 'КАСЛИ': $Gender = 'ЖР'; break;
			/*
    		case 'ЧАСТИ': $Gender = 'ЖР'; break;
    		case 'ХОДОВОЙ': $Gender = 'ЖР'; break;
			*/
    	}
	}
	return $Gender;
}

function GetLar($word, $morphy)
{
	$GramInfo = $morphy-> getGramInfo($word);
	$Lar = null;
	foreach ($GramInfo[0][0]['grammems'] as $gi) 
	{
		if ( $gi == 'ЕД' or $gi == 'МН' )
			$Lar = $gi;
		//Исключения
		switch ($word) {
		
    		case 'КАРТАЛЫ': $Lar = 'МН'; break;
    		case 'КУРГАН': $Lar = 'ЕД'; break;
			case 'ЛЕСНОЙ': $Lar = 'ЕД'; break;
			case 'ПОЛЕВСКОЙ': $Lar = 'ЕД'; break;
			case 'САМАРА': $Lar = 'ЕД'; break;			
			case 'КАСЛИ': $Lar = 'МН'; break;
			/*
			case 'ТРУСЫ': $Lar = 'МН'; break;
			case 'СТРИНГИ': $Lar = 'МН'; break;
			*/
    	}

	}
	return $Lar;
}


function CheckWord($word, $form = 1)
{
/*
	$Words['маска-шоры'] = array(1 => 'маска-шоры', 2 => 'маска-шоры');
	$Words['коп'] = array(1 => 'копа', 2 => 'копа');
	$Words['виброяйцо'] = array(1 => 'виброяйцо', 2 => 'виброяйцо');
	$Words['кость'] = array(1 => 'кости', 2 => 'кость');
	$Words['пар'] = array(1 => 'пар', 2 => 'пары');
	$Words['стразами'] = array(1 => 'страз', 2 => 'стразами');
	$Words['масок'] = array(1 => 'масок', 2 => 'маски');
	$Words['заклёпками'] = array(1 => 'заклёпками', 2 => 'заклёпками');
	$Words['мини'] = array(1 => 'мини', 2 => 'мини');
	$Words['шариками'] = array(1 => 'шариками', 2 => 'шариками');
	$Words['фаллоимитатор'] = array(1 => 'фаллоимитаторов', 2 => 'фаллоимитатор');
	$Words['трусы'] = array(1 => 'трусов', 2 => 'трусы');
*/
}
?>